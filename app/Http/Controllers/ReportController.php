<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Lead;
use Excel;

class ReportController extends Controller {

    public function viewReport() {

        $get_data = Lead::whereNull('deleted_at')->orderBy('created_at', 'desc')->get();
        $active_link = 'all';

        return view('pages.report')->with([
                    'data_lead' => $get_data,
                    'active_link' => $active_link
        ]);
    }

    public function viewReportPhilippines() {
        $get_data = Lead::where('country_group', 'like', 'ph')->whereNull('deleted_at')->orderBy('created_at', 'desc')->get();
        $active_link = 'Philippines';

        return view('pages.report')->with([
                    'data_lead' => $get_data,
                    'active_link' => $active_link
        ]);
    }

    public function viewReportOthers() {
        $get_data = Lead::where('country_group', '!=', 'ph')->whereNull('deleted_at')->orderBy('created_at', 'desc')->get();
        $active_link = 'others';

        return view('pages.report')->with([
                    'data_lead' => $get_data,
                    'active_link' => $active_link
        ]);
    }

    public function exportLead(Request $request) {

        $mytime = \Carbon\Carbon::now();
        $nama_file = 'lead-sqi-' . $mytime;

        $from_date = $request->from_date;
        $until_date = $request->until_date;
        $country = $request->country;

        Excel::create($nama_file, function($excel) use($from_date, $until_date, $country) {

            $excel->sheet('Lead', function($sheet) use($from_date, $until_date, $country) {

                $sheet->setAutoSize(true);

                $sheet->cell('A1:AB1', function($cells) {
                    $cells->setBackground('#00B0F0');
                    $cells->setFontSize(16);
                    $cells->setFontWeight('bold');
                    $cells->setBorder('none', 'none', 'thick', 'none');
                    $cells->setAlignment('center');
                });

                $sheet->setColumnFormat([
                    'F' => '@'
                ]);

                $sheet->row(1, array(
                    'Created At', 'Has Been Followed Up', 'Prefer time called', 'Total Score',
                    'Name', 'Phone Number', 'Email', 'Gender',
                    'Age Group', 'Nationality', 'Current Location', 'Last Academic', 'When Graduate',
                    'Area Study Interest', 'Study Destination', 'Level of Study', 'Planning Apply',
                    'Ever Take Test?', 'What Test you take?', 'When take test?', 'Test Score',
                    'Last Holiday', 'Close Family Overseas', 'Additional Note (if any)',
                    'UTM Source', 'Campaign', 'Adset', 'Ads'
                ));

                #kalau dia Philippines, tampilkan Philippines
                #kalau others, tampilkan selain Philippines
                #kalau tidak ada, tampilkan semuanya
                $leads = Lead::whereNull('deleted_at');

                if (strcasecmp($country, 'Philippines') == 0) {
                    $leads = $leads->where('country_group', 'like', 'ph');
                } else if (strcasecmp($country, 'others') == 0) {
                    $leads = $leads->where('country_group', '<>', 'ph');
                }
                $leads = $leads
                        ->orderBy('created_at', 'desc');

                if (isset($from_date) && isset($until_date)) {
                    $leads = $leads->where('created_at', '>=', \Carbon\Carbon::createFromFormat('Y-m-d', $from_date)->startOfDay()->toDateTimeString())
                            ->where('created_at', '<=', \Carbon\Carbon::createFromFormat('Y-m-d', $until_date)->endOfDay()->toDateTimeString());
                }

                $leads = $leads->get();

                $j = 2;

                foreach ($leads as $lead) {

                    $finish = 'No';
                    if ($lead->is_finish == 1) {
                        $finish = 'Yes';
                    }

                    $follow = 'No';
                    if ($lead->is_followed_up == 1) {
                        $follow = 'Yes';
                    }


                    if ($lead->prefer_time) {
                        $call_time = $lead->prefer_time;
                    } else {
                        $call_time = "Haven't Decided";
                    }

                    $sheet->row($j++, array(
                        $lead->created_at,
                        $follow,
                        $call_time,
                        $lead->total_score,
                        $lead->first_name . ' ' . $lead->last_name,
                        ' ' . $lead->mobile_phone,
                        trim($lead->email),
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->gender,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->age_group,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->nationality,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->current_location,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->last_academic,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->when_graduate,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->area_study_interest,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->study_destination,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->level_of_study,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->planning_apply,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->ever_take_ielts,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->test_taken,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->when_take_test,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->test_score,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->last_holiday,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->close_family_overseas,
                        $lead->leadsurvey == null ? '' : $lead->leadsurvey->additional_note,
                        $lead->utm_source,
                        $lead->campaign,
                        $lead->adset,
                        $lead->ads
                    ));
                }
            });
        })->export('xls');
    }

    public function setCandidatetype(Request $request) {

        $candidate = Lead::find($request->candidate_id);
        $candidate->is_followed_up = 1;
        $candidate->lead_rating = $request->candidate_type;
        $candidate->lead_status = $request->lead_status;
        $candidate->note_counselor = $request->candidate_note;
        $candidate->date_response = \Carbon\Carbon::now();
        $candidate->save();
        
        #update data di staging
        $response = $this->sendUpdatedLead($candidate, $request->candidate_type, $request->lead_status);

        $data = array(
            'candidate_type_controller' => $candidate->lead_rating,
            'candidate_note_controller' => $candidate->note_counselor,
            'lead_status' => $candidate->lead_status,
            'response' => $response
        );
        $data = json_encode($data);
        return $data;
        exit;
    }

    public function setCandidateStatus(Request $request) {
        $candidate = Lead::where('ph', $request->candidate_id)->first();
        $candidate->lead_status = $request->lead_status;
        $candidate->save();

        $data = array(
            'lead_status' => $candidate->lead_status,
        );
        $data = json_encode($data);
        return $data;
        exit;
    }

    public function reportFilter(Request $request) {
        $lead=[];
        if (strpos($request->choose_country, 'all') !== FALSE) {
            $lead = Lead::where('created_at', '>=', \Carbon\Carbon::createFromFormat('Y-m-d', $request->from_date)->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', \Carbon\Carbon::createFromFormat('Y-m-d', $request->until_date)->endOfDay()->toDateTimeString())
                    ->whereNull('deleted_at')
                    ->orderBy('created_at', 'desc')
                    ->get();

            $active_link = "all";
        } elseif (strpos($request->choose_country, 'philippines') !== FALSE) {
            $lead = Lead::where('created_at', '>=', \Carbon\Carbon::createFromFormat('Y-m-d', $request->from_date)->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', \Carbon\Carbon::createFromFormat('Y-m-d', $request->until_date)->endOfDay()->toDateTimeString())
                    ->whereNull('deleted_at')
                    ->where('leads.country_group', '=', 'ph')
                    ->orderBy('leads.created_at', 'desc')
                    ->get();
            $active_link = "Philippines";
        } elseif (strpos($request->choose_country, 'others') !== FALSE) {
            $lead = Lead::where('created_at', '>=', \Carbon\Carbon::createFromFormat('Y-m-d', $request->from_date)->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', \Carbon\Carbon::createFromFormat('Y-m-d', $request->until_date)->endOfDay()->toDateTimeString())
                    ->whereNull('deleted_at')
                    ->where('leads.country_group', '!=', 'ph')
                    ->orderBy('leads.created_at', 'desc')
                    ->get();
            $active_link = "others";
        }
        return view('pages/report')->with('data_lead', $lead)
                        ->with('active_link', $active_link)
                        ->with('from_date', $request->from_date)
                        ->with('until_date', $request->until_date);
    }
    
    private function sendUpdatedLead($lead, $lead_rating, $lead_status){
        $lead_source = "Study Query Philippines";

        #set data untuk dikirim
        $post = array(
            'email' => $lead->email,
            'lead_source' => $lead_source,
            'lead_rating' => $lead_rating,
            'lead_status' => $lead_status,
        );
        $post = json_encode($post);

        $ch = curl_init('http://128.199.163.151/api/lead/update');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $response = curl_exec($ch);

        curl_close($ch);
        return $response;
    }

}
