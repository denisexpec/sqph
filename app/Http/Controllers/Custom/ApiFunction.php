<?php

namespace App\Http\Controllers\Custom;

class ApiFunction {

    public static function sendUtmLead($utm_source, $utm_campaign, $utm_adset, $utm_ads, $utm_term, $lead_source, $client_ip) {
        #ambil data country code
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://ipinfo.io/' . $client_ip);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        $source_domain = "http://studyquery.co.id/";
        $country_code = 'id';

        if (isset($result->country)) {
            $country_code = $result->country;
        }

        $post_data = [
            'utm_source' => $utm_source,
            'utm_campaign' => $utm_campaign,
            'utm_adset' => $utm_adset,
            'utm_ads' => $utm_ads,
            'utm_term' => $utm_term,
            'client_ip' => $client_ip,
            'country_code' => $country_code,
            'source_domain' => $source_domain
        ];

        #kirim data 
        $ch = curl_init('http://128.199.163.151/utm/save');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $response = curl_exec($ch);
        curl_close($ch);
    }

}
