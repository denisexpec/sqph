<?php

namespace App\Http\Controllers\Custom;

use Illuminate\Http\Request;
use App\Http\Controllers\Custom\ApiFunction;
use App\Models\Utmlead;
use App\Models\Utmvisitor;

class UtmLeadFunction {

    public static function saveUtmLead(Request $request) {
        $utm_source = null;
        $utm_campaign = null;
        $adset = null;
        $ads = null;
        $utm_term = null;
        $lead_source = 'StudyQuery Indonesia - Studyquery.co.id';

        if (isset($request->utm_campaign)) {
            $utm_campaign = $request->utm_campaign;
        }

        if (isset($request->adset)) {
            $adset = $request->adset;
        }

        if (isset($request->ads)) {
            $ads = $request->ads;
        }

        if (isset($request->utm_term)) {
            $utm_term = $request->utm_term;
        }

        if (isset($request->utm_source)) {
            if (strlen($request->utm_source) > 0) {
                $utm_source = $request->utm_source;
            }

            #bagian set cookie, setiap ada isi utm doang;
            setcookie("utm_source", $utm_source, time() + 2419200);
            if (isset($utm_campaign)) {
                setcookie("utm_campaign", $utm_campaign, time() + 2419200);
            }
            if (isset($adset)) {
                setcookie("adset", $adset, time() + 2419200);
            }
            if (isset($ads)) {
                setcookie("ads", $ads, time() + 2419200);
            }
            if (isset($utm_term)) {
                setcookie("utm_term", $utm_term, time() + 2419200);
            }
        } elseif (isset($_COOKIE['utm_source'])) {
            $utm_source = $_COOKIE['utm_source'];
            if (isset($_COOKIE['utm_campaign'])) {
                $utm_campaign = $_COOKIE['utm_campaign'];
            }
            if (isset($_COOKIE['adset'])) {
                $adset = $_COOKIE['adset'];
            }
            if (isset($_COOKIE['ads'])) {
                $ads = $_COOKIE['ads'];
            }
            if (isset($_COOKIE['utm_term'])) {
                $utm_term = $_COOKIE['utm_term'];
            }
        }

        #kalau utm_source ada, kirim ke fortrust
        if (isset($utm_source)) {
            ApiFunction::sendUtmLead($utm_source, $utm_campaign, $adset, $ads, $utm_term, $lead_source, $request->ip());
        }

        #check dulu utmleadnya
        #dan save
        $utm_lead = null;
        if ($utm_source) {
            $utm_lead = Utmlead::where('utm_source', 'like', $utm_source);
        } else {
            $utm_lead = Utmlead::whereNull('utm_source');
        }
        if ($utm_campaign) {
            $utm_lead = $utm_lead->where('campaign', 'like', $utm_campaign);
        } else {
            $utm_lead = $utm_lead->whereNull('campaign');
        }
        if ($adset) {
            $utm_lead = $utm_lead->where('adset', 'like', $adset);
        } else {
            $utm_lead = $utm_lead->whereNull('adset');
        }
        if ($ads) {
            $utm_lead = $utm_lead->where('ads', 'like', $ads);
        } else {
            $utm_lead = $utm_lead->whereNull('ads');
        }
        if ($utm_term) {
            $utm_lead = $utm_lead->where('utm_term', 'like', $utm_term);
        } else {
            $utm_lead = $utm_lead->whereNull('utm_term');
        }
        $utm_lead = $utm_lead->first();

        #kalau ada, increment
        #kalau tidak ada, buat baru
        if ($utm_lead) {
            $utm_lead->counter += 1;
            $utm_lead->save();
        } else {
            $utm_lead = new Utmlead;
            $utm_lead->utm_source = $utm_source;
            $utm_lead->campaign = $utm_campaign;
            $utm_lead->adset = $adset;
            $utm_lead->ads = $ads;
            $utm_lead->utm_term = $utm_term;
            $utm_lead->lead_source = $lead_source;
            $utm_lead->counter = 1;
            $utm_lead->save();
        }

        return $utm_lead->id;
    }

    public static function saveUtmVisitor($utmlead_id, $client_ip) {
        $utm_visitor = new Utmvisitor;
        $utm_visitor->utmlead_id = $utmlead_id;
        $utm_visitor->client_ip = $client_ip;

        #ambil data country code
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://ipinfo.io/' . $client_ip);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        if (isset($result->country)) {
            $utm_visitor->country_code = $result->country;
        } else {
            $utm_visitor->country_code = 'id';
        }
        $utm_visitor->save();

        return $utm_visitor->id;
    }

}
