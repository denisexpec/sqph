<?php

namespace App\Http\Controllers\Custom;
use App;

class TitleFunction {

    public static function getTitleStudent($lang) {
        $title = 'Pilihan Tepat untuk Siswa';
        if ($lang) {
            App::setLocale($lang);
            if ($lang == 'en') {
                $title = 'Smart Choices for Smart Student';
            }
        }
        return $title;
    }

    public static function getTitleParent($lang) {
        $title = 'Pilihan Tepat untuk Anak Anda';
        if ($lang) {
            App::setLocale($lang);
            if ($lang == 'en') {
                $title = 'Smart Choices for Smart Parent';
            }
        }
        return $title;
    }

}
