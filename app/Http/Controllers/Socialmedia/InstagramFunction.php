<?php

namespace App\Http\Controllers\Socialmedia;

use App\Models\Instagramdata;

class InstagramFunction {

    public static function getNewestFeed($code) {
        #siapin data untuk OAUTH
        $data = [
            'client_id' => 'd0322cd0bd7a4f069e5f6c24a040d127',
            'client_secret' => '65eef3c2f38e4cc9bbba9325134308ba',
            'grant_type' => 'authorization_code',
            'redirect_uri' => 'http://studyquery.co.id/igfeed',
            'code' => $code
        ];

        #OAUTH
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.instagram.com/oauth/access_token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);

        #get recent media of user
        if (isset($result->access_token)) {
            $access_token = $result->access_token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $access_token . '&count=6');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($response);

            #save ke DB
            foreach ($result->data as $data) {
                $instagram = Instagramdata::where('instagram_id', 'like', $data->id)
                        ->first();
                if (!$instagram) {
                    $instagram = new Instagramdata;
                    $instagram->instagram_id = $data->id;
                    $instagram->thumbnail = $data->images->thumbnail->url;
                    $instagram->low_res = $data->images->low_resolution->url;
                    $instagram->high_res = $data->images->standard_resolution->url;
                    $instagram->link = $data->link;
                    $instagram->save();
                }
            }
            return 1;
        } else {
            #kalau tidak ada access token berarti code nya expired.
            return 0;
        }
    }

}
