<?php

namespace App\Http\Controllers\Socialmedia;

use App\Models\Socialtiming;

class SocialTimingFunction {

    public static function validateInstagramTiming() {
        #per jam
        $timing_periodic = 1;

        $social_timing = Socialtiming::first();
        $must_retrieve_new_data = 1;
        if (!isset($social_timing)) {
            $social_timing = new Socialtiming;
            $social_timing->facebook_timing = \Carbon\Carbon::now()->toDateTimeString();
            $social_timing->instagram_timing = \Carbon\Carbon::now()->toDateTimeString();
            $social_timing->save();
        } else {
            #cek apakah timingnya instagram sudah lewat 1 jam
            $social_timing = Socialtiming::where('instagram_timing', '<=', \Carbon\Carbon::now()->subHours($timing_periodic)->toDateTimeString())
                    ->first();
            if (!$social_timing) {
                $must_retrieve_new_data = 0;
            }
        }

        return $must_retrieve_new_data;
    }
    
    public static function updateInstagramSocialTiming(){
        $social_timing = Socialtiming::first();
        $social_timing->instagram_timing = \Carbon\Carbon::now()->toDateTimeString();
        $social_timing->save();
    }

}
