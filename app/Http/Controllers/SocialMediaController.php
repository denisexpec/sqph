<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Socialmedia\InstagramFunction;

class SocialMediaController extends Controller {

    public function instagramFeed(Request $request) {
        #grab data dari instagram kemudian save ke database per periodik
        if (isset($request->code)) {
            $result = InstagramFunction::getNewestFeed($request->code);
            if ($result) {
                return redirect('/');
            } else {
                return redirect('igfeed');
            }
        } else {
            return redirect("https://api.instagram.com/oauth/authorize/?client_id=d0322cd0bd7a4f069e5f6c24a040d127&redirect_uri=http://studyquery.co.id/igfeed&response_type=code");
        }
    }

}
