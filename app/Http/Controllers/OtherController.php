<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OtherController extends Controller {

    public function viewAbout(Request $request) {
        $title = 'Tentang Kami';
        $source = 'student';
        $lead_source = 'direct';
        if ($request->segment(2)) {
            $lead_source = $request->segment(1);
            $source = $request->segment(1);
        }

        if ($request->lang) {
            if ($request->lang == 'en') {
                $title = 'About Us';
                \App::setLocale('en');
            }else{
                \App::setLocale('id');
            }
        }

        return view('pages.aboutus' . $source)->with([
                    'title' => $title,
                    'lead_source' => $lead_source,
                    'source' => $source,
        ]);
    }

}
