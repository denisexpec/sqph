<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Custom\UtmLeadFunction;
use App\Http\Controllers\Custom\TitleFunction;
use App\Models\Lead;
use App\Models\Leadsurvey;
use App\Models\Utmvisitor;
use hisorange\BrowserDetect;
use App;

class HomeController extends Controller {

    public function test(Request $request) {
        #function ini biarkan saja, khusus untuk testing
    }
    
    public function viewHome(Request $request) {
        $utmlead_id = UtmLeadFunction::saveUtmLead($request);
        $utmvisitor_id = UtmLeadFunction::saveUtmVisitor($utmlead_id, $request->ip());
        $title = TitleFunction::getTitleStudent($request->lang);
        
        #ambil segment pertamanya
        $segment = $request->segment(1);
        if(!$segment){
            $segment = 'student';
        }

        return view('pages.home.' . $segment)->with([
                    'lead_source' => $segment,
                    'title' => $title,
                    'utm_lead' => $utmlead_id,
                    'utm_visitor' => $utmvisitor_id,
                    'page' => $segment
        ]);
    }

    public function insertLead(Request $request) {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required'
        ]);

        $result = \BrowserDetect::detect();

        $browser_name = \BrowserDetect::browserName();
        $browser_version = \BrowserDetect::browserVersion();
        $browser_family = \BrowserDetect::browserFamily();
        $is_mobile = \BrowserDetect::isMobile();
        $is_tablet = \BrowserDetect::isTablet();
        $is_desktop = \BrowserDetect::isDesktop();
        $is_bot = \BrowserDetect::isBot();
        $os_family = \BrowserDetect::osFamily();
        $os_version = \BrowserDetect::osVersion();
        $os_name = \BrowserDetect::osName();
        $device_family = \BrowserDetect::deviceFamily();
        $device_model = \BrowserDetect::deviceModel();
        $css_version = \BrowserDetect::cssVersion();
        $mobile_grade = \BrowserDetect::mobileGrade();

        //checking if lead is exists or not;
        $lead = Lead::where('email', '=', $request->email)->first();

        if (!$lead) {
            $lead = new Lead; // insert baru, bukan kayak codeigniter
            $lead->email = $request->email;
        }

        $lead->first_name = $request->first_name;
        $lead->last_name = $request->last_name;
        $lead->full_name = $request->first_name . ' ' . $request->last_name;
        $lead->mobile_phone = $request->phone;
        $lead->lead_source = $request->lead_source;
        $lead->country_group = $request->country_code;
        $lead->total_score = 0;

        $lead->utm_visitor_id = $request->utm_visitor;

        $lead->ip_address = $request->ip();
        $lead->browser_name = $browser_name;
        $lead->browser_version = $browser_version;
        $lead->browser_family = $browser_family;
        $lead->is_mobile = $is_mobile;
        $lead->is_tablet = $is_tablet;
        $lead->is_desktop = $is_desktop;
        $lead->is_bot = $is_bot;
        $lead->os_family = $os_family;
        $lead->os_version = $os_version;
        $lead->os_name = $os_name;
        $lead->device_family = $device_family;
        $lead->device_model = $device_model;
        $lead->css_version = $css_version;
        $lead->mobile_grade = $mobile_grade;

        if ($request->lead_source) {
            $lead->lead_source = $request->lead_source;
        }

        if (isset($request->utm_source)) {
            $lead->utm_source = $request->utm_source;
        }

        if ($request->campaign) {
            $lead->campaign = $request->campaign;
        }

        if ($request->adset) {
            $lead->adset = $request->adset;
        }

        if ($request->ads) {
            $lead->ads = $request->ads;
        }

        $utm_term = NULL;
        if (isset($request->term)) {
            $utm_term = $request->term;
        }
        $lead->save();

        #kirim data awal
        $survey_data = [
            'gender' => '',
            'nationality' => 'Philippines',
            'destination' => ''
        ];

        if (isset($request->utm_source)) {
            $this->sendApiLead($lead->email, $lead->first_name, $lead->last_name, $lead->mobile_phone, $lead->lead_source, 4, 0, $lead->utm_source, $lead->campaign, $lead->adset, $lead->ads, $utm_term, $survey_data);
        }
        #save ke leadsurvey yang baru
        $lead_survey = Leadsurvey::where('lead_id', $lead->id)->first();
        if ($lead_survey) {
            $lead_survey->delete();
        } else {
            $lead_survey = new Leadsurvey;
            $lead_survey->lead_id = $lead->id;
            $lead_survey->save();
        }

        $redirect = 'survey';
        if(strcasecmp($request->segment(1), 'parent') == 0){
            $redirect = 'parentsurvey';
        }
        return redirect($redirect)->with([
            'leadinfo' => $lead,
            'language' => $request->language_setting
        ]);
    }

    public function survey(Request $request) {

        if (!$request->session()->get('leadinfo')) {
            return redirect('');
        }

        $data['lead_info'] = $request->session()->get('leadinfo');

        if ($request->session()->get('language')) {
            $lang = $request->session()->get('language');
            App::setLocale($lang);
            $data['lang'] = $lang;
        } else {
            App::setLocale('id');
            $data['lang'] = 'id';
        }

        if ($data['lead_info']->lead_source == 'student') {
            $data['source'] = 'student';
        } else {
            $data['source'] = 'direct';
        }


        if (isset($data['lead_info'])) {
            return view('pages/survey')->with($data);
        } else {
            return redirect('');
        }
    }

    public function parentsurvey(Request $request) {

        $data['lead_info'] = $request->session()->get('leadinfo');


        if ($request->session()->get('language')) {
            $lang = $request->session()->get('language');
            App::setLocale($lang);
            $data['lang'] = $lang;
        } else {
            App::setLocale('id');
            $data['lang'] = 'id';
        }

        $data['source'] = 'parent';

        if (isset($data['lead_info'])) {
            return view('pages/parentsurvey')->with($data);
        } else {
            return redirect('parent');
        }
    }

    public function processSurvey(Request $request) {

        $leadsurvey = Leadsurvey::where('lead_id', $request->id_user_candidate)->first();
        if ($leadsurvey) {
            if ($request->flag == 0) {
                $leadsurvey->age_group = $request->age_group;
                $leadsurvey->gender = $request->gender;
                $leadsurvey->nationality = $request->nationality;
                $leadsurvey->current_location = $request->current_location;
                $leadsurvey->current_score = 0;
            } elseif ($request->flag == 1) {
                $leadsurvey->last_academic = $request->highest_academy;
                $leadsurvey->when_graduate = $request->when_graduate;
                $leadsurvey->current_score = 0;
            } elseif ($request->flag == 2) {
                $leadsurvey->area_study_interest = $request->course;
                $leadsurvey->study_destination = $request->destination;
                $leadsurvey->planning_apply = $request->time_apply;
                $leadsurvey->level_of_study = $request->level_study;

//                if (strcmp($leadsurvey->study_destination, 'Australia') == 0) {
//                    $leadsurvey->current_score += 1;
//                }
                // semua negara study destination dapet tambahan 1 point
                $leadsurvey->current_score += 1;

                if (strcmp($leadsurvey->planning_apply, 'Within 6 months') == 0) {
                    $leadsurvey->current_score += 3;
                }

                if (strcmp($leadsurvey->planning_apply, 'Within a year') == 0) {
                    $leadsurvey->current_score += 2;
                }

                if (strcmp($leadsurvey->planning_apply, 'Within 2 years') == 0) {
                    $leadsurvey->current_score += 1;
                }
            } elseif ($request->flag == 3) {
                $leadsurvey->ever_take_ielts = $request->ever_take_test;
                // $leadsurvey->current_score += $request->scoring;
            } elseif ($request->flag == 4) {
                $leadsurvey->test_taken = $request->test_type;
            } elseif ($request->flag == 5) {
                $leadsurvey->when_take_test = $request->when_take_test;
                if(strlen($request->test_score) > 0){
                    $leadsurvey->test_score = $request->test_score;
                }else{
                    $leadsurvey->test_score = 0;
                }

                if ($leadsurvey->when_take_test == '2017') {
                    $leadsurvey->current_score += 3;
                } elseif ($leadsurvey->when_take_test == '2016') {
                    $leadsurvey->current_score += 2;
                } elseif ($leadsurvey->when_take_test == ' 2015') {
                    $leadsurvey->current_score += 1;
                }
            } elseif ($request->flag == 6) {
                $leadsurvey->last_holiday = $request->last_holiday;
                $leadsurvey->close_family_overseas = $request->family_overseas;
                $leadsurvey->prefer_time = $request->prefer_time_called;
                $leadsurvey->come_to_expo = $request->come_to_expo;
                $leadsurvey->expo_name = $request->expo_name;
                $leadsurvey->scholarship = $request->scholarship;

                if (strcmp($leadsurvey->last_holiday, 'asia') == 0) {
                    $leadsurvey->current_score += 1;
                } elseif (strcmp($leadsurvey->last_holiday, 'europe') == 0) {
                    $leadsurvey->current_score += 3;
                } elseif (strcmp($leadsurvey->last_holiday, 'china') == 0) {
                    $leadsurvey->current_score += 2;
                } elseif (strcmp($leadsurvey->last_holiday, 'australia') == 0) {
                    $leadsurvey->current_score += 3; // change from 2
                } elseif (strcmp($leadsurvey->last_holiday, 'usa') == 0) {
                    $leadsurvey->current_score += 3;
                }

                if (strcmp($leadsurvey->close_family_overseas, 'yes') == 0) {
                    $leadsurvey->current_score += 1;
                }

                if(strcmp($leadsurvey->scholarship, 'no') == 0){
                    $leadsurvey->current_score = -1;
                }
            } elseif ($request->flag == 7) {
                if (strlen($request->additional_note) > 0) {
                    if (strpos($request->additional_note, 'scholarship') !== FALSE || strpos($request->additional_note, 'beasiswa') !== FALSE || strpos($request->additional_note, 'bea-siswa') !== FALSE) {

                        if ($leadsurvey->current_score > 0) {
                            $leadsurvey->current_score = ($leadsurvey->current_score - 1);
                        }
                    }

                    $leadsurvey->additional_note = $request->additional_note;
                }
                $leadsurvey->is_finish = 1;
            }

            $leadsurvey->save();





            $getleadinfo = Lead::where('id', $leadsurvey->lead_id)->first();
            if ($getleadinfo) {
                $getleadinfo->total_score = $leadsurvey->current_score;
                if ($request->flag == 7) {
                    $getleadinfo->is_finish = 1;
                }

                $getleadinfo->save();


                if ($request->flag == 0) {

                    $survey_data = [
                        'gender' => $leadsurvey->gender,
                        'nationality' => $leadsurvey->nationality,
                        'destination' => ''
                    ];

                    $this->sendApiLead($getleadinfo->email, $getleadinfo->first_name, $getleadinfo->last_name, $getleadinfo->mobile_phone, $getleadinfo->lead_source, 4, $leadsurvey->current_score, $getleadinfo->utm_source, $getleadinfo->campaign, $getleadinfo->adset, $getleadinfo->ads, $getleadinfo->utm_term, $survey_data);
                }


                if ($request->flag == 2) {

                    $survey_data = [
                        'gender' => $leadsurvey->gender,
                        'nationality' => $leadsurvey->nationality,
                        'destination' => $leadsurvey->study_destination
                    ];

                    $this->sendApiLead($getleadinfo->email, $getleadinfo->first_name, $getleadinfo->last_name, $getleadinfo->mobile_phone, $getleadinfo->lead_source, 4, $leadsurvey->current_score, $getleadinfo->utm_source, $getleadinfo->campaign, $getleadinfo->adset, $getleadinfo->ads, $getleadinfo->utm_term, $survey_data);
                }

                if ($request->flag == 7) {

                    $survey_data = [
                        'gender' => $leadsurvey->gender,
                        'nationality' => $leadsurvey->nationality,
                        'destination' => $leadsurvey->study_destination
                    ];

                    $this->sendApiLead($getleadinfo->email, $getleadinfo->first_name, $getleadinfo->last_name, $getleadinfo->mobile_phone, $getleadinfo->lead_source, 4, $leadsurvey->current_score, $getleadinfo->utm_source, $getleadinfo->campaign, $getleadinfo->adset, $getleadinfo->ads, $getleadinfo->utm_term, $survey_data);
                }
            }
        } else {
            exit;
        }
        echo 'score : ' . $leadsurvey->current_score;
    }

    public function viewThanks(Request $request) {

        $lang = NULL;
        $source = NULL;

        if (isset($request->lang)) {
            $lang = $request->lang;
        }

        if (isset($request->source)) {
            $source = $request->source;
        }

        return view('pages.thanks')->with([
                    'source' => $source,
                    'lang' => $lang,
        ]);
    }

    public function quickHome() {
        return view('pages.quickhome');
    }

    public function sendApiLead($email = FALSE, $first_name = FALSE, $last_name = FALSE, $mobile_number = FALSE, $lead_source = FALSE, $lead_rating = FALSE, $total_score = FALSE, $utm_source = FALSE, $utm_campaign = FALSE, $utm_adset = FALSE, $utm_ads = FALSE, $utm_term = FALSE, $survey_data = FALSE) {

        $getuser = Lead::where('email', $email)->first();
        $getutm_visitor = Utmvisitor::where('id', $getuser->utm_visitor_id)->first();

        $client_ip = '4';
        $country_code = 'ph';
        if ($getutm_visitor) {
            $client_ip = $getutm_visitor->client_ip;
            $country_code = $getutm_visitor->country_code;
        }

        $source_domain = "http://studyquery.com.ph/";
        $lead_source = "Study Query Philippines";

        // set post fields
        $post = array(
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'mobile_number' => $mobile_number,
            'lead_source' => $lead_source,
            'lead_rating' => $lead_rating,
            'total_score' => $total_score,
            'utm_source' => $utm_source,
            'utm_campaign' => $utm_campaign,
            'utm_adset' => $utm_adset,
            'utm_ads' => $utm_ads,
            'utm_term' => $utm_term,
            'country_code' => $country_code,
            'client_ip' => $client_ip,
            'survey_data' => $survey_data,
            'source_domain' => $source_domain
        );
        $post = json_encode($post);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fortrust.com/api/lead/save");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $response = curl_exec($ch);

        curl_close($ch);
        return;
    }

}
