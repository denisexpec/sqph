<?php

namespace App\Http\Middleware;

use Closure;

class CheckThanks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->valid){
            return $next($request);
        }

        return redirect('/');
    }
}
