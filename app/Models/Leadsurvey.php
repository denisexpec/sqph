<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leadsurvey extends Model
{
    use SoftDeletes;
    
    /**
     * Then table assoiated with the model.
     *
     * @var string
     */
    protected $table = 'leadsurveys';
    
    public function lead()
    {
        return $this->belongsTo('App\Models\Lead');
    }

}
