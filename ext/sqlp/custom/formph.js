var token = $('input[name=_token]').val();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

$('#dataForm').hide();
$('#buttonForm').hide();
$('#submitButton').prop("disabled", true);

$('#queryForm').change(function () {
    //first name validation
    if ($('#first_name').val() == '' || $('#first_name').val().length < 3 || $('#first_name').val().length > 40) {
        $('#errorMessage').text('First Name Length Must be 3 - 40.');
        $('#submitButton').prop("disabled", true);
    }
    //last name validation
    else if ($('#last_name').val() == '' || $('#last_name').val().length < 3 || $('#last_name').val().length > 80) {
        $('#errorMessage').text('Last Name Length Must be 3 - 40.');
        $('#submitButton').prop("disabled", true);
    }
    //email validation
    else if ($('#email').val() == '' || $('#email').val().length < 8 || $('#email').val().length > 80) {
        $('#errorMessage').text('Email Length Must be 8 - 80.');
        $('#submitButton').prop("disabled", true);
    }
    //email format validation
    else if (!regex.test($('#email').val())) {
        $('#errorMessage').text('Email Format is Wrong.');
        $('#submitButton').prop("disabled", true);
    }
    //mobile validation
    else if ($('#mobile').val() == '' || $('#mobile').val().length < 8 || $('#mobile').val().length > 40) {
        $('#errorMessage').text('Mobile Number Must be 8 - 40.');
        $('#submitButton').prop("disabled", true);
    }
    //school destination validation
    else if ($('#00N2800000DHSP8').val() == null) {
        $('#errorMessage').text('School Destination must be Selected.');
        $('#submitButton').prop("disabled", true);
    }
    //study area validation
    else if ($('#00N2800000DHSPD').val() == null) {
        $('#errorMessage').text('Study Area must be Selected.');
        $('#submitButton').prop("disabled", true);
    }
//    //best day validation
//    else if ($('#00N2800000DBUgW').val() == null) {
//        $('#errorMessage').text('Best Day must be selected.');
//        $('#submitButton').prop("disabled", true);
//    }
//    //best time validation
//    else if ($('#00N2800000DBUhA').val() == null) {
//        $('#errorMessage').text('Best Time must be selected.');
//        $('#submitButton').prop("disabled", true);
//    }
    //english level validation
    else if ($('#00N2800000IW6LA').val() == null) {
        $('#errorMessage').text('English Proficiency Level must be Selected.');
        $('#submitButton').prop("disabled", true);
    } 
    //Pass IELTS validation
    else if ($('#00N2800000IW6Lt').val() == null) {
        $('#errorMessage').text('IELTS Completion must be Selected.');
        $('#submitButton').prop("disabled", true);
    } 
    //School planning validation
    else if ($('#00N2800000IW6MS').val() == null) {
        $('#errorMessage').text('School Planning must be Selected.');
        $('#submitButton').prop("disabled", true);
    } 
    else {
        $('#errorMessage').text('All Done.');
        $('#submitButton').prop("disabled", false);
    }
});

$('#initialForm').click(function () {
    $('#dataForm').show();
    $('#buttonForm').show();
});
$('#closeForm').click(function () {
    $('#dataForm').hide();
    $('#buttonForm').hide();
});

$('#mobile').focus(function () {
    if ($(this).val() == '' || $(this).val().length < 3) {
        $(this).val('+63');
    }
});

$.get(
        "http://freegeoip.net/json/",
        {
        },
        function (data) {
            var countryCode = data.country_code;
            var country = data.country_name;
            $('#ip_address').val(data.ip);
            $('#00N2800000DHSPI').val(countryCode);
            $('#00N2800000DHSXm').val(country);
        }
);