var token = $('input[name=_token]').val();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

$('#dataForm').hide();
$('#buttonForm').hide();
$('#submitButton').prop("disabled", true);

$('#queryForm').change(function () {
    //first name validation
    if ($('#first_name').val() == '' || $('#first_name').val().length < 3 || $('#first_name').val().length > 40) {
        $('#errorMessage').text('Panjang Nama Depan harus 3 - 40.');
        $('#submitButton').prop("disabled", true);
    }
    //last name validation
    else if ($('#last_name').val() == '' || $('#last_name').val().length < 3 || $('#last_name').val().length > 80) {
        $('#errorMessage').text('Panjang Nama Belakang harus 3 - 40.');
        $('#submitButton').prop("disabled", true);
    }
    //email validation
    else if ($('#email').val() == '' || $('#email').val().length < 8 || $('#email').val().length > 80) {
        $('#errorMessage').text('Panjang Email harus 8 - 80.');
        $('#submitButton').prop("disabled", true);
    }
    //email format validation
    else if (!regex.test($('#email').val())) {
        $('#errorMessage').text('Format Email Salah.');
        $('#submitButton').prop("disabled", true);
    }
    //mobile validation
    else if ($('#mobile').val() == '' || $('#mobile').val().length < 8 || $('#mobile').val().length > 40) {
        $('#errorMessage').text('Panjang Nomor HP harus 8 - 40.');
        $('#submitButton').prop("disabled", true);
    }
    //school destination validation
    else if ($('#00N2800000DHSP8').val() == null) {
        $('#errorMessage').text('Destinasi Sekolah harus Dipilih.');
        $('#submitButton').prop("disabled", true);
    }
    //study area validation
    else if ($('#00N2800000DHSPD').val() == null) {
        $('#errorMessage').text('Jurusan harus Dipilih.');
        $('#submitButton').prop("disabled", true);
    }
//    //best day validation
//    else if ($('#00N2800000DBUgW').val() == null) {
//        $('#errorMessage').text('Hari untuk Dihubungi harus Dipilih.');
//        $('#submitButton').prop("disabled", true);
//    }
//    //best time validation
//    else if ($('#00N2800000DBUhA').val() == null) {
//        $('#errorMessage').text('Waktu untuk Dihubungi harus Dipilih.');
//        $('#submitButton').prop("disabled", true);
//    } 
    //english level validation
    else if ($('#00N2800000IW6LA').val() == null) {
        $('#errorMessage').text('Tingkat Kemahiran Bahasa Inggris harus Dipilih.');
        $('#submitButton').prop("disabled", true);
    } 
    //Pass IELTS validation
    else if ($('#00N2800000IW6Lt').val() == null) {
        $('#errorMessage').text('Kelulusan Ujian IELTS harus Dipilih.');
        $('#submitButton').prop("disabled", true);
    } 
    //School planning validation
    else if ($('#00N2800000IW6MS').val() == null) {
        $('#errorMessage').text('Waktu Rencana Mulai Sekolah harus Dipilih.');
        $('#submitButton').prop("disabled", true);
    } 
    else {
        $('#errorMessage').text('Semua Selesai.');
        $('#submitButton').prop("disabled", false);
    }
});

$('#initialForm').click(function () {
    $('#dataForm').show();
    $('#buttonForm').show();
});
$('#closeForm').click(function () {
    $('#dataForm').hide();
    $('#buttonForm').hide();
});

$('#mobile').focus(function () {
    if ($(this).val() == '' || $(this).val().length < 3) {
        $(this).val('+62');
    }
});

$.get(
        "http://freegeoip.net/json/",
        {
        },
        function (data) {
            var countryCode = data.country_code;
            var country = data.country_name;
            $('#ip_address').val(data.ip);
            $('#00N2800000DHSPI').val(countryCode);
            $('#00N2800000DHSXm').val(country);
        }
);