
$('#survey03, #survey04, #survey05, #survey06, #survey07, #survey08, #survey09, #survey10, #survey11, #survey12, #survey13, #survey14' ).hide()

var url_form = '/form_question';
// var url_email = '/submitSF';
var url_thanks = '/thanks';

var overall_score = 0; // first declare of total scoring variable

function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 100) input.value = 100;
    console.log(input);
}

function isNumber (o) {
    return ! isNaN (o-0);
}

$(".toefl_ielts_score").keyup(function(e){
    txtVal = $(this).val();
    if(isNumber(txtVal) && txtVal.length >2)
    {
        $(this).val(txtVal.substring(0,3) )
    }
});

// $("#survey01 .btnn").click(function(){
//     $( "#survey01" ).fadeOut(300);
//     $( "#survey02" ).delay(300).fadeIn(800);
// });

var id_user_candidate = $('#id_user_candidate').val();
var total_score = 0;
var active = false;

console.log(total_score);


function request_ajax(data_content){

    var request = $.ajax({
        url: window.location.origin+url_form,
        method: "POST",
        data: data_content,
        dataType: "text",
        async: false,
    });

    return request;
}

//Prelim Question
$("#survey02 .btnn").click(function(){

    if(active){
        return;
    }

    var age_group = $('select[name=age_group]').val();
    var gender = $('select[name=gender]').val();
    var nationality = $('select[name=nationality]').val();
    var current_location = $('select[name=current_country]').val();

    console.log('age_group = ' + age_group);
    console.log('gender = '+ gender);
    console.log('nationality = '+nationality);
    console.log('id user candidate = '+ id_user_candidate);

    var score_age_group = 0;
    if(age_group === '18-24'){
        // score_age_group = 2;
    }else if(age_group === '25-34'){
        // score_age_group = 1;
    }

    // total_score = total_score + score_age_group;

    var survey02 = {
        'id_user_candidate' : id_user_candidate,
        'age_group' : age_group,
        'gender' : gender,
        'nationality' : nationality,
        'current_location' : current_location,
        'scoring' : score_age_group,
        '_token' : $('meta[name="_token"]').attr('content'),
        'flag' : 0,
        // 'total_score' : total_score,
    };

    console.log('total score prelim = '+total_score);

    active = true;
    var rq = request_ajax(survey02);
    rq.done(function( msg ) {
        active=false;
        console.log(msg);
        $( "#survey02" ).fadeOut(300);
        $( "#survey03" ).delay(300).fadeIn(800);
    });


});


//Survey 01
$("#Q3A1").click(function(){

    // var score_choice = parseFloat($(this).attr('data-score'));
    // total_score = total_score + score_choice;
    // console.log('pilihan pertama = '+total_score);

    var highest_academy = $('select[name=highest_academy]').val();
    var when_graduate = $('select[name=when_graduate]').val();

    var score_choice = 0;
    if(when_graduate == 2017){
        score_choice = 0;
    }else if(when_graduate == 2016){
        score_choice = 0;
    }else if(when_graduate == 2015){
        score_choice = 0;
    }else{
        score_choice = 0;
    }


    var survey_data = {
        'id_user_candidate' : id_user_candidate,
        'scoring' : score_choice,
        '_token' : $('meta[name="_token"]').attr('content'),
        'flag' : 1,
        // 'total_score' : total_score,
        'highest_academy' : highest_academy,
        'when_graduate' : when_graduate
    };

    active = true;
    var rq = request_ajax(survey_data);
    rq.done(function( msg ) {
        active = false;
        $( "#survey03" ).fadeOut(300);
        $( "#survey05" ).delay(300).fadeIn(800);
    });
});



//survey 03
$("#Q5A1").click(function(){

    $('.destination_survey03').addClass('hidden');
    $('.course_survey03').addClass('hidden');
    $('.time_survey03').addClass('hidden');



    var destination = $('select[name=destination_university_survey03]').val();
    var course = $('select[name=course_university_survey03]').val();
    var time_apply = $('.time_apply_survey03').find('.active').attr('data-value');
    var level_study = $('.level_study').find('.active').attr('data-value');

    var get_score = $('.time_apply_survey03').find('.active').attr('data-score');

    console.log('time apply score = '+get_score);

    if(course === null){
        $('.course_survey03').removeClass('hidden');
    }else if(destination === null){
        $('.destination_survey03').removeClass('hidden');
    }else if( time_apply === undefined || time_apply.length === 0){
        $('.time_survey03').removeClass('hidden');
    }else{

        // var score_time = parseFloat($('.time_apply_survey03').find('.active').attr('data-score'));
        // total_score = total_score + score_time;
        // console.log('time apply 1 = '+total_score);


        // if(destination == 'Australia' || destination == 'australia'){
        //     get_score =  parseInt(get_score) + 1;
        // }
        // semua negara destination dapet 1 point
        get_score =  parseInt(get_score) + 1;
        //
        overall_score = overall_score + parseInt(get_score);
        console.log("get_score_1 = "+get_score);
        console.log("overall_score = "+overall_score);

        var survey_data = {
            'id_user_candidate' : id_user_candidate,
            '_token' : $('meta[name="_token"]').attr('content'),
            'flag' : 2,
            'destination' : destination,
            'course' : course,
            'time_apply' : time_apply,
            'level_study' : level_study,
            'scoring' : overall_score,
            // 'total_score' : total_score,
        };

        var rq = request_ajax(survey_data);
        rq.done(function( msg ) {
            $( "#survey05" ).fadeOut(300);
            $( "#survey10" ).delay(300).fadeIn(800);

        });
    }

});

// radio button handler
$(".expo_selection_y").on('click change', function(){
    $('.expo_name').show();
});

$(".expo_selection_n").on('click change', function(){
    $('.expo_name').hide();
    if($('.expo_name_err').hasClass("hidden") == false){
        $( ".expo_name_err" ).addClass( "hidden" );
    }
});

$("#Q9A1").click(function(){

    var last_holiday = $('select[name=where_last_holiday]').val();
    var family_overseas = $('.family_overseas').find('.active').attr('data-value');
    var prefer_time_called = $('.prefer_time_called').find('.active').attr('data-value');
    var come_to_expo = $('.come_to_expo').find('.active').attr('data-value');
    var expo_name = $('.expo_name').val();
    var scholarship = $('.scholarship').find('.active').attr('data-value');

    var get_score = 0;
    if(last_holiday == 'asia' || last_holiday == 'Asia'){
        get_score = 1;
    }else if(last_holiday == 'china' || last_holiday == 'China'){
        get_score = 2;
    }else if(last_holiday == 'europe' || last_holiday == 'Europe'){
        get_score = 3;
    }else if(last_holiday == 'australia' || last_holiday == 'Australia'){
        get_score = 3;
    }else if(last_holiday == 'usa' || last_holiday == 'USA'){
        get_score = 3;
    }else if(last_holiday == 'others' || last_holiday == 'Others'){
        get_score = 0;
    }else{
        get_score = 0;
    }


    if(family_overseas == 'yes'){
        get_score = parseInt(get_score) + 2;
    }

    if(scholarship == 'no'){
        overall_score = parseInt(-1);
    }else{
        overall_score = overall_score + parseInt(get_score);
    }

    console.log("scholarship_"+scholarship+"="+overall_score);
    console.log("get_score_1 = "+get_score);
    console.log("overall_score = "+overall_score);

    if(come_to_expo == 'yes' && expo_name == '') {
        $(".expo_name_err").removeClass('hidden');
        $(".expo_name").focus();
    }else {

        var survey_data = {
            'id_user_candidate': id_user_candidate,
            'scoring': overall_score,
            '_token': $('meta[name="_token"]').attr('content'),
            'flag': 6,
            // 'toefl_ielts_test' : 'yes',
            // 'total_score' : total_score,
            'last_holiday': last_holiday,
            'family_overseas': family_overseas,
            'prefer_time_called': prefer_time_called,
            'come_to_expo': come_to_expo,
            'expo_name': expo_name,
            'scholarship': scholarship
        };

        console.log("form=" + survey_data);

        var rq = request_ajax(survey_data);
        rq.done(function (msg) {
            $("#survey09").fadeOut(300);
            $("#survey11").delay(300).fadeIn(800);
        });
    } // end else

});


$('#Q10A1').click(function(){

    var survey_data = {
        'id_user_candidate' : id_user_candidate,
        'scoring' : 1,
        '_token' : $('meta[name="_token"]').attr('content'),
        'flag' : 3,
        'ever_take_test' : 'yes',
    };

    var rq = request_ajax(survey_data);
    rq.done(function( msg ) {
        $( "#survey10" ).fadeOut(300);
        $( "#survey12" ).delay(300).fadeIn(800);
    });

});

$('#Q10A2').click(function(){
    var survey_data = {
        'id_user_candidate' : id_user_candidate,
        'scoring' : 0,
        '_token' : $('meta[name="_token"]').attr('content'),
        'flag' : 3,
        'ever_take_test' : 'no',
    };

    var rq = request_ajax(survey_data);
    rq.done(function( msg ) {
        $( "#survey10" ).fadeOut(300);
        $( "#survey09" ).delay(300).fadeIn(800);
    });
});

$('#Q12A1').click(function(){
    var survey_data = {
        'id_user_candidate' : id_user_candidate,
        // 'scoring' : 0,
        '_token' : $('meta[name="_token"]').attr('content'),
        'flag' : 4,
        'test_type' : 'IELTS',
    };

    var rq = request_ajax(survey_data);
    rq.done(function( msg ) {
        $( "#survey12" ).fadeOut(300);
        $( "#survey14" ).delay(300).fadeIn(800);
    });
});

$('#Q12A2').click(function(){

    var survey_data = {
        'id_user_candidate' : id_user_candidate,
        // 'scoring' : 0,
        '_token' : $('meta[name="_token"]').attr('content'),
        'flag' : 4,
        'test_type' : 'TOEFL',
    };

    var rq = request_ajax(survey_data);
    rq.done(function( msg ) {
        $( "#survey12" ).fadeOut(300);
        $( "#survey14" ).delay(300).fadeIn(800);
    });
});

$('#Q11A1').click(function(){

    var get_note = $('textarea[name=anything_to_add]').val();

    if(get_note == ''){
        $('.additional_info_error').removeClass('hidden');
        $('textarea[name=anything_to_add]').focus();
    }else{

        console.log('note=' + get_note);


        var survey_data = {
            'id_user_candidate': id_user_candidate,
            // 'scoring' : 0,
            '_token': $('meta[name="_token"]').attr('content'),
            'flag': 7,
            'additional_note': get_note,
            'is_finish': 1,
        };

        var rq = request_ajax(survey_data);
        rq.done(function (msg) {
            $("#survey11").fadeOut(300);

            window.location = window.location.origin + url_thanks;
        });
    } // end else
})


//survey 08
$("#Q14A1").click(function(){

    var time_apply_toefl = $('.time_apply_toefl').find('.active').attr('data-value');

    console.log('time_apply_toefl =' + time_apply_toefl);

    if(time_apply_toefl === undefined || time_apply_toefl.length === 0  ){
        $('.toefl_ielts_time').removeClass('hidden');
    }else{
        var toefl_score = $('.toefl_ielts_score').val();
        // var score_toefl = 0;

        // if(toefl_score){
        // score_toefl = 1;
        // }else{
        // toefl_score = 0;
        // }
        var get_score = 0;
        if(time_apply_toefl == 2017){
            get_score = 2;
        }else if(time_apply_toefl == 2016){
            get_score = 1;
        }


        // $('.toefl_ielts_time').addClass('hidden');

        // var scoring_survey = score_toefl + time_score_apply;

        // overall_score = overall_score + parseInt(get_score);
        console.log("get_score_1 = "+get_score);
        console.log("overall_score = "+overall_score);

        var survey_data = {
            'id_user_candidate' : id_user_candidate,
            //'scoring' : get_score,
            '_token' : $('meta[name="_token"]').attr('content'),
            'flag' : 5,
            'test_score' : toefl_score,
            'when_take_test' : time_apply_toefl,
        };

        var rq = request_ajax(survey_data);
        rq.done(function(e){
            console.log('bitrix online');
            $("#survey14").fadeOut(300);
            $("#survey09").delay(300).fadeIn(800);

        });

    }

});



var picked = {
    background : '#0B5D62',
    color : '#fff'
}

$('.select').on('click', function(){
    $(this).addClass("active").siblings().removeClass("active");
    $(this).siblings().removeAttr('style');
    $(this).css(picked);
});

$('.check').click(function() {
    $( this ).toggleClass( "active" );
}); 