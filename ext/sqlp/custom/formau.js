var token = $('input[name=_token]').val();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

$('#dataForm').hide();
$('#buttonForm').hide();

$('#queryForm').change(function () {
    //first name validation
    if ($('#first_name').val() == '' || $('#first_name').val().length < 3 || $('#first_name').val().length > 40) {
        $('#errorMessage').text('First Name Length Must be 3 - 40.');
    }
    //last name validation
    else if ($('#last_name').val() == '' || $('#last_name').val().length < 3 || $('#last_name').val().length > 80) {
        $('#errorMessage').text('Last Name Length Must be 3 - 40.');
    }
    //email validation
    else if ($('#email').val() == '' || $('#email').val().length < 8 || $('#email').val().length > 80) {
        $('#errorMessage').text('Email Length Must be 8 - 80.');
    }
    //email format validation
    else if (!regex.test($('#email').val())) {
        $('#errorMessage').text('Email Format is Wrong.');
    }
    //mobile validation
    else if ($('#mobile').val() == '' || $('#mobile').val().length < 8 || $('#mobile').val().length > 40) {
        $('#errorMessage').text('Mobile Number Must be 8 - 40.');
    }
    else {
        $('#errorMessage').text('All Done.');
        $('#submitButton').prop("disabled", false);
    }
});

$('#initialForm').click(function () {
    $('#dataForm').show();
    $('#buttonForm').show();
});
$('#closeForm').click(function () {
    $('#dataForm').hide();
    $('#buttonForm').hide();
});

$('#mobile').focus(function () {
    if ($(this).val() == '' || $(this).val().length < 3) {
        $(this).val('+61');
    }
});

