-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2017 at 02:57 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sqiddb`
--

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lead_source` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lead_rating` tinyint(4) NOT NULL DEFAULT '0',
  `note_counselor` text COLLATE utf8_unicode_ci,
  `utm_source` text COLLATE utf8_unicode_ci,
  `campaign` text COLLATE utf8_unicode_ci,
  `adset` text COLLATE utf8_unicode_ci,
  `ads` text COLLATE utf8_unicode_ci,
  `is_followed_up` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_group` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser_version` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser_family` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_mobile` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_tablet` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_desktop` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_bot` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os_family` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os_version` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_family` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_model` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `css_version` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_grade` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_finish` tinyint(4) NOT NULL DEFAULT '0',
  `email_sent` tinyint(4) NOT NULL DEFAULT '0',
  `email_sent_to_candidate` tinyint(4) NOT NULL DEFAULT '0',
  `total_score` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `first_name`, `last_name`, `full_name`, `mobile_phone`, `email`, `lead_source`, `lead_rating`, `note_counselor`, `utm_source`, `campaign`, `adset`, `ads`, `is_followed_up`, `nationality`, `country_group`, `gender`, `ip_address`, `browser_name`, `browser_version`, `browser_family`, `is_mobile`, `is_tablet`, `is_desktop`, `is_bot`, `os_family`, `os_version`, `os_name`, `device_family`, `device_model`, `css_version`, `mobile_grade`, `is_finish`, `email_sent`, `email_sent_to_candidate`, `total_score`, `created_at`, `updated_at`, `deleted_at`) VALUES
(80, 'Supri', 'Yani', 'Supri Yani', '', 'aninabuwbuw@yahoo.co.id', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsindo', NULL, NULL, 'id', NULL, '120.188.77.76', 'Android 4', '4', 'Android', '0', '1', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', 'SamsungTablet', '3', 'A', 0, 0, 0, 5, '2017-07-20 08:56:48', '2017-07-20 08:56:48', NULL),
(82, 'Nikolaus ', 'Ao', 'Nikolaus  Ao', '+628123985669', 'nicov69@yahoo.co.id', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsindo', NULL, NULL, 'id', NULL, '114.125.69.233', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, NULL, '2017-07-20 09:46:32', '2017-07-20 09:46:32', NULL),
(83, 'Misbah', 'Destiady', 'Misbah Destiady', '+6285260278769', 'misbah_destiady@yahoo.com', 'student', 0, NULL, 'direct', NULL, NULL, NULL, NULL, NULL, 'id', NULL, '180.241.13.106', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, NULL, '2017-07-20 11:11:48', '2017-07-20 11:11:48', NULL),
(84, 'Andrian', 'Basir', 'Andrian Basir', '+6282272424989', 'aqua_lapandua@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_carousel', NULL, NULL, 'id', NULL, '114.125.164.252', 'Safari', '', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, NULL, '2017-07-20 12:34:52', '2017-07-20 12:34:52', NULL),
(88, 'hayuningrum citra', 'maharsi', 'hayuningrum citra maharsi', '+6282154541151', 'cececitra@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '118.97.57.11', 'Safari', '', 'Safari', '0', '0', '1', '0', 'MacOSX', '', 'MacOSX', '', '', '1', '', 0, 0, 0, 9, '2017-07-20 14:15:14', '2017-07-20 14:16:38', NULL),
(89, 'Irmania', 'Wahyuningtyas', 'Irmania Wahyuningtyas', '+6281217279270', 'irmaniawahyu.22@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '120.188.74.135', 'Safari 4', '4', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', '', '3', 'A', 0, 0, 0, NULL, '2017-07-20 14:29:04', '2017-07-20 14:37:53', NULL),
(90, 'Tamara', 'Gheraldyne de amira', 'Tamara Gheraldyne de amira', '+6281333121197', 'tamaragheraldyne15@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '180.251.80.151', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 6, '2017-07-20 14:31:25', '2017-07-20 14:35:19', NULL),
(91, 'Muhammad ', 'Sabarudin', 'Muhammad  Sabarudin', '+6282340210995', 'jtaradoz@gmail.com', 'parent', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '203.78.117.221', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 3, '2017-07-20 15:16:00', '2017-07-20 15:17:07', NULL),
(92, 'Elan Rahmawan', 'Fachrozi', 'Elan Rahmawan Fachrozi', '+6281297069643', 'vfachrozi@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '180.244.137.111', 'Safari', '', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-20 22:03:19', '2017-07-20 22:03:58', NULL),
(93, 'Agung rahmat ', 'fauzi', 'Agung rahmat  fauzi', '+6282251000065', 'rfagung@gmail.com', 'student', 0, NULL, 'direct', NULL, NULL, NULL, NULL, NULL, 'id', NULL, '112.215.241.30', 'Safari', '', 'Safari', '0', '0', '1', '0', 'MacOSX', '', 'MacOSX', '', '', '1', '', 0, 0, 0, NULL, '2017-07-21 06:07:15', '2017-07-21 06:07:15', NULL),
(94, 'Ribut', 'Riyanto', 'Ribut Riyanto', '+6282394703464', 'ributriyanto24@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsindo', NULL, NULL, 'id', NULL, '114.125.220.76', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-21 09:10:36', '2017-07-21 09:11:07', NULL),
(95, 'Katherine', 'Merryana', 'Katherine Merryana', '+6281260362419', 'kath.shn79@icloud.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.43.202', 'Safari', '', 'Safari', '1', '0', '0', '0', 'iOS', '', 'iOS', 'iPhone', 'iPhone', '3', 'A', 0, 0, 0, 5, '2017-07-21 10:45:37', '2017-07-21 10:48:52', NULL),
(96, 'Adinda', 'Siregar', 'Adinda Siregar', '+62 822-9403-3346', 'adindasakinah14@gmail.com', 'direct', 0, NULL, 'direct', NULL, NULL, NULL, NULL, NULL, 'id', NULL, '114.125.21.217', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-21 10:59:07', '2017-07-21 11:00:32', NULL),
(97, 'Nafis', 'Khoiry', 'Nafis Khoiry', '+6289608184626', 'nafisaza@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '202.67.40.195', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 2, '2017-07-21 11:10:07', '2017-07-21 11:40:54', NULL),
(98, 'ANGGRAENI PUSPA', 'DEWI', 'ANGGRAENI PUSPA DEWI', '+628974011455', 'anggraenipd1@gmail.com', 'direct', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '116.206.28.29', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', 'Samsung', '3', 'A', 0, 0, 0, 6, '2017-07-21 11:13:04', '2017-07-21 11:42:59', NULL),
(99, 'ANGGRAENI PUSPA', 'DEWI', 'ANGGRAENI PUSPA DEWI', '08974011455', 'anggraenipd1@yahoo.co.id', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '107.167.113.132', 'Opera', '', 'Opera', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', '', '1', 'A', 0, 0, 0, NULL, '2017-07-21 11:20:15', '2017-07-21 11:22:44', NULL),
(100, 'Ali', 'Ali zamri.s', 'Ali Ali zamri.s', '+6282399469224', 'alizamri95@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_staticsindo', NULL, NULL, 'id', NULL, '36.83.209.154', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 3, '2017-07-21 13:07:01', '2017-07-21 13:07:40', NULL),
(101, 'Firman', 'Syah', 'Firman Syah', '+62082316666248', 'syahf776@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.218.35', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-21 14:32:32', '2017-07-21 14:33:53', NULL),
(102, 'Tirta', 'Bil sagara', 'Tirta Bil sagara', '+62085761596513', 'tirtabilsagara28@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_staticsindo', NULL, NULL, 'id', NULL, '114.125.31.162', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-21 14:54:10', '2017-07-21 14:54:32', NULL),
(103, 'heri', 'sunandar', 'heri sunandar', '+6282161709754', 'herisunandar433@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '180.241.83.71', 'Safari 4', '4', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', '', '3', 'A', 0, 0, 0, NULL, '2017-07-21 15:06:10', '2017-07-21 15:06:10', NULL),
(104, 'sarah', 'anastasha', 'sarah anastasha', '+6281283356607', 'sarahsarr06@yahoo.co.id', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '61.5.31.55', 'Chrome', '', 'Chrome', '0', '0', '1', '0', 'MacOSX', '', 'MacOSX', '', '', '1', '', 0, 0, 0, 5, '2017-07-21 15:25:50', '2017-07-21 15:26:29', NULL),
(105, 'Irma ', 'khikmawati', 'Irma  khikmawati', '+6282326534242', 'irmakhikmawati@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '180.254.247.177', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 5, '2017-07-21 15:26:19', '2017-07-21 15:26:54', NULL),
(106, 'Ryandika', 'Mega ', 'Ryandika Mega ', '+6285752455314', 'ryandikamega@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_carousel', NULL, NULL, 'id', NULL, '180.248.247.1', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 6, '2017-07-21 15:42:34', '2017-07-21 15:43:39', NULL),
(107, 'danny', 'IgnoreMe', 'danny IgnoreMe', '+62876545678765', 'danny@expecto.io', 'direct', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'id', NULL, '139.228.188.199', 'Safari', '', 'Safari', '0', '0', '1', '0', 'MacOSX', '', 'MacOSX', '', '', '1', '', 0, 0, 0, NULL, '2017-07-21 16:48:47', '2017-07-21 16:48:47', NULL),
(108, 'nadiah', 'khansa', 'nadiah khansa', '081380618246', 'nadiahzafirah8@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '223.255.230.35', 'HiSoRange Generic Browser', '', 'HiSoRange Generic Browser', '0', '0', '1', '0', 'HiSoRange Generic OS', '', 'HiSoRange Generic OS', '', '', '0', '', 0, 0, 0, 3, '2017-07-21 18:12:10', '2017-07-21 18:12:44', NULL),
(109, 'Bagus', 'Fahmi', 'Bagus Fahmi', '+6282283718190', 'Bagusfw48@gmail.com', 'parent', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.16.255', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 7, '2017-07-21 19:24:53', '2017-07-21 19:26:32', NULL),
(110, 'fahrul', 'ruzi', 'fahrul ruzi', '+622143375654', 'fahrulruzi357@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '36.79.221.221', 'Chrome 4', '4', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', '', '3', 'A', 0, 0, 0, 6, '2017-07-21 19:33:56', '2017-07-21 19:35:38', NULL),
(111, 'Herman', 'Felani', 'Herman Felani', '085757047813', 'herman89gmail@yahoo.com', 'parent', 0, NULL, 'facebook', 'SQI_traffic', 'idpr', 'pr_statics2', NULL, NULL, 'id', NULL, '114.125.165.177', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, NULL, '2017-07-21 20:07:10', '2017-07-21 20:07:10', NULL),
(112, 'Ansel', 'Silva', 'Ansel Silva', '+6295704930140', 'anselsilva@yahoo.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '202.67.35.28', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-21 20:36:56', '2017-07-21 20:37:42', NULL),
(113, 'Venny', 'Veronica', 'Venny Veronica', '+6287780686867', 'vennyveronica26@yahoo.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '115.178.195.219', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', 'Samsung', '3', 'A', 0, 0, 0, 6, '2017-07-21 21:07:19', '2017-07-21 21:08:26', NULL),
(114, 'Muhammad Daffa', 'Al-Farizi', 'Muhammad Daffa Al-Farizi', '+622191984454', 'daffajazi25@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '116.206.33.48', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, NULL, '2017-07-21 21:30:21', '2017-07-21 21:31:16', NULL),
(115, 'Syifa', 'Khoerunnisa', 'Syifa Khoerunnisa', '+625603748117', 'syifakhoerunnisa544@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.124.142.192', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-21 21:43:09', '2017-07-21 21:44:03', NULL),
(116, 'Nadhiba', 'Zahra', 'Nadhiba Zahra', '16', '319.nadhiba@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '202.67.40.209', 'HiSoRange Generic Browser', '', 'HiSoRange Generic Browser', '0', '0', '1', '0', 'HiSoRange Generic OS', '', 'HiSoRange Generic OS', '', '', '0', '', 0, 0, 0, 10, '2017-07-21 22:47:22', '2017-07-21 23:11:35', NULL),
(117, 'Roy Rorot ', 'Saragih', 'Roy Rorot  Saragih', '+6285283411115', 'turniproy@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '36.68.126.95', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 3, '2017-07-21 23:27:44', '2017-07-21 23:29:11', NULL),
(118, 'kurnia', 'wahidia', 'kurnia wahidia', '+622958006218', 'kkwhdy15@gmail.com', 'parent', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '203.78.119.154', 'Chrome', '', 'Chrome', '0', '0', '1', '0', 'Win7', '', 'Win7', '', '', '1', '', 0, 0, 0, 6, '2017-07-21 23:32:34', '2017-07-21 23:33:56', NULL),
(119, 'Andika', 'Zulkarnain', 'Andika Zulkarnain', '+628111095726', 'andika.zulkarnain270190@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '125.161.43.155', 'HiSoRange Generic Browser', '', 'HiSoRange Generic Browser', '0', '0', '1', '0', 'HiSoRange Generic OS', '', 'HiSoRange Generic OS', '', '', '0', '', 0, 0, 0, 3, '2017-07-22 07:14:28', '2017-07-22 07:20:22', NULL),
(120, 'riezkina', 'putri', 'riezkina putri', '+6281219695820', 'awkinaa@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '111.95.43.10', 'Safari', '', 'Safari', '0', '0', '1', '0', 'MacOSX', '', 'MacOSX', '', '', '1', '', 0, 0, 0, 6, '2017-07-22 09:16:10', '2017-07-22 09:17:03', NULL),
(121, 'Aldi Krisna', 'Arsana', 'Aldi Krisna Arsana', '+6287761100277', 'aldikrisnaarsana@gmail.com', 'direct', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'id', NULL, '203.128.94.198', 'Firefox', '', 'Firefox', '0', '0', '1', '0', 'Win7', '', 'Win7', 'PC', '', '3', '', 0, 0, 0, 7, '2017-07-22 10:43:38', '2017-07-22 10:46:03', NULL),
(122, 'Novia', 'Pangestika', 'Novia Pangestika', '+6282134338150', 'novia.pangestika63@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '202.67.41.12', 'Firefox', '', 'Firefox', '0', '0', '1', '0', 'Win7', '', 'Win7', 'PC', '', '3', '', 0, 0, 0, 5, '2017-07-22 10:53:22', '2017-07-22 10:55:05', NULL),
(123, 'Cicylia', 'Samalo', 'Cicylia Samalo', '+6281337413943', 'cicylia44@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.103.166', 'Chrome', '', 'Chrome', '0', '0', '1', '0', 'Win8.1', '', 'Win8.1', '', '', '1', '', 0, 0, 0, 4, '2017-07-22 11:18:16', '2017-07-22 11:22:46', NULL),
(124, 'Yasmin', 'Athirah', 'Yasmin Athirah', '+6285732006827', 'yasminathiirah@gmail.com', 'student', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'id', NULL, '120.188.87.105', 'Safari', '', 'Safari', '1', '0', '0', '0', 'iOS', '', 'iOS', 'iPhone', 'iPhone', '3', 'A', 0, 0, 0, 13, '2017-07-22 11:42:42', '2017-07-22 11:44:16', NULL),
(125, 'Sri', 'Agustina', 'Sri Agustina', '+6283805654644', 'Sriagustina072@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '120.188.38.74', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-22 12:28:46', '2017-07-22 12:29:35', NULL),
(126, 'Febi', 'Oktavia', 'Febi Oktavia', '+6281288135260', 'febiioktavia@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '103.228.246.148', 'Chrome', '', 'Chrome', '0', '0', '1', '0', 'Win7', '', 'Win7', '', '', '1', '', 0, 0, 0, 2, '2017-07-22 13:04:34', '2017-07-22 13:04:50', NULL),
(127, 'Naura', 'Fathiya', 'Naura Fathiya', '+6289626585988', 'naurafathiya10@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '168.235.198.79', 'Safari', '', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 6, '2017-07-22 14:33:33', '2017-07-22 14:34:59', NULL),
(128, 'Bhaga', 'Prabhasa', 'Bhaga Prabhasa', '+6289601303709', 'bagas891@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '180.254.227.142', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 6, '2017-07-22 15:46:50', '2017-07-22 15:48:26', NULL),
(129, 'Nurhasia', 'nur', 'Nurhasia nur', '+6211', 'nurhasia01@gmail.com', 'student', 0, NULL, 'facebook', NULL, NULL, NULL, NULL, NULL, 'id', NULL, '114.125.171.112', 'Chrome 4', '4', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', '', '3', 'A', 0, 0, 0, NULL, '2017-07-22 21:37:43', '2017-07-22 21:37:44', NULL),
(130, 'Anastasia ', 'Sitorus', 'Anastasia  Sitorus', '+6281808487525', 'anastasiaflorensia@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '139.194.228.75', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 4, '2017-07-22 23:31:51', '2017-07-22 23:32:48', NULL),
(131, 'Novita Aristha Anggreni', 'Nainggolan', 'Novita Aristha Anggreni Nainggolan', '+6281298767473', 'naristha16@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.86.203', 'HiSoRange Generic Browser', '', 'HiSoRange Generic Browser', '0', '0', '1', '0', 'HiSoRange Generic OS', '', 'HiSoRange Generic OS', '', '', '0', '', 0, 0, 0, 4, '2017-07-23 00:06:37', '2017-07-23 00:07:35', NULL),
(132, 'frengki', 'ky', 'frengki ky', '+6281351747633', 'ahwei.22.aw@gmail.com', 'parent', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '139.194.172.11', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 7, '2017-07-23 01:52:09', '2017-07-23 01:54:12', NULL),
(133, 'Yuli Yanna', 'Fauzie', 'Yuli Yanna Fauzie', '+6282112070930', 'yoeliyannaf@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.124.141.171', 'Safari', '', 'Safari', '1', '0', '0', '0', 'iOS', '', 'iOS', 'iPhone', 'iPhone', '3', 'A', 0, 0, 0, 7, '2017-07-23 01:56:54', '2017-07-23 01:58:59', NULL),
(134, 'josep wanma', 'josep', 'josep wanma josep', '+622342415414', 'andreawanma@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '168.235.205.16', 'Safari 4', '4', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', 'Android', '', '3', 'A', 0, 0, 0, 6, '2017-07-23 02:04:45', '2017-07-23 02:08:09', NULL),
(135, 'Fauzul', 'Adzim', 'Fauzul Adzim', '+6283806938632', 'fadzim98@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '112.215.236.11', 'Safari', '', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-23 02:16:24', '2017-07-23 02:17:01', NULL),
(136, 'Dini', 'Safitri', 'Dini Safitri', '+6281261230799', 'dinisafitri286@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.42.179', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 2, '2017-07-23 04:37:11', '2017-07-23 04:38:51', NULL),
(137, 'Silvani', 'Putry', 'Silvani Putry', '082115890954', 'silvanipputry68@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.217.188', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 6, '2017-07-23 05:21:46', '2017-07-23 05:23:16', NULL),
(138, 'rizqi', 'agustin', 'rizqi agustin', '08213795574', 'rizqinannn@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '112.215.240.29', 'HiSoRange Generic Browser', '', 'HiSoRange Generic Browser', '0', '0', '1', '0', 'HiSoRange Generic OS', '', 'HiSoRange Generic OS', '', '', '0', '', 0, 0, 0, 5, '2017-07-23 07:25:30', '2017-07-23 07:26:13', NULL),
(139, 'Dinda Shadrina', 'Malik', 'Dinda Shadrina Malik', '+6281213441195', 'dindashadrina@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '139.192.151.223', 'Safari', '', 'Safari', '1', '0', '0', '0', 'iOS', '', 'iOS', 'iPhone', 'iPhone', '3', 'A', 0, 0, 0, 8, '2017-07-23 08:04:45', '2017-07-23 08:06:50', NULL),
(140, 'Khairun', 'Nisa', 'Khairun Nisa', '+6282174445994', 'khairunisa424@gmail.com', 'parent', 0, NULL, 'facebook', 'SQI_traffic', 'idpr', 'pr_statics2', NULL, NULL, 'id', NULL, '180.241.148.216', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 8, '2017-07-23 08:26:51', '2017-07-23 08:35:56', NULL),
(141, 'Juicy', 'Lowise', 'Juicy Lowise', '+6281278353383', 'rapunzelpapaso15@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '202.80.216.234', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', 'Samsung', '0', 'A', 0, 0, 0, 6, '2017-07-23 08:39:56', '2017-07-23 08:42:43', NULL),
(142, 'Asril', 'Majid amrillah', 'Asril Majid amrillah', '+62087864194332', 'bajanglama89@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsindo', NULL, NULL, 'id', NULL, '114.125.74.42', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 2, '2017-07-23 10:38:39', '2017-07-23 10:40:36', NULL),
(143, 'Muhammad ', 'Ihwanul Muslimin', 'Muhammad  Ihwanul Muslimin', '+6285338664826', 'iwanmuslim96@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '168.235.195.227', 'Safari', '', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 4, '2017-07-23 10:50:08', '2017-07-23 10:50:45', NULL),
(144, 'Deta', 'Setiawan', 'Deta Setiawan', '+6289610269493', 'kikipaar9@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '36.74.135.40', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 7, '2017-07-23 12:06:44', '2017-07-23 12:08:15', NULL),
(145, 'Parid ', 'maulana ', 'Parid  maulana ', '+6285711702633', 'maulfarid73@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_carousel', NULL, NULL, 'id', NULL, '36.84.67.67', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-23 12:53:43', '2017-07-23 12:54:19', NULL),
(146, 'Setiaji', 'Ahmad', 'Setiaji Ahmad', '+628123316678', 'setiajidrspp@gmail.com', 'parent', 0, NULL, 'facebook', 'SQI_traffic', 'idpr', 'pr_statics2', NULL, NULL, 'id', NULL, '114.125.116.241', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 6, '2017-07-23 13:42:22', '2017-07-23 13:44:34', NULL),
(147, 'Novi', 'Gladistia', 'Novi Gladistia', '+6282327900046', 'novigladistiaarifin@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '116.206.28.37', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 5, '2017-07-23 14:24:08', '2017-07-23 14:25:24', NULL),
(148, 'Yusliana', 'Yusliana', 'Yusliana Yusliana', '+6282395787610', 'yusliana.ana1@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsindo', NULL, NULL, 'id', NULL, '202.67.37.47', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 6, '2017-07-23 15:06:31', '2017-07-23 15:09:15', NULL),
(149, 'muhammad rivai', 'rasyid', 'muhammad rivai rasyid', '+6282346576991', 'pairasmuhammad@yahoo.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '125.167.186.73', 'HiSoRange Generic Browser', '', 'HiSoRange Generic Browser', '0', '0', '1', '0', 'HiSoRange Generic OS', '', 'HiSoRange Generic OS', '', '', '0', '', 0, 0, 0, 4, '2017-07-23 15:43:10', '2017-07-23 15:44:15', NULL),
(150, 'Riana', 'Widya', 'Riana Widya', '+6282299987731', 'rianawidyatari31@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '120.188.6.191', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-23 16:21:29', '2017-07-23 16:22:22', NULL),
(151, 'Fitrian kartika', 'Fitri', 'Fitrian kartika Fitri', '+62085648256881', 'ffitriankartika@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '112.215.154.189', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-23 16:22:31', '2017-07-23 16:22:54', NULL),
(152, 'Bill', 'Mandaw', 'Bill Mandaw', '+6285389898300', 'billmandaw@gmail.com', 'direct', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'id', NULL, '36.84.1.160', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 5, '2017-07-23 16:27:15', '2017-07-23 16:28:05', NULL),
(153, 'Dewi', 'Fortuna', 'Dewi Fortuna', '+628970352550', 'dolphindewi.fortuna@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '8.37.230.76', 'Safari', '', 'Safari', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-23 17:27:08', '2017-07-23 17:27:43', NULL),
(154, 'Marita', 'Wati', 'Marita Wati', '087812871513', 'marita194@yahoo.co.id', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_carousel', NULL, NULL, 'id', NULL, '112.215.243.25', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-23 18:37:30', '2017-07-23 18:39:12', NULL),
(155, 'Dea ', 'Fitrajaya', 'Dea  Fitrajaya', '+6282122503350', 'defitra135@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_carousel', NULL, NULL, 'id', NULL, '114.124.179.6', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 6, '2017-07-23 19:51:36', '2017-07-23 19:53:19', NULL),
(156, 'AKE TUPESLA ', 'BAHAR', 'AKE TUPESLA  BAHAR', '+6282285489480', 'aketupeslabahar@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '202.67.44.18', 'Firefox', '', 'Firefox', '0', '0', '1', '0', 'Win8.1', '', 'Win8.1', 'PC', '', '3', '', 0, 0, 0, 7, '2017-07-23 21:25:16', '2017-07-23 21:28:11', NULL),
(157, 'Giovanni', 'Sutantiono', 'Giovanni Sutantiono', '+6285746441311', 'giovannitan43@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '110.136.72.109', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '3', 'A', 0, 0, 0, 7, '2017-07-23 22:06:24', '2017-07-23 22:09:15', NULL),
(158, 'Juko', 'Zhou', 'Juko Zhou', '+6281270036229', 'jukohyhy@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_lookalike', 'idpg', 'hs_carousel', NULL, NULL, 'id', NULL, '36.76.249.90', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', 'Samsung', '0', 'A', 0, 0, 0, 5, '2017-07-23 22:11:24', '2017-07-23 22:12:18', NULL),
(159, 'steffanie', 'wenlan', 'steffanie wenlan', '+6281219032228', 'steffaniewenlann@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_lookalike', 'idhs', 'hs_staticsindo', NULL, NULL, 'id', NULL, '103.214.236.22', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', 'Samsung', '0', 'A', 0, 0, 0, 7, '2017-07-23 22:42:45', '2017-07-23 22:43:30', NULL),
(160, 'Albert', 'Darren', 'Albert Darren', '+6281255501265', 'albertdarren35@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.125.104.177', 'Safari', '', 'Safari', '0', '0', '1', '0', 'MacOSX', '', 'MacOSX', '', '', '1', '', 0, 0, 0, 5, '2017-07-23 22:58:24', '2017-07-23 22:58:55', NULL),
(161, 'Sindy', 'Puji Astari', 'Sindy Puji Astari', '+62089605549314', 'Sindy.pujiastari@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '180.214.232.66', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-24 01:25:26', '2017-07-24 01:27:08', NULL),
(162, 'Leny', 'Ramadhan', 'Leny Ramadhan', '+62 852-0530-2406', 'lenyramadhan57@gmail.com', 'student', 0, NULL, 'facebook', NULL, NULL, NULL, NULL, NULL, 'id', NULL, '112.215.237.1', 'Chrome', '', 'Chrome', '0', '0', '1', '0', 'Win7', '', 'Win7', '', '', '1', '', 0, 0, 0, 4, '2017-07-24 02:57:31', '2017-07-24 03:03:35', NULL),
(163, 'Dyan', 'Maulana', 'Dyan Maulana', '+6282361338957', 'dyanm22@gmail.com', 'student', 0, NULL, 'facebook', 'SQI_lookalike', 'idhs', 'hs_staticsindo', NULL, NULL, 'id', NULL, '103.10.66.3', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 4, '2017-07-24 03:24:48', '2017-07-24 03:25:07', NULL),
(164, 'delistia', 'naim', 'delistia naim', '+6281384053624', 'afifirus@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '114.124.242.108', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 5, '2017-07-24 05:35:23', '2017-07-24 05:36:02', NULL),
(165, 'sutan amir ', 'pohan', 'sutan amir  pohan', '+6287787574848', 'ndp4mor@gmail.com', 'student', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '112.215.45.176', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 7, '2017-07-24 05:55:27', '2017-07-24 05:56:41', NULL),
(166, 'Aisyah', 'Aisyah', 'Aisyah Aisyah', '+6281261737958', 'aiaisyah19758@gmail.com', 'direct', 0, NULL, 'adwords', 'SQI', NULL, NULL, NULL, NULL, 'id', NULL, '223.255.231.137', 'Chrome', '', 'Chrome', '1', '0', '0', '0', 'AndroidOS', '', 'AndroidOS', '', '', '0', 'A', 0, 0, 0, 4, '2017-07-24 07:14:32', '2017-07-24 07:18:47', NULL),
(167, 'tutik', 'koesmining', 'tutik koesmining', '+6281344223880', 'juragancantik2011@gmail.com', 'direct', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'id', NULL, '114.125.139.79', 'Firefox', '', 'Firefox', '0', '0', '1', '0', 'Win7', '', 'Win7', 'PC', '', '3', '', 0, 0, 0, 2, '2017-07-24 07:20:49', '2017-07-24 07:47:07', NULL),
(168, 'Martina ', 'Sutanto', 'Martina  Sutanto', '+6288803903270', 'martina.chen11@gmail.com', 'direct', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'id', NULL, '36.79.14.42', 'Chrome', '', 'Chrome', '0', '0', '1', '0', 'Win7', '', 'Win7', '', '', '1', '', 0, 0, 0, 3, '2017-07-24 08:57:27', '2017-07-24 08:57:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leadsurveys`
--

CREATE TABLE `leadsurveys` (
  `id` int(10) UNSIGNED NOT NULL,
  `lead_id` int(11) NOT NULL,
  `age_group` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_location` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_academic` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `when_graduate` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_study_interest` text COLLATE utf8_unicode_ci,
  `study_destination` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `planning_apply` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_of_study` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ever_take_ielts` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_taken` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `when_take_test` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_score` int(11) DEFAULT NULL,
  `last_holiday` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `close_family_overseas` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefer_time` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_note` text COLLATE utf8_unicode_ci,
  `is_finish` int(11) DEFAULT NULL,
  `current_score` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leadsurveys`
--

INSERT INTO `leadsurveys` (`id`, `lead_id`, `age_group`, `gender`, `nationality`, `current_location`, `last_academic`, `when_graduate`, `area_study_interest`, `study_destination`, `planning_apply`, `level_of_study`, `ever_take_ielts`, `test_taken`, `when_take_test`, `test_score`, `last_holiday`, `close_family_overseas`, `prefer_time`, `additional_note`, `is_finish`, `current_score`, `created_at`, `updated_at`, `deleted_at`) VALUES
(114, 80, '25-34', 'Female', 'Indonesia', 'Indonesia', 'bachelor', '2015', 'Law', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', 'before_2015', 430, 'domestically', 'no', 'morning', '', 1, 5, '2017-07-20 08:56:48', '2017-07-20 09:00:51', NULL),
(116, 82, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', 'before_2015', 'Education and Training', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 2, '2017-07-20 09:46:32', '2017-07-20 09:49:03', NULL),
(117, 83, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', 'before_2015', 'Business and Management', 'Australia', 'Within a year', 'postgraduate', 'yes', 'IELTS', '2017', 6, 'domestically', 'no', 'afternoon', 'Saya butuh material untuk upgrade overall IELTS Score\n', 1, 3, '2017-07-20 11:11:48', '2017-07-20 11:14:43', NULL),
(118, 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-20 12:34:52', '2017-07-20 12:34:52', NULL),
(126, 88, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'USA', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'europe', 'no', 'morning', 'saya ingin mendaftarkan diri dalam beasiswa luar negri.', 1, 9, '2017-07-20 14:15:14', '2017-07-20 14:17:01', NULL),
(127, 89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-20 14:29:04', '2017-07-20 14:35:40', '2017-07-20 14:35:40'),
(128, 90, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2016', 'MBA', 'Others', 'Within 6 months', 'undergraduate', 'yes', 'IELTS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-07-20 14:31:25', '2017-07-20 14:35:19', '2017-07-20 14:35:19'),
(129, 89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-20 14:37:04', '2017-07-20 14:37:53', '2017-07-20 14:37:53'),
(130, 91, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', 'before_2015', 'Business and Management', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 3, '2017-07-20 15:16:00', '2017-07-20 15:17:42', NULL),
(131, 92, '18-24', 'Male', 'Indonesia', 'Indonesia', 'college', '2017', 'Health and Medicine', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'Sebagai anak tunggal saya ingin membanggakan hati kedua orang tua saya ', 1, 5, '2017-07-20 22:03:19', '2017-07-20 22:05:46', NULL),
(132, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 06:07:15', '2017-07-21 06:07:15', NULL),
(133, 94, '18-24', 'Male', 'Indonesia', 'Indonesia', 'bachelor', '2017', 'Applied and Pure Sciences', 'Australia', 'Unsure', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 5, '2017-07-21 09:10:36', '2017-07-21 09:12:13', NULL),
(134, 95, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-21 10:45:37', '2017-07-21 10:48:52', '2017-07-21 10:48:52'),
(135, 96, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Social Studies and Media', 'Australia', 'Unsure', 'undergraduate', 'yes', 'TOEFL', '2016', 550, 'domestically', 'yes', 'afternoon', '', 1, 5, '2017-07-21 10:59:07', '2017-07-21 11:03:33', NULL),
(136, 97, '18-24', 'Female', 'Indonesia', 'Indonesia', 'college', 'before_2015', 'Health and Medicine', 'Others', 'Unsure', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Informasi tentang biaya hidup yang harus dikeluarkan ketika saya kuliah nanti jika saya mendapatkan beasiswa tersebut, dan juga informasi tentang apakah mahasiswa asing seperti saya bisa mendapatkan pekerjaan sampingan guna mencukupi kebutuhan hidup yang harus saya keluarkan, lalu informasi tentang kesempatan saya untuk bisa bekerja di negara yang menjadi tujuan kuliah saya ketika saya sudah menyelesaikan pendidikan', 1, 2, '2017-07-21 11:10:07', '2017-07-21 11:40:54', '2017-07-21 11:40:54'),
(137, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 11:13:04', '2017-07-21 11:15:55', '2017-07-21 11:15:55'),
(138, 99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 11:20:15', '2017-07-21 11:22:44', '2017-07-21 11:22:44'),
(139, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 11:33:09', '2017-07-21 11:37:33', '2017-07-21 11:37:33'),
(140, 98, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Law', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Nilai ijazah saya tidak bagus.\nRata ratanya hanya 5,5  (skala 1-10) saja', 1, 6, '2017-07-21 11:40:34', '2017-07-21 11:44:25', NULL),
(141, 100, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2017-07-21 13:07:01', '2017-07-21 13:07:40', NULL),
(142, 101, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', '2016', 'Education and Training', 'Australia', 'Within 6 months', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Saya Sarjana Ekonomi. \nSaya muslim\nDan... \nSaya miskin. ', 1, 5, '2017-07-21 14:32:32', '2017-07-21 14:35:22', NULL),
(143, 102, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Unsure', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 5, '2017-07-21 14:54:10', '2017-07-21 14:55:50', NULL),
(144, 103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 15:06:10', '2017-07-21 15:06:10', NULL),
(145, 104, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-21 15:25:50', '2017-07-21 15:26:29', '2017-07-21 15:26:29'),
(146, 105, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Social Studies and Media', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'yes', 'afternoon', 'Info mengenai program beasiswa study di australia dan univ yang tepat', 1, 5, '2017-07-21 15:26:19', '2017-07-21 15:29:02', NULL),
(147, 106, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Computer Science and IT', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'Tidak ada ', 1, 6, '2017-07-21 15:42:34', '2017-07-21 15:44:18', NULL),
(148, 107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 16:48:47', '2017-07-21 16:48:47', NULL),
(149, 108, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2015', 'Humanities', 'Australia', 'Within 2 years', 'postgraduate', 'yes', 'TOEFL', '2015', 323, 'domestically', 'no', 'afternoon', 'informasi beasiswa, tempat tinggal, kuliner halal, tempat beribadah', 1, 3, '2017-07-21 18:12:10', '2017-07-21 18:17:20', NULL),
(150, 109, '18-24', 'Male', 'Indonesia', 'Indonesia', 'bachelor', '2017', 'Social Studies and Media', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', '2015', 453, 'domestically', NULL, 'afternoon', '', 1, 7, '2017-07-21 19:24:53', '2017-07-21 19:27:33', NULL),
(151, 110, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Within a year', 'english_language', 'no', NULL, NULL, NULL, 'others', 'no', 'afternoon', 'Ingin belajar di Australia dengan mendapatkan scholarship', 1, 6, '2017-07-21 19:33:56', '2017-07-21 19:36:49', NULL),
(152, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 20:07:10', '2017-07-21 20:07:10', NULL),
(153, 112, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Computer Science and IT', 'Australia', 'Within 2 years', 'english_language', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'Biaya kuliah apakah ditanggung sama anda\nTermasuk biaya tempat tinggal?\nTerus visa student apakah anda yang bikin untuk saya?', 1, 5, '2017-07-21 20:36:56', '2017-07-21 20:40:02', NULL),
(154, 113, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'yes', 'afternoon', '', 1, 6, '2017-07-21 21:07:19', '2017-07-21 21:09:07', NULL),
(155, 114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-21 21:30:21', '2017-07-21 21:31:16', '2017-07-21 21:31:16'),
(156, 115, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Travel and Hospitality', 'Australia', 'Unsure', 'undergraduate', 'yes', 'TOEFL', '2016', 259, 'domestically', 'no', 'afternoon', '', 1, 5, '2017-07-21 21:43:09', '2017-07-21 21:47:00', NULL),
(157, 116, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Computer Science and IT', 'USA', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Saya anak pertama dari keluarga berpenghasilan rendah. saya memiliki 1 adik perempuan yang masih kelas 3 SD saya tidak ingin membebani orangtua saya', 1, 10, '2017-07-21 22:47:22', '2017-07-21 23:11:35', '2017-07-21 23:11:35'),
(158, 117, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', 'before_2015', 'Education and Training', 'Australia', 'Within a year', 'english_language', 'no', NULL, NULL, NULL, 'asia', 'no', 'morning', '', 1, 3, '2017-07-21 23:27:44', '2017-07-21 23:29:22', NULL),
(159, 118, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Within a year', 'undergraduate', 'yes', 'IELTS', '2015', -7, 'others', 'no', 'morning', '', 1, 6, '2017-07-21 23:32:34', '2017-07-21 23:35:29', NULL),
(160, 119, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', 'before_2015', 'Engineering', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', '2017', 580, NULL, NULL, NULL, NULL, NULL, 3, '2017-07-22 07:14:28', '2017-07-22 07:20:50', NULL),
(161, 120, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', '', 1, 6, '2017-07-22 09:16:10', '2017-07-22 09:17:26', NULL),
(162, 121, '18-24', 'Male', 'Indonesia', 'Indonesia', 'bachelor', '2016', 'Humanities', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'IELTS', '2017', 6, 'asia', 'yes', 'morning', '', 1, 7, '2017-07-22 10:43:38', '2017-07-22 10:47:07', NULL),
(163, 122, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Applied and Pure Sciences', 'USA', 'Unsure', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 5, '2017-07-22 10:53:22', '2017-07-22 10:57:09', NULL),
(164, 123, '18-24', 'Female', 'Indonesia', 'Indonesia', 'college', '2016', 'Creative Arts and Design', 'Australia', 'Within 2 years', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'yes', 'afternoon', 'bisakah saya mendapatkan beasiswa? dan apa syarat-syaratnya? terimakasih', 1, 4, '2017-07-22 11:18:16', '2017-07-22 11:25:56', NULL),
(165, 124, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Others', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '1. Dimana bisa mendapatkan beasiswa untuk S1 di eropa atau australia ??\n2. Apakah monash university malaysia tidak jauh berbeda dengan yang di australia? Bila berbeda , apa yg paling jelas terlihat berbeda ?? Apakah dengan biaya yg kita keluarkan di monash malaysia worth it dengan gelar kelulusan  kelak untuk mencari pekerjaan? \n', 1, 13, '2017-07-22 11:42:42', '2017-07-22 11:49:08', NULL),
(166, 125, '18-24', 'Female', 'Indonesia', 'Indonesia', 'college', '2017', 'Health and Medicine', 'Australia', 'Unsure', 'postgraduate', 'no', NULL, NULL, NULL, 'others', 'no', 'morning', 'Sebenarnya saya ingin mengambil beasiswa s2 di melbourne di fakultas pendidikan, tapi adakah fakultas pendidikan di sana? ', 1, 5, '2017-07-22 12:28:46', '2017-07-22 12:31:42', NULL),
(167, 126, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', 'before_2015', 'Social Studies and Media', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Sebenarnya, saya sedang kuliah dan mengambil jurusan Akuntansi, tapi saya juga berminat di bagian seni seperti vokal/ menyanyi, composer, dsb. Saya juga cukup tertarik pada teologi Katolik dan bisnis . Semoga saya bisa mendapatkan jurusan yang sesuai dengan minat dan membantu karir saya kedepannya.', 1, 2, '2017-07-22 13:04:34', '2017-07-22 13:13:08', NULL),
(168, 127, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Humanities', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Saya masih lulus tahun depan, hanya saja ingin mulai mencari tentang beasiswa dari sekarang, karena saya ingin kuliah di luar negeri dan mendapat pengalaman baru', 1, 6, '2017-07-22 14:33:33', '2017-07-22 14:36:39', NULL),
(169, 128, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Engineering', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Saya tamat sma tahun depan dan ingin mencari beasiswa kuliah di australia', 1, 6, '2017-07-22 15:46:50', '2017-07-22 15:49:36', NULL),
(170, 129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-22 21:37:43', '2017-07-22 21:37:44', '2017-07-22 21:37:44'),
(171, 130, '25-34', 'Female', 'Indonesia', 'Indonesia', 'bachelor', '2016', 'Architecture and Construction', 'Australia', 'Within a year', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'yes', 'afternoon', '', 1, 4, '2017-07-22 23:31:51', '2017-07-22 23:33:06', NULL),
(172, 131, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2016', 'Agriculture and Veterinary Medicine', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 4, '2017-07-23 00:06:37', '2017-07-23 00:13:33', NULL),
(173, 132, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Within 6 months', 'english_language', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'saya ingin mencari informasi untuk adik saya dan adik saya ingin Sekolah kursus belajar bahasa ingris, sudah ada nilai Bahasa ingris nya adik saya ingin sekali rencana nya ke Austalia....\n\n', 1, 7, '2017-07-23 01:52:09', '2017-07-23 01:58:22', NULL),
(174, 133, '18-24', 'Female', 'Indonesia', 'Indonesia', 'bachelor', '2016', 'Business and Management', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', '2016', 490, 'asia', 'yes', 'morning', '', 1, 7, '2017-07-23 01:56:54', '2017-07-23 01:59:06', NULL),
(175, 134, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Personal Care and Fitness', 'USA', 'Within a year', 'english_language', 'no', NULL, NULL, NULL, 'others', 'no', 'afternoon', 'Saya berharap dan igin sekali bisa kuliah atau bekerja di luar negeri', 1, 6, '2017-07-23 02:04:45', '2017-07-23 02:09:49', NULL),
(176, 135, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Social Studies and Media', 'Australia', 'Within 2 years', 'postgraduate', 'yes', 'IELTS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-23 02:16:24', '2017-07-23 02:18:08', NULL),
(177, 136, '25-34', 'Female', 'Indonesia', 'Indonesia', 'diploma', 'before_2015', 'Health and Medicine', 'Canada', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Proses administrasi', 1, 2, '2017-07-23 04:37:11', '2017-07-23 04:39:59', NULL),
(178, 137, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'USA', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Saya ingin sekali membanggakan kedua orang tua saya melalui beasiswa ke luar.', 1, 6, '2017-07-23 05:21:46', '2017-07-23 05:24:52', NULL),
(179, 138, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Business and Management', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-23 07:25:30', '2017-07-23 07:27:22', NULL),
(180, 139, '18-24', 'Female', 'Indonesia', 'Indonesia', 'bachelor', '2017', 'Architecture and Construction', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', '2016', 475, 'asia', 'no', 'morning', '', 1, 8, '2017-07-23 08:04:45', '2017-07-23 08:07:00', NULL),
(181, 140, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Social Studies and Media', 'Australia', 'Unsure', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'yes', 'afternoon', '', 1, 8, '2017-07-23 08:26:51', '2017-07-23 08:35:56', NULL),
(182, 141, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Travel and Hospitality', 'Australia', 'Within a year', 'undergraduate', 'yes', 'TOEFL', '2017', 450, 'domestically', 'no', 'afternoon', '- biaya kuliah di berbagai universitas yang sesuai dengan travel and hospitality\n- info beasiswa bagi international student\n- biaya tempat tinggal', 1, 6, '2017-07-23 08:39:56', '2017-07-23 08:45:33', NULL),
(183, 142, '25-34', 'Male', 'Indonesia', 'Indonesia', 'highschool', 'before_2015', 'Social Studies and Media', 'Australia', 'Within a year', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'Gelar terakhir saya hari ini adalah sarjana pendidikan. (S. Pd) \n\n', 1, 2, '2017-07-23 10:38:39', '2017-07-23 10:42:30', NULL),
(184, 143, '18-24', 'Male', 'Indonesia', 'Indonesia', 'college', '2016', 'Health and Medicine', 'Australia', 'Unsure', 'postgraduate', 'no', NULL, NULL, NULL, 'others', 'yes', 'afternoon', '', 1, 4, '2017-07-23 10:50:08', '2017-07-23 10:51:48', NULL),
(185, 144, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Social Studies and Media', 'Canada', 'Within 6 months', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 7, '2017-07-23 12:06:44', '2017-07-23 12:08:40', NULL),
(186, 145, '18-24', 'Male', 'Indonesia', 'Indonesia', 'college', '2017', 'Humanities', 'Canada', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 5, '2017-07-23 12:53:43', '2017-07-23 12:55:52', NULL),
(187, 146, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Health and Medicine', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Nggak', 1, 6, '2017-07-23 13:42:22', '2017-07-23 13:45:12', NULL),
(188, 147, '18-24', 'Female', 'Indonesia', 'Indonesia', 'diploma', '2017', 'Law', 'Australia', 'Within 2 years', 'postgraduate', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-23 14:24:08', '2017-07-23 14:34:07', NULL),
(189, 148, '18-24', 'Female', 'Indonesia', 'Indonesia', 'college', '2017', 'Humanities', 'Australia', 'Within a year', 'postgraduate', 'no', NULL, NULL, NULL, 'others', 'no', 'afternoon', 'Mohon Bimbingannya.', 1, 6, '2017-07-23 15:06:31', '2017-07-23 15:10:28', NULL),
(190, 149, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', 'before_2015', 'Business and Management', 'Australia', 'Within 6 months', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'yes', 'afternoon', 'nothing', 1, 4, '2017-07-23 15:43:10', '2017-07-23 15:47:04', NULL),
(191, 150, '18-24', 'Female', 'Indonesia', 'Indonesia', 'college', '2017', 'Agriculture and Veterinary Medicine', 'Australia', 'Within 2 years', 'postgraduate', 'yes', 'TOEFL', '2017', 558, 'domestically', 'no', 'afternoon', 'Ipk saya sangat minim,tapi saya berada di world class university yang ada di indonesia yaitu IPB Institut Pertanian Bogor', 1, 5, '2017-07-23 16:21:29', '2017-07-23 16:27:03', NULL),
(192, 151, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Creative Arts and Design', 'Australia', 'Within 2 years', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Informasi tentang beasiswa di luar negri jurusan desain grafis maupun hubungan internasional tolong beritahu saya', 1, 5, '2017-07-23 16:22:31', '2017-07-23 16:24:33', NULL),
(193, 152, '18-24', 'Male', 'Indonesia', 'Indonesia', 'college', '2017', 'Travel and Hospitality', 'USA', 'Unsure', 'english_language', 'yes', 'TOEFL', 'before_2015', 400, 'domestically', 'yes', 'afternoon', '', 1, 5, '2017-07-23 16:27:15', '2017-07-23 16:33:32', NULL),
(194, 153, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-23 17:27:08', '2017-07-23 17:27:43', NULL),
(195, 154, '25-34', 'Female', 'Indonesia', 'Indonesia', 'diploma', '2016', 'Health and Medicine', 'Canada', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', '2016', 450, 'domestically', 'no', 'morning', 'Sy seorang D3 keperawatan yg baru lulus thn 2016', 1, 5, '2017-07-23 18:37:30', '2017-07-23 18:40:53', NULL),
(196, 155, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', '2017', 'Computer Science and IT', 'Canada', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', '2017', 480, 'others', 'no', 'morning', 'Keahlian yang saya kuasai :\n1. Mampu menguasai micrososft office\n2. Audit Teknologi Informasi\n3. Membuat Sistem Artificial Intelegent\n4. Keahlian Presentasi\n5. Analisis Bisnis SWOT Arsitektur Perusahaan\n6. Desain Web\n7. Teknik Quality Assurance. (QA)\n8. Teknik Quality Control. (QC)\nTerimakasih. ', 1, 6, '2017-07-23 19:51:36', '2017-07-23 19:57:43', NULL),
(197, 156, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Engineering', 'Australia', 'Within 6 months', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 7, '2017-07-23 21:25:16', '2017-07-23 21:28:52', NULL),
(198, 157, '18-24', 'Preferable_not', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Creative Arts and Design', 'Australia', 'Within a year', 'undergraduate', 'no', NULL, NULL, NULL, 'asia', 'no', 'afternoon', 'Saya ingin beasiswa penuh', 1, 7, '2017-07-23 22:06:24', '2017-07-23 22:09:40', NULL),
(199, 158, '25-34', 'Male', 'Indonesia', 'Indonesia', 'bachelor', '2017', 'Travel and Hospitality', 'Australia', 'Within a year', 'undergraduate', 'yes', 'IELTS', 'before_2015', 300, 'domestically', 'no', 'afternoon', '', 1, 5, '2017-07-23 22:11:24', '2017-07-23 22:13:49', NULL),
(200, 159, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Personal Care and Fitness', 'Australia', 'Within 6 months', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 7, '2017-07-23 22:42:45', '2017-07-23 22:43:48', NULL),
(201, 160, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2017-07-23 22:58:24', '2017-07-23 22:58:55', NULL),
(202, 161, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2015', 'Health and Medicine', 'Australia', 'Within 6 months', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', 'Bagaimana dengan pembiayaan kuliah di australia? Apakah ada beasiswa full? Syarat apa yang harus di tempuh supaya mendapatkan beasiswa? ', 1, 5, '2017-07-24 01:25:26', '2017-07-24 01:29:07', NULL),
(203, 162, '18-24', 'Female', 'Indonesia', 'Indonesia', 'bachelor', '2016', 'Health and Medicine', 'Australia', 'Unsure', 'postgraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'saya memiliki pengetahuan tentang berbicara bahasa inggris yang sangat minim, bagaimana saya mewujudkan mimpi saya untuk melanjutkan pendidikan saya ke luar negeri.? padahal saya ingin membuktikan kepada orang tua saya kalau saya bisa membuat mereka bangga dan ingin memperdalam ilmu yang sangat minim yang saya miliki sekarang.', 1, 4, '2017-07-24 02:57:31', '2017-07-24 03:07:30', NULL),
(204, 163, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2016', 'Business and Management', 'UK', 'Unsure', 'english_language', 'no', NULL, NULL, NULL, 'domestically', 'no', 'afternoon', '', 1, 4, '2017-07-24 03:24:48', '2017-07-24 03:27:19', NULL),
(205, 164, '18-24', 'Female', 'Indonesia', 'Indonesia', 'college', '2017', 'Health and Medicine', 'Australia', 'Within 2 years', 'postgraduate', 'yes', 'TOEFL', '2017', 437, 'domestically', 'no', 'morning', 'kemampuan bahasa inggris saya kurang memadai, adakah saran untuk saya?', 1, 5, '2017-07-24 05:35:23', '2017-07-24 05:38:05', NULL),
(206, 165, '18-24', 'Male', 'Indonesia', 'Indonesia', 'highschool', '2017', 'Engineering', 'Australia', 'Within 6 months', 'undergraduate', 'no', NULL, NULL, NULL, 'domestically', 'no', 'morning', 'kalau sy mau kuliah jurusan tehnik elektro buat sarjana atau program diploma berapa biayanya tks', 1, 7, '2017-07-24 05:55:27', '2017-07-24 05:57:41', NULL),
(207, 166, '18-24', 'Female', 'Indonesia', 'Indonesia', 'highschool', '2016', 'Health and Medicine', 'Australia', 'Unsure', 'postgraduate', 'yes', 'TOEFL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-07-24 07:14:32', '2017-07-24 07:18:47', '2017-07-24 07:18:47'),
(208, 167, '35+', 'Female', 'Indonesia', 'Indonesia', 'bachelor', 'before_2015', 'Law', 'Australia', 'Within 6 months', 'postgraduate', 'yes', 'TOEFL', 'before_2015', 400, 'domestically', 'no', 'afternoon', '', 1, 2, '2017-07-24 07:20:49', '2017-07-24 07:47:07', '2017-07-24 07:47:07'),
(209, 168, '18-24', 'Female', 'Indonesia', 'Indonesia', 'bachelor', '2015', 'Education and Training', 'Australia', 'Unsure', 'english_language', 'yes', 'TOEFL', '2016', 0, NULL, NULL, NULL, NULL, NULL, 3, '2017-07-24 08:57:27', '2017-07-24 09:03:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_11_30_082130_create_leads_table', 1),
(4, '2017_01_10_114406_create_leadsurveys_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utmleads`
--

CREATE TABLE `utmleads` (
  `id` int(11) NOT NULL,
  `lead_source` text,
  `utm_source` text,
  `campaign` text,
  `adset` text,
  `ads` text,
  `counter` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utmleads`
--

INSERT INTO `utmleads` (`id`, `lead_source`, `utm_source`, `campaign`, `adset`, `ads`, `counter`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsindo', 118, '2017-07-19 18:36:57', '2017-07-24 09:54:30', NULL),
(2, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_staticsindo', 2792, '2017-07-19 18:45:51', '2017-07-24 09:46:35', NULL),
(3, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_carousel', 2040, '2017-07-19 18:49:03', '2017-07-24 09:55:11', NULL),
(4, NULL, 'facebook', 'SQI_traffic', 'idpr', 'pr_statics2', 2825, '2017-07-19 18:54:57', '2017-07-24 09:51:23', NULL),
(5, NULL, 'facebook', 'SQI_traffic', 'idpr', 'pr_carousel', 281, '2017-07-19 19:07:47', '2017-07-24 09:54:59', NULL),
(6, NULL, 'facebook', 'SQI_traffic', 'idhs', 'hs_staticsenglish', 27, '2017-07-19 19:53:10', '2017-07-24 08:43:20', NULL),
(7, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_carousel', 446, '2017-07-19 20:01:12', '2017-07-24 09:53:57', NULL),
(8, NULL, 'facebook', 'SQI_traffic', 'idpr', 'pr_statics1', 42, '2017-07-19 20:06:25', '2017-07-23 21:31:25', NULL),
(9, NULL, 'facebook', 'SQI_traffic', 'idpg', 'hs_staticsenglish', 12, '2017-07-19 22:51:11', '2017-07-24 09:12:46', NULL),
(10, NULL, 'facebook', 'SQI_lookalike', 'idhs', 'hs_staticsindo', 93, '2017-07-20 00:01:39', '2017-07-24 09:43:27', NULL),
(11, NULL, 'facebook', 'SQI_lookalike', 'idhs', 'hs_carousel', 256, '2017-07-20 00:02:31', '2017-07-24 09:46:28', NULL),
(12, NULL, 'facebook', 'SQI_lookalike', 'idhs', 'hs_staticsenglish', 263, '2017-07-20 00:03:41', '2017-07-24 09:39:49', NULL),
(13, NULL, 'facebook', 'SQI_lookalike', 'idpg', 'hs_carousel', 118, '2017-07-20 00:05:59', '2017-07-24 09:24:18', NULL),
(14, NULL, 'facebook', 'SQI_lookalike', 'idpg', 'hs_staticsindo', 17, '2017-07-20 00:06:51', '2017-07-24 06:53:34', NULL),
(15, NULL, 'facebook', 'SQI_lookalike', 'idpg', 'hs_staticsenglish', 10, '2017-07-20 00:07:44', '2017-07-24 01:34:24', NULL),
(16, NULL, 'facebook', 'SQI', 'post', 'studentpost=', 95, '2017-07-20 00:11:43', '2017-07-24 01:06:45', NULL),
(17, NULL, 'facebook', 'SQI', 'post', 'parentpost', 2, '2017-07-20 00:14:02', '2017-07-20 00:14:03', NULL),
(18, NULL, NULL, NULL, NULL, NULL, 1735, '2017-07-21 16:45:39', '2017-07-24 09:55:12', NULL),
(19, NULL, 'adwords', 'SQI', NULL, NULL, 438, '2017-07-21 16:50:09', '2017-07-24 09:57:10', NULL),
(20, NULL, NULL, NULL, 'idhs', 'hs_carousel', 1, '2017-07-22 13:18:09', '2017-07-22 13:18:09', NULL),
(21, NULL, NULL, NULL, 'post', 'studentpost%3D', 1, '2017-07-23 11:30:25', '2017-07-23 11:30:25', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leadsurveys`
--
ALTER TABLE `leadsurveys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `utmleads`
--
ALTER TABLE `utmleads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `leadsurveys`
--
ALTER TABLE `leadsurveys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `utmleads`
--
ALTER TABLE `utmleads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
