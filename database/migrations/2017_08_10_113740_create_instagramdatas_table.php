<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagramdatas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->text('instagram_id');
            $table->text('thumbnail');
            $table->text('low_res');
            $table->text('high_res');
            $table->text('link');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagramdatas');
    }
}
