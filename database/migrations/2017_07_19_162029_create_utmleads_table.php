<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtmleadsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('utmleads', function (Blueprint $table) {
            $table->increments('id');

            $table->text('lead_source')->nullable();
            $table->text('utm_source')->nullable();
            $table->text('campaign')->nullable();
            $table->text('adset')->nullable();
            $table->text('ads')->nullable();
            $table->text('utm_term')->nullable();
            $table->integer('counter')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('utmleads');
    }

}
