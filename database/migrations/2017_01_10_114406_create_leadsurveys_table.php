<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsurveysTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('leadsurveys', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lead_id');
            $table->text('age_group')->nullable();
            $table->text('gender')->nullable();
            $table->text('nationality')->nullable();

            $table->text('current_location')->nullable();
            $table->text('last_academic')->nullable();
            $table->text('when_graduate')->nullable();
            $table->text('area_study_interest')->nullable();
            $table->text('study_destination')->nullable();
            $table->text('planning_apply')->nullable();
            $table->text('level_of_study')->nullable();
            $table->text('ever_take_ielts')->nullable();
            $table->text('test_taken')->nullable();
            $table->text('when_take_test')->nullable();
            $table->text('test_score')->nullable();
            $table->text('last_holiday')->nullable();
            $table->text('close_family_overseas')->nullable();
            $table->text('prefer_time')->nullable();
            $table->text('come_to_expo')->nullable();
            $table->text('expo_name')->nullable();
            $table->text('additional_note')->nullable();

            $table->integer('is_finish')->nullable();
            $table->double('current_score')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('leadsurveys');
    }

}
