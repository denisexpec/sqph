<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name', 64)->nullable();
            $table->string('last_name', 64)->nullable();
            $table->text('full_name')->nullable();
            $table->string('mobile_phone', 24)->nullable();
            $table->string('email', 128)->nullable();
            $table->integer('utm_visitor_id')->nullable();
            $table->string('lead_source', 128)->nullable();
            $table->tinyInteger('lead_rating')->nullable();
            $table->text('note_counselor')->nullable();
            $table->integer('lead_status')->nullable();
            $table->string('utm_source', 128)->nullable();
            $table->string('campaign', 128)->nullable();
            $table->string('adset', 128)->nullable();
            $table->string('ads', 128)->nullable();
            $table->string('utm_term', 128)->nullable();
            $table->string('is_followed_up', 128)->nullable();
            $table->string('nationality')->nullable();
            $table->string('country_group', 128)->nullable();
            $table->string('gender', 128)->nullable();

            $table->string('ip_address', 64)->nullable();
            $table->string('browser_name', 128)->nullable();
            $table->string('browser_version', 128)->nullable();
            $table->string('browser_family', 128)->nullable();
            $table->string('is_mobile', 128)->nullable();
            $table->string('is_tablet', 128)->nullable();
            $table->string('is_desktop', 128)->nullable();
            $table->string('is_bot', 128)->nullable();
            $table->string('os_family', 128)->nullable();
            $table->string('os_version', 128)->nullable();
            $table->string('os_name', 128)->nullable();
            $table->string('device_family', 128)->nullable();
            $table->string('device_model', 128)->nullable();
            $table->string('css_version', 128)->nullable();
            $table->string('mobile_grade', 128)->nullable();

            $table->tinyInteger('is_finish')->default(0);
            $table->tinyInteger('email_sent')->default(0);
            $table->tinyInteger('email_sent_to_candidate')->default(0);
            $table->double('total_score')->default(0);


            $table->timestamp('date_response')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('leads');
    }

}
