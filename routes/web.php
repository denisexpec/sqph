<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
#########################################################
#routes ini dibiarkan saja, khusus untuk testing function
Route::get('test', 'HomeController@test');
#########################################################

Route::get('/', 'HomeController@viewHome');
Route::get('student', 'HomeController@viewHome');
Route::get('parent', 'HomeController@viewHome');
Route::get('country', 'Homecontroller@viewCountry');

Route::get('igfeed', 'SocialMediaController@instagramFeed');


Route::get('quick','HomeController@quickHome');

Route::post('setcandidatetype','ReportController@setCandidatetype');
Route::post('setleadstatus','ReportController@setCandidateStatus');

Route::get('parent/aboutus','OtherController@viewAbout');
Route::get('student/aboutus','OtherController@viewAbout');
Route::get('aboutus','OtherController@viewAbout');



Route::post('insertlead', 'HomeController@insertLead');
Route::post('parent/insertlead', 'HomeController@insertLead');

Route::get('thanks', 'HomeController@viewThanks');

// Route::get('thanks/{first_name?}/{valid?}', 'HomeController@viewThanks')->middleware('thanks');
// Route::get('privacy', 'HomeController@viewPrivacy');
// Route::get('downloadguide', 'HomeController@downloadGuide');
Route::get('survey', 'HomeController@survey');
Route::get('parentsurvey', 'HomeController@parentsurvey');

Route::post('form_question','HomeController@processSurvey');

Route::get('changelanguage','HomeController@changeLanguage');

Route::get('report/x9878NBkPvKCzhJz2KKS','ReportController@viewReport');
Route::get('report/ph/x9878NBkPvKCzhJz2KKS','ReportController@viewReportPhilippines');
Route::get('report/others/x9878NBkPvKCzhJz2KKS','ReportController@viewReportOthers');

Route::post('report/x9878NBkPvKCzhJz2KKS/reportfilter','ReportController@reportFilter');

Route::get('exportlead', 'ReportController@exportLead');