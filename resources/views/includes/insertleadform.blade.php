<section class="page-section bg-scroll " data-background="{{asset('ext/images/sq-id/footer_banner.jpg')}}" id="into_form" style="background-image: url('{{asset('ext/images/sq-id/footer_banner.jpg')}}');">
                <div class="container relative">
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
							@if(App::getLocale() == 'id')
                            <div class="section-title white" style="color:#fff">
                                Konsultasi Gratis dengan Kami
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Isilah form di bawah ini, para konselor berpengalaman kami akan menghubungi anda secepatnya.</h2>
							@else
							<div class="section-title white" style="color:#fff">
                                Free Consultation with Experts
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Fill out the form below, our counsellors will contact you as soon as possible.</h2>
							@endif
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- form-->
						<div class="row">
				
                        <div class="contact">
						
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
									</ul>
								</div>
							@endif
							
							
                            <!-- Contact Form -->                            
                            <form class="form contact-form" id="contact_form_2"  autocomplete="off" method="post" enctype="multipart/form-data"
							@if($lead_source == 'parent')
							action="{{url('parent/insertlead')}}"
							@else
							action="{{url('insertlead')}}"
							@endif
							>
                                <div class="clearfix mb-20 mb-xs-0">
                                    
                                    {{csrf_field()}}
                                    <input type="hidden" name="lead_source" value="{{$lead_source}}"/>
                                    <input type="hidden" name="language_setting" value="{{App::getLocale()}}"/>
									
									@if(isset($_GET['utm_source']))
									<input type="hidden" name="utm_source" value="{{$_GET['utm_source']}}"/>
									@else
										@if(isset($_COOKIE['utm_source']))
											<input type="hidden" name="utm_source" value="{{$_COOKIE['utm_source']}}"/>
										@else
											<input type="hidden" name="utm_source" value="{{old('utm_source')}}"/>
										@endif
									@endif
									
									@if(isset($_GET['adset']))
									<input type="hidden" name="adset" value="{{$_GET['adset']}}"/>
									@else
										@if(isset($_COOKIE['adset']))
											<input type="hidden" name="adset" value="{{$_COOKIE['adset']}}"/>
										@else
											<input type="hidden" name="adset" value="{{old('adset')}}"/>
										@endif
									@endif
								
									@if(isset($_GET['ads']))
									<input type="hidden" name="ads" value="{{$_GET['ads']}}"/>
									@else
										@if(isset($_COOKIE['adset']))
											<input type="hidden" name="ads" value="{{$_COOKIE['ads']}}"/>
										@else
											<input type="hidden" name="ads" value="{{old('ads')}}"/>
										@endif
									@endif
								
									@if(isset($_GET['utm_campaign']))
									<input type="hidden" name="campaign" value="{{$_GET['utm_campaign']}}"/>
									@else
										@if(isset($_COOKIE['utm_campaign']))
											<input type="hidden" name="campaign" value="{{$_COOKIE['utm_campaign']}}"/>
										@else
											<input type="hidden" name="campaign" value="{{old('utm_campaign')}}"/>
										@endif
									@endif
									
									@if(isset($utm_visitor))
										<input type="hidden" name="utm_visitor" value="{{$utm_visitor}}"/>
									@else
										<input type="hidden" name="utm_visitor" value="{{old('utm_visitor')}}"/>
									@endif
									
									@if(isset($_GET['utm_term']))
									<input type="hidden" name="term" value="{{$_GET['utm_term']}}"/>
									@else
										@if(isset($_COOKIE['utm_term']))
											<input type="hidden" name="term" value="{{$_COOKIE['utm_term']}}"/>
										@else
											<input type="hidden" name="term" value="{{old('utm_term')}}"/>
										@endif
									@endif
									
								
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="first_name" id="first-name" class="ci-field form-control" placeholder="First Name" required value="{{old('first_name')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="last_name" id="last-name" class="ci-field form-control" placeholder="Last Name" required value="{{old('last_name')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="phone form-control ci-field" type="tel" id="country_phone_2" name="phone" minlength="7" maxlength="14" style="margin-top:8px !important;" onkeypress='return event.charCode == 43 || (event.charCode >= 48 && event.charCode <= 57)' required value="{{old('phone')}}"/>
											<input type="hidden" name="country_code" value="id" class="country_code_from_phone" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="ci-field form-control" placeholder="Email" required value="{{old('email')}}">                                        <p class="text-left" style="color: red;"  id="error_email_2"></p>
                                        </div>

                                    </div>
                                    <div class="cf-left-col">
                                    </div>
                                    <div class="cf-right-col">
                                    </div>  
                                </div>
                                <!-- Send Button -->
                                <button class="btn btn-default btn-lg btn-sq-cta" id="submit_btn" style="font-size: 12px;background: #EF5245;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;"> Next! </button>
								@if(App::getLocale() == 'id')
                                <p style="color:#fff; font-style:italic; font-size: 14px;opacity:.6;margin-top: 25px;">Kami sangat menjaga kerahasian data Anda. Kami tidak akan memperjualbelikan, atau memberikan data Anda kepada pihak lain.</p>
								@else
								<p style="color:#fff; font-style:italic; font-size: 14px;opacity:.6;margin-top: 25px;">We take your privacy seriously. We will not sell, share or distribute your contact to any third party.</p>	
								@endif
								
								
								
                                <div id="result"></div>           
                            </form>                            
                            <!-- End Contact Form -->
                            </div>
					</div>
			


                    </div>
                    <!-- End form -->
                    
                </div>
	</section>