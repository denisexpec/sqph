<?php
#ambil data instagram
$instagrams = App\Models\Instagramdata::orderBy('created_at', 'desc')
    ->take(6)
    ->get();
?>
<ul class="works-grid clearfix" id="work-grid">
    @if(false)
        @foreach($instagrams as $instagram)
            <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <a href="{{ $instagram->link }}" class="work-lightbox-link mfp-image" target="_blank">
                    <div class="work-img">
                        <img src="{{ $instagram->thumbnail }}" alt="{{ $instagram->id }}" />
                    </div>
                </a>
            </li>
        @endforeach
    @endif
    <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
        <a href="
https://www.instagram.com/p/BgSREDuHEaC/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
            <div class="work-img">
                <img src="https://scontent-sin6-2.cdninstagram.com/vp/d632d37356513e87b86e521383d97e1b/5B3B1E65/t51.2885-15/e35/28753445_1174044639397521_468471814054477824_n.jpg" alt="Study Query Indonesia" />
            </div>
        </a>
    </li>
    <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
        <a href="https://www.instagram.com/p/BgkzD-1H_jE/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
            <div class="work-img">
                <img src="https://scontent-sin6-2.cdninstagram.com/vp/e3a8ccad000d5d2d0eb2d97554bf83ab/5B465A09/t51.2885-15/s640x640/sh0.08/e35/28765504_361757534339605_322632853739274240_n.jpg" alt="Study Query Indonesia" />
            </div>
        </a>
    </li>
    <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
        <a href="https://www.instagram.com/p/BgX3bKVn01g/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
            <div class="work-img">
                <img src="https://scontent-sin6-2.cdninstagram.com/vp/bcc8bbd464d18c55406a5fb5d3f323fb/5B48FBD4/t51.2885-15/e35/28753677_2019222245001582_3726717361525358592_n.jpg" alt="Study Query Indonesia" />
            </div>
        </a>
    </li>
    <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
        <a href="https://www.instagram.com/p/BgVRjOsHCmP/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
            <div class="work-img">
                <img src="https://scontent-sin6-2.cdninstagram.com/vp/6a42ad2c144aa59501cae44abed13018/5B2F5DB9/t51.2885-15/e35/28434972_324092828113073_5951408170566942720_n.jpg
" alt="Study Query Indonesia" />
            </div>
        </a>
    </li>
    <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
        <a href="https://www.instagram.com/p/BfKKOqVggWG/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
            <div class="work-img">
                <img src="https://instagram.fcgk12-1.fna.fbcdn.net/vp/80719b949eae46cb5df959b5c128f009/5B47426D/t51.2885-15/e35/27880872_149454255744321_1699555555875487744_n.jpg " alt="Study Query Indonesia" />
            </div>
        </a>
    </li>
    <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
        <a href="https://www.instagram.com/p/BgDZ7e0Hn9-/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
            <div class="work-img">
                <img src="https://scontent-sin6-2.cdninstagram.com/vp/a59261f40ca06597138a33724445fa8e/5B3A0856/t51.2885-15/s640x640/sh0.08/e35/28427193_186856841929987_6883339047855456256_n.jpg" alt="Study Query Indonesia" />
            </div>
        </a>
    </li>

</ul>