<!-- BEGIN section form -->
<form class="form form-banner register-form" id="contact_form" autocomplete="off" method="post" enctype="multipart/form-data" 
      @if($lead_source == 'parent')
      action="{{url('parent/insertlead')}}"
      @else
      action="{{url('insertlead')}}"
      @endif
      >
      <div class="clearfix mb-20 mb-xs-0">

        {{csrf_field()}}
        <input type="hidden" name="lead_source" value="{{$lead_source}}"/>
        <input type="hidden" name="language_setting" value="{{App::getLocale()}}"/>

        @if(isset($_GET['utm_source']))
        <input type="hidden" name="utm_source" value="{{$_GET['utm_source']}}"/>
        @else
        @if(isset($_COOKIE['utm_source']))
        <input type="hidden" name="utm_source" value="{{$_COOKIE['utm_source']}}"/>
        @else
        <input type="hidden" name="utm_source" value="{{old('utm_source')}}"/>
        @endif

        @endif

        @if(isset($_GET['adset']))
        <input type="hidden" name="adset" value="{{$_GET['adset']}}"/>
        @else
        @if(isset($_COOKIE['adset']))
        <input type="hidden" name="adset" value="{{$_COOKIE['adset']}}"/>
        @else
        <input type="hidden" name="adset" value="{{old('adset')}}"/>
        @endif
        @endif

        @if(isset($_GET['ads']))
        <input type="hidden" name="ads" value="{{$_GET['ads']}}"/>
        @else
        @if(isset($_COOKIE['ads']))
        <input type="hidden" name="ads" value="{{$_COOKIE['ads']}}"/>
        @else
        <input type="hidden" name="ads" value="{{old('ads')}}"/>
        @endif
        @endif

        @if(isset($_GET['utm_campaign']))
        <input type="hidden" name="campaign" value="{{$_GET['utm_campaign']}}"/>
        @else
        @if(isset($_COOKIE['utm_campaign']))
        <input type="hidden" name="campaign" value="{{$_COOKIE['utm_campaign']}}"/>
        @else
        <input type="hidden" name="campaign" value="{{old('utm_campaign')}}"/>
        @endif
        @endif

        @if($utm_visitor)
        <input type="hidden" name="utm_visitor" value="{{$utm_visitor}}"/>
        @else
        <input type="hidden" name="utm_visitor" value="{{old('utm_visitor')}}"/>
        @endif

        @if(isset($_GET['utm_term']))
        <input type="hidden" name="term" value="{{$_GET['utm_term']}}"/>
        @else
        @if(isset($_COOKIE['utm_term']))
        <input type="hidden" name="term" value="{{$_COOKIE['utm_term']}}"/>
        @else
        <input type="hidden" name="term" value="{{old('utm_term')}}"/>
        @endif
        @endif

        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="required-field">
                    <input name="first_name" id="hero-fname" class="hero-input" type="text" required value="{{old('first_name')}}" placeholder="First Name">
                </div> <!--/ .required-field -->
            </div>
            <div class="col-lg-6 col-md-6">
                <input name="last_name" id="hero-lname" class="hero-input" type="text" required value="{{old('last_name')}}" placeholder="Last Name">
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="required-field">
                    <input name="email" id="hero-email" class="hero-input" type="email" required value="{{old('email')}}" placeholder="Email Address">
                    <span style="position: relative;bottom: 30px;color: red;" id="error_email"></span>
                </div> <!--/ .required-field -->
            </div>
            <div class="col-lg-8 col-md-8">
                <input class="phone hero-input ci-field" id="country-code" required minlength="7" maxlength="14" type="tel" name="phone" style="margin-bottom: 30px;width: 100%;height: 50px;background: rgba(255, 255, 255, 0.5);" onkeypress='return event.charCode == 43 || (event.charCode >= 48 && event.charCode <= 57)' required value="{{old('phone')}}"/>
                <input type="hidden" name="country_code" value="id" class="country_code_from_phone" id="country-code" style="border: 1px solid #000 !important; border-radius: 3px; padding: 10px;color:#000;"/>
                <!-- <input name="heroPhone" id="hero-phone" class="hero-input" type="text" placeholder="Phone Number">
                </div> -->
            </div>
            <div class="col-lg-4 col-md-4">
                @if(App::getLocale() == 'id')
                <button id="hero-submit" type="submit" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 18px 30px;">Lanjut!</button>
                @else
                <button id="hero-submit" type="submit" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 18px 30px;">Next!</button>
                @endif
            </div>
        </div>

        @if(App::getLocale() == 'id')
        <p class="zero-bottom" style="font-size: 14px; font-style: italic;margin-bottom: 10px;text-align: center;">Kami sangat menjaga kerahasian data Anda. Kami tidak akan memperjualbelikan, atau memberikan data Anda kepada pihak lain.</p>
        @else
        <p class="zero-bottom" style="font-size: 14px; font-style: italic;margin-bottom: 10px;text-align: center;">We take your privacy seriously. We will not sell, share or distribute your contact to any third party.</p>
        @endif
        <div id="result"></div>
</form> <!--/ .register-form -->


