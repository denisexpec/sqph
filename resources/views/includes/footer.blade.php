<!-- Footer -->
    <section id="contact_us">
		<footer>
			<div class="container text-center">
				<div class="row">
					<div class="col-lg-3 col-md-3 footer-logo">
						<a class="" href="{{url('')}}">
						<img src="{{asset('ext/images/sq-id/logo.png')}}"></a>
					</div>
					<div class="col-lg-3 col-md-3">
						<p style="text-align: left;font-weight:bold;">Study Query - Australia</p><br/>
						<p style="text-align:left;">Level 3, 393 George St,<br> Sydney New South Wales 2000,<br>Australia</p>
					</div>
					<div class="col-lg-3 col-md-3"> 
						<p style="text-align: left;font-weight:bold;">Study Query - Philippines</p><br/>
						<p style="text-align:left;">5th flr Twin Cities Condominium 110 Legazpi Street,<br> Makati City 1228,<br>Philippines</p>
						<p style="text-align:left">Email: <a href="mailto:help@studyquery.com" style="color: #fff">help@studyquery.com</a></p>
						<p style="text-align:left">Phone Number: <a href="tel:+639270838043" style="color: #fff">+63 927 083 8043</a></p>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="addthis_inline_follow_toolbox">
							<div id="atftbx" class="at-follow-tbx-element addthis-smartlayers addthis-animated at4-show">
								<p>
									<span>Follow</span>
								</p>
								<div class="addthis_toolbox addthis_default_style">
									<a class="at300b at-follow-btn" data-svc="facebook" data-svc-id="studyqueryid" title="Follow on Facebook" href="http://www.facebook.com/studyqueryph" target="_blank">
				<span class="at-icon-wrapper" style="background-color: rgb(59, 89, 152); line-height: 32px; height: 32px; width: 32px; border-radius: 0%;">
					<svg
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-13" class="at-icon at-icon-facebook" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;">
						<title id="at-svg-facebook-13">Facebook</title>
						<g>
							<path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>
						</g>
					</svg>
				</span>
										<span class="addthis_follow_label">
					<span class="at4-visually-hidden">Follow on </span>Facebook
				</span>
									</a>
									<a class="at300b at-follow-btn" data-svc="twitter" data-svc-id="studyquery" title="Follow on Twitter" href="http://twitter.com/intent/follow?source=followbutton&amp;variant=1.0&amp;screen_name=studyquery" target="_blank">
				<span class="at-icon-wrapper" style="background-color: rgb(29, 161, 242); line-height: 32px; height: 32px; width: 32px; border-radius: 0%;">
					<svg
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-14" class="at-icon at-icon-twitter" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;">
						<title id="at-svg-twitter-14">Twitter</title>
						<g>
							<path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path>
						</g>
					</svg>
				</span>
										<span class="addthis_follow_label">
					<span class="at4-visually-hidden">Follow on </span>Twitter
				</span>
									</a>
									<a class="at300b at-follow-btn" data-svc="instagram" data-svc-id="sqtotherescue" title="Follow on Instagram" href="http://instagram.com/sqtotherescue" target="_blank">
				<span class="at-icon-wrapper" style="background-color: rgb(224, 53, 102); line-height: 32px; height: 32px; width: 32px; border-radius: 0%;">
					<svg
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-instagram-15" class="at-icon at-icon-instagram" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;">
						<title id="at-svg-instagram-15">Instagram</title>
						<g>
							<path d="M16 5c-2.987 0-3.362.013-4.535.066-1.17.054-1.97.24-2.67.512a5.392 5.392 0 0 0-1.95 1.268 5.392 5.392 0 0 0-1.267 1.95c-.272.698-.458 1.498-.512 2.67C5.013 12.637 5 13.012 5 16s.013 3.362.066 4.535c.054 1.17.24 1.97.512 2.67.28.724.657 1.337 1.268 1.95a5.392 5.392 0 0 0 1.95 1.268c.698.27 1.498.457 2.67.51 1.172.054 1.547.067 4.534.067s3.362-.013 4.535-.066c1.17-.054 1.97-.24 2.67-.51a5.392 5.392 0 0 0 1.95-1.27 5.392 5.392 0 0 0 1.268-1.95c.27-.698.457-1.498.51-2.67.054-1.172.067-1.547.067-4.534s-.013-3.362-.066-4.535c-.054-1.17-.24-1.97-.51-2.67a5.392 5.392 0 0 0-1.27-1.95 5.392 5.392 0 0 0-1.95-1.267c-.698-.272-1.498-.458-2.67-.512C19.363 5.013 18.988 5 16 5zm0 1.982c2.937 0 3.285.01 4.445.064 1.072.05 1.655.228 2.042.38.514.198.88.437 1.265.822.385.385.624.75.823 1.265.15.387.33.97.38 2.042.052 1.16.063 1.508.063 4.445 0 2.937-.01 3.285-.064 4.445-.05 1.072-.228 1.655-.38 2.042-.198.514-.437.88-.822 1.265-.385.385-.75.624-1.265.823-.387.15-.97.33-2.042.38-1.16.052-1.508.063-4.445.063-2.937 0-3.285-.01-4.445-.064-1.072-.05-1.655-.228-2.042-.38-.514-.198-.88-.437-1.265-.822a3.408 3.408 0 0 1-.823-1.265c-.15-.387-.33-.97-.38-2.042-.052-1.16-.063-1.508-.063-4.445 0-2.937.01-3.285.064-4.445.05-1.072.228-1.655.38-2.042.198-.514.437-.88.822-1.265.385-.385.75-.624 1.265-.823.387-.15.97-.33 2.042-.38 1.16-.052 1.508-.063 4.445-.063zm0 12.685a3.667 3.667 0 1 1 0-7.334 3.667 3.667 0 0 1 0 7.334zm0-9.316a5.65 5.65 0 1 0 0 11.3 5.65 5.65 0 0 0 0-11.3zm7.192-.222a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0" fill-rule="evenodd"></path>
						</g>
					</svg>
				</span>
										<span class="addthis_follow_label">
					<span class="at4-visually-hidden">Follow on </span>Instagram
				</span>
									</a>
									<a class="at300b at-follow-btn" data-svc="messenger" data-svc-id="studyqueryid" title="Follow on Facebook Messenger" href="https://m.me/studyqueryph" target="_blank">
				<span class="at-icon-wrapper" style="background-color: rgb(0, 132, 255); line-height: 32px; height: 32px; width: 32px; border-radius: 0%;">
					<svg
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-messenger-16" class="at-icon at-icon-messenger" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;">
						<title id="at-svg-messenger-16">Facebook Messenger</title>
						<g>
							<path d="M16 6C9.925 6 5 10.56 5 16.185c0 3.205 1.6 6.065 4.1 7.932V28l3.745-2.056c1 .277 2.058.426 3.155.426 6.075 0 11-4.56 11-10.185C27 10.56 22.075 6 16 6zm1.093 13.716l-2.8-2.988-5.467 2.988 6.013-6.383 2.868 2.988 5.398-2.987-6.013 6.383z" fill-rule="evenodd"></path>
						</g>
					</svg>
				</span>
										<span class="addthis_follow_label">
					<span class="at4-visually-hidden">Follow on </span>Facebook Messenger
				</span>
									</a>
									<div class="atclear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p style="margin-top: 50px">© Study Query 2018. All rights reserved.</p>
			</div>
		</footer>
    </section>

	<!-- jQuery -->
	<script src="{{asset('ext/vendor/jquery/jquery.js')}}"></script>

	<!-- Scrolling navigation smooth -->
	<script src="{{asset('ext/vendor/jquery/scrolling-nav.js')}}"></script>

    <!-- Navigation Expand -->
    <script src="{{asset('ext/js/navigationExpan.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('ext/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('ext/js/grayscale.min.js')}}"></script>
    
	<script src="{{asset('ext/js/scrollfixed.js')}}"></script>
	
    <script src="{{asset('ext/custom/ga.js')}}"></script>
	
    
    <script src="{{ asset('ext/js/mainmap.js')}}"></script> 
    
    <script>
    // $('.into_form').click(function() {
    // $('body').animate({
        // scrollTop: eval($('#into_form').offset().top - 70)
        // }, 1000);
    // });
    </script>
	<script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
	0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
	for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
	mixpanel.init("9e358f36874b7f4461ccffd1b6dabc63");</script>


	<script>
        $('select[name=change_language]').on('change',function(e){
            var get_lang = $(this).val();
            $('input[name=lang]').val(get_lang);
            $('.submit_change_language').submit();
        });
    </script>
	<script>
		//submit form contact
        $( "#contact_form" ).submit(function( event ) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var email = $('#hero-email').val();
            //alert(regex.test(email));
            if(regex.test(email) == false)
			{
			    $("#error_email").text("Please enter a valid email");
			    event.preventDefault();
            }
            else
			{
                $("#error_email").text("");
			}

        });

        //submit form contact
        $( "#contact_form_2" ).submit(function( event ) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var email = $('#email').val();
            //alert(regex.test(email));
            if(regex.test(email) == false)
            {
                $("#error_email_2").text("Please enter a valid email");
                event.preventDefault();
            }
            else
            {
                $("#error_email_2").text("");
            }

        });

        $("#country-code").keypress(function() {
            var length = $(this).val().length;
            var telepon = $(this).val();
            if(length == 2)
			{
			    if(telepon == "08")
				{
				    $(this).val("+62");
				}


			}

			if(length == 5)
			{
                if(telepon == "+6208")
                {
                    $(this).val("+62");
                }
			}
        });

        $("#country_phone_2").keypress(function() {
            var length = $(this).val().length;
            var telepon = $(this).val();
            if(length == 2)
            {
                if(telepon == "08")
                {
                    $(this).val("+62");
                }
            }

            if(length == 5)
            {
                if(telepon == "+6208")
                {
                    $(this).val("+62");
                }
            }
        });
	</script>

	<script src="{{ asset('ext/js/phone/build/js/intlTelInput.js')}}"></script>
	<script>
    $(".phone").intlTelInput({
        // allowDropdown: false,
        autoHideDialCode: true,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        //formatOnDisplay: true,
         geoIpLookup: function(callback) {
           $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
             var countryCode = (resp && resp.country) ? resp.country : "";
             callback(countryCode);
           });
         },
        initialCountry: "auto",
        nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        preferredCountries: ['au', 'id', 'my', 'mm', 'ph'],
        separateDialCode: false,
        utilsScript: "ext/js/phone/build/js/utils.js"
    });
    
    
    $(".phone").on("countrychange", function(e, countryData) {
        // do something with countryData
        console.log(countryData.iso2);
        var country_code = countryData.iso2;
        
        $('.country_code_from_phone').val(country_code);
        	
    });
	
  </script>
  
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5aa7796d7c18e2d6"></script>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
{{--   	<script type="text/javascript">
	var addthis_share = {
	   url: "studyquery.com.ph",
	   title: "Study Query Phillipines",
	   description: "THE DESCRIPTION",
	}
</script> --}}
  
</body>

</html>