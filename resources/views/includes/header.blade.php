<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83022576-13"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'UA-83022576-13');
	</script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Open Your New Door To Adventure By Studying Abroad! Get an internationally recognized academic degree and unforgettable life experiences for the rest of your life.">
    <meta name="author" content="">

	<!--FACEBOOK IMAGE-->
	<meta property="og:image" content="{{ asset('ext/images/facebook_img.png') }}">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="1024">
	<meta property="og:image:height" content="1024">
	<meta property="og:title" content="StudyQuery Phillipines | Smart Choices for Smart Students">
	<meta property="og:url" content="http://studyquery.com.ph/">
	<meta property="og:description" content="Open Your New Door To Adventure By Studying Abroad! Get an internationally recognized academic degree and unforgettable life experiences for the rest of your life.">
	
	
	
    <title>Study Query | 
	@if($title)
	{{$title}}
	@endif
	</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('ext/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('ext/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500" rel="stylesheet">
	<!--telphone section-->
	<link rel="stylesheet" href="{{asset('ext/js/phone/build/css/intlTelInput.css')}}">
	<link rel="stylesheet" href="{{asset('ext/js/phone/build/css/demo.css')}}">

    <!-- Theme CSS -->
    <link href="{{asset('ext/css/grayscale.css')}}" rel="stylesheet">
	<link href="{{asset('ext/css/font-awesome.min.css')}}" rel="stylesheet">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
	<!-- Smooth scrolling -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.6/jquery.fullpage.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.6/vendors/scrolloverflow.min.js"></script>
	
	<script src="{{asset('ext/custom/facebookpixel.js')}}"></script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=344245682617173&ev=PageView&noscript=1"
	/></noscript>
	
	
	<script>
	fbq('track', 'Lead');
	</script>
	
	
	<style>
		span.country-name{
			color:black;
		}
	
	
	</style>

	<!-- Facebook Pixel Code -->
	<script>
 		!function(f,b,e,v,n,t,s)
 		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
 		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
 		n.queue=[];t=b.createElement(e);t.async=!0;
 		t.src=v;s=b.getElementsByTagName(e)[0];
 		s.parentNode.insertBefore(t,s)}(window, document,'script',
 		'https://connect.facebook.net/en_US/fbevents.js');
 		fbq('init', '344245682617173');
 		fbq('track', 'PageView');
	</script>
	
	<noscript><img height="1" width="1" style="display:none"
 src="https://www.facebook.com/tr?id=344245682617173&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>