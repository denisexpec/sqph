

<section class="page-section bg-scroll form__Banner" data-background="{{asset('ext/images/sq-id/footer_banner.jpg')}}" id="from-banner-home" style="height: 610px;margin-top: 0px;width: 420px;">
                <div class="">
                    <!-- Section Headings -->
							@if(App::getLocale() == 'id')
                            <div class="section-title white" style="color:#333;text-transform: capitalize;letter-spacing:1px;font-size:25px;">
                                KONSULTASI GRATIS DENGAN KAMI
                            </div> 
                            <h2 class="section-heading white" style="color: #333;font-family: inherit;font-weight:500; text-transform: lowercase;margin-bottom: 25px;font-size: 14px;">Isilah form di bawah ini sesuai data Anda, para konselor berpengalaman kami akan menghubungi anda secepatnya.</h2>
							@else
							<div class="section-title white" style="color:#333">
                                FREE CONSULTAION WITH EXPERTS
                            </div>
                            <h2 class="section-heading white" style="color: #333;font-family: inherit;font-weight:500; text-transform: lowercase;margin-bottom: 25px;font-size: 14px;">Fill out the form below with your data, our counsellors will contact you as soon as possible.</h2>
							@endif
                    <!-- End Section Headings -->
                    
                    <!-- form-->
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
									</ul>
								</div>
							@endif
							
							
                            <!-- Contact Form -->                            
                            <form class="form contact-form form-banner" id="contact_form" autocomplete="off" method="post" enctype="multipart/form-data" 
							@if($lead_source == 'parent')
							action="{{url('parent/insertlead')}}"
							@else
							action="{{url('insertlead')}}"
							@endif
							>
                                <div class="clearfix mb-20 mb-xs-0">
                                    
                                    {{csrf_field()}}
                                    <input type="hidden" name="lead_source" value="{{$lead_source}}"/>
                                    <input type="hidden" name="language_setting" value="{{App::getLocale()}}"/>
									
									@if(isset($_GET['utm_source']))
									<input type="hidden" name="utm_source" value="{{$_GET['utm_source']}}"/>
									@else
										@if(isset($_COOKIE['utm_source']))
											<input type="hidden" name="utm_source" value="{{$_COOKIE['utm_source']}}"/>
										@else
											<input type="hidden" name="utm_source" value="{{old('utm_source')}}"/>
										@endif
									@endif
									
									@if(isset($_GET['adset']))
									<input type="hidden" name="adset" value="{{$_GET['adset']}}"/>
									@else
										@if(isset($_COOKIE['adset']))
											<input type="hidden" name="adset" value="{{$_COOKIE['adset']}}"/>
										@else
											<input type="hidden" name="adset" value="{{old('adset')}}"/>
										@endif
									@endif
								
									@if(isset($_GET['ads']))
									<input type="hidden" name="ads" value="{{$_GET['ads']}}"/>
									@else
										@if(isset($_COOKIE['ads']))
											<input type="hidden" name="ads" value="{{$_COOKIE['ads']}}"/>
										@else
											<input type="hidden" name="ads" value="{{old('ads')}}"/>
										@endif
									@endif
								
									@if(isset($_GET['utm_campaign']))
									<input type="hidden" name="campaign" value="{{$_GET['utm_campaign']}}"/>
									@else
										@if(isset($_COOKIE['utm_campaign']))
											<input type="hidden" name="campaign" value="{{$_COOKIE['utm_campaign']}}"/>
										@else
											<input type="hidden" name="campaign" value="{{old('utm_campaign')}}"/>
										@endif
									@endif
									
									@if($utm_visitor)
										<input type="hidden" name="utm_visitor" value="{{$utm_visitor}}"/>
									@else
										<input type="hidden" name="utm_visitor" value="{{old('utm_visitor')}}"/>
									@endif
									
									@if(isset($_GET['utm_term']))
									<input type="hidden" name="term" value="{{$_GET['utm_term']}}"/>
									@else
										@if(isset($_COOKIE['utm_term']))
											<input type="hidden" name="term" value="{{$_COOKIE['utm_term']}}"/>
										@else
											<input type="hidden" name="term" value="{{old('utm_term')}}"/>
										@endif
									@endif
									
									
									<div class="form-group banner_H">
										<input type="text" name="first_name" id="first-name" class="ci-field form-control" placeholder="First Name" required value="{{old('first_name')}}" style="border: 1px solid #000 !important; border-radius: 3px; padding: 10px;color:#000;">
									</div>
									<div class="form-group banner_H">
										<input type="text" name="last_name" id="last-name" class="ci-field form-control" placeholder="Last Name" required value="{{old('last_name')}}" style="border: 1px solid #000 !important; border-radius: 3px; padding: 10px;color:#000;">
									</div>
									<div class="form-group banner_H">
										<input class="phone form-control ci-field" id="country-code" type="tel" name="phone" style="margin-top:8px !important; border: 1px solid #000 !important; border-radius: 3px;color:#000;" onkeypress='return event.charCode == 43 || (event.charCode >= 48 && event.charCode <= 57)' required value="{{old('phone')}}"/>
										<input type="hidden" name="country_code" value="id" class="country_code_from_phone" id="country-code" style="border: 1px solid #000 !important; border-radius: 3px; padding: 10px;color:#000;"/>
									</div>
									<div class="form-group banner_H">
										<input type="email" name="email" id="email" class="ci-field form-control" placeholder="Email" required value="{{old('email')}}" style="border: 1px solid #000 !important; border-radius: 3px; padding: 10px;color:#000;">
									</div>
                                </div>
                                <!-- Send Button -->
                                <button class="btn btn-default btn-lg btn-sq-cta" id="submit_btn" style="font-size: 12px;background: #0890A1
;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;"> Next! </button>
								@if(App::getLocale() == 'id')
                                <p style="color:#fff; font-style:italic; font-size: 14px;opacity:.6;margin-top: 25px;">Kami sangat menjaga kerahasian data Anda. Kami tidak akan memperjualbelikan, atau memberikan data Anda kepada pihak lain.</p>
								@else
								<p style="color:#fff; font-style:italic; font-size: 14px;opacity:.6;margin-top: 25px;">We take your privacy seriously. We will not sell, share or distribute your contact to any third party.</p>	
								@endif
								
								
								
                                <div id="result"></div>           
                            </form>                            
                            <!-- End Contact Form -->
			


                    </div>
                    <!-- End form -->
                    
                </div>
	</section>