<div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="100">
    <!-- Post -->
    <div class="blog-item">
        <!-- Image -->
        <div class="blog-media relative">    
            <a href="https://www.facebook.com/studyqueryph/photos/a.149308482438555.1073741828.140336216669115/161968424505894/?type=3&theater" class="lp-item animate-init" target="_blank">
                <div class="lp-image" style="background-image: url(https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/29196501_161968427839227_724743918377238528_n.png?_nc_cat=0&_nc_eui2=v1%3AAeF4TwwjGueIuCGPSnNgOEcJR9N1dmTXKeMJvG39HXKnKCtW0F73Q4tMsvcIE6japsBZZCP_pysn2LijPvu-syFmMPPo4YdMLlmNQJWWO7bU_g&oh=b2e873a6eba1f13689210d302c7e32d9&oe=5B45742A);"></div>
            </a>
        </div>
        <!-- Post Title -->
        <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px; font-weight: bold;">
                 MORE THAN THAT </p></h2>
        <!-- Author, Categories, Comments -->
        <!-- @if(false) -->
        @if(false)
        <div class="blog-item-data mb-10" style="margin-bottom:15px;">
            <span class="number">5</span>&nbsp;Likes
            <span class="separator">&mdash;</span>
            <span class="number">0</span>&nbsp;Comments
        </div>
        @endif
        <!-- @endif -->
        <!-- Text Intro -->
        <div class="blog-item-body">
            <p style="font-size: 14px; color:#7e8890;">
                Giving consultations to students looking to study abroad have become an industry ...
            </p>
        </div>
        <!-- Read More Link -->
        <div class="blog-item-foot">
                <a href="https://www.facebook.com/studyqueryph/photos/a.149308482438555.1073741828.140336216669115/161968424505894/?type=3&theater" target="_blank" class="btn btn-mod btn-small" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 15px 20px;">Read More <!-- <i class="fa fa-angle-right"></i> --></a>
        </div>
    </div>
    <!-- End Post -->
</div>

<div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="100">
    <!-- Post -->
    <div class="blog-item">
        <!-- Image -->
        <div class="blog-media relative">
            <a href="https://www.facebook.com/studyqueryph/photos/a.149308482438555.1073741828.140336216669115/161442934558443/?type=3&theater" target="_blank">
                <div class="lp-image" style="background-image: url(https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/29136576_161442937891776_7708264272412803072_n.png?_nc_cat=0&_nc_eui2=v1%3AAeHXKo4KCsyZz_RaUtQZxb4NwtSxP9gTDmqxpWO61_y_70aed2d11R_BA3rfWiTdCoPEtz1LIzklQ-Pr5ClupNOA1-AfG8To-jf8Cek6IIjhRA&oh=b056aaa6c4762c31397c45797a399c60&oe=5B2CD946);"></div>

            </a>
        </div>
        <!-- Post Title -->
        <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px; font-weight: bold;">Why settle for less?</p></h2>
        <!-- Author, Categories, Comments -->
    <!-- @if(false) -->
        @if(false)
            <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                <span class="number">5</span>&nbsp;Likes
                <span class="separator">&mdash;</span>
                <span class="number">0</span>&nbsp;Comments
            </div>
    @endif
    <!-- @endif -->
        <!-- Text Intro -->
        <div class="blog-item-body">
            <p style="font-size: 14px; color:#7e8890;">
                You wouldn’t choose listening to a Maroon 5 cover band over listening to Adam ...
            </p>
        </div>
        <!-- Read More Link -->
        <div class="blog-item-foot">
            <a href="https://www.facebook.com/studyqueryph/photos/a.149308482438555.1073741828.140336216669115/161442934558443/?type=3&theater" target="_blank" class="btn btn-mod btn-small" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 15px 20px;">Read More <!-- <i class="fa fa-angle-right"></i> --></a>
        </div>
    </div>
    <!-- End Post -->
</div>

<div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="100">
    <!-- Post -->
    <div class="blog-item">
        <!-- Image -->
        <div class="blog-media relative">    
            <a href="https://www.facebook.com/studyqueryph/photos/a.149308482438555.1073741828.140336216669115/161115534591183/?type=3&theater" class="lp-item animate-init" target="_blank">
                <div class="lp-image" style="background-image: url(https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/29066592_161115537924516_6184495349936160768_n.jpg?_nc_cat=0&_nc_eui2=v1%3AAeGu-dvMSmQ5TRJ2eiDZmEuJsnduVA5FMez03P4JOOkKGdoNrNxCZh_Th28k8YuQMTEPLeZNvGoIO9XS1l6A5-Ji4pexgJGCa4nZ1K6Pzj6QqA&oh=737a218d83b9ab202398243a84d15693&oe=5B4594BB);"></div>

            </a>
        </div>
        <!-- Post Title -->
        <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px; font-weight: bold;">The one to trust</p></h2>
        <!-- Author, Categories, Comments -->
        <!-- @if(false) -->
        @if(false)
        <div class="blog-item-data mb-10" style="margin-bottom:15px;">
            <span class="number">5</span>&nbsp;Likes
            <span class="separator">&mdash;</span>
            <span class="number">0</span>&nbsp;Comments
        </div>
        @endif
        <!-- @endif -->
        <!-- Text Intro -->
        <div class="blog-item-body">
            <p style="font-size: 14px; color:#7e8890;">
                BEWARE OF FALSE NEWS. It’s no longer a secret that social media has become a place ...
            </p>
        </div>
        <!-- Read More Link -->
        <div class="blog-item-foot">
                <a href="https://www.facebook.com/studyqueryph/photos/a.149308482438555.1073741828.140336216669115/161115534591183/?type=3&theater" target="_blank" class="btn btn-mod btn-small" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 15px 20px;">Read More <!-- <i class="fa fa-angle-right"></i> --></a>
        </div>
    </div>
    <!-- End Post -->
</div>