<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>StudyQuery | Survey</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- master css -->
    <link href="{{ asset('ext/sqlp/css/master.css') }}" rel="stylesheet">
    <link href="{{ asset('ext/sqlp/css/color.css') }}" rel="stylesheet">
    <link href="{{asset('ext/sqlp/css/style2.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('ext/sqlp/images/favicon.ico') }}">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'><!--[if lt IE 9]>
    <![endif]-->


    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '344245682617173'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=344245682617173&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

</head>
<body>
<div class="full">
<!-- <div class="container main" id="survey01">
                <img src="{{ asset('ext/sqlp/images/SQ/logo-sq.png') }}" alt="StudyQuery">
				@if(App::getLocale() == 'id')
    <h1>PENDIDIKAN KELAS DUNIAMU DIMULAI SEKARANG</h1>
    <p>Dapatkan universitas terbaik di luar negeri!</p>
    <a class="btnn">AYO MULAI!</a>
@else
    <h1>YOUR WORLD-CLASS HIGHER EDUCATION STARTS NOW</h1>
    <p>Get the BEST universities & colleges to study abroad!</p>
    <a class="btnn">LET'S BEGIN</a>
@endif
        </div> -->

    <div class="container question" id="survey02">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2 class="text-center"><b>Isi dan lengkapi pertanyaan di bawah ini agar konsultan pendidikan dapat memberikan solusi paling tepat.</b></h2>
                <!-- <p class="text-center">Layanan konsultasi kami gratis. Ayo luangkan waktu untuk mengisi informasi detail Anda agar kami dapat memberikan solusi yang terbaik.</p> -->
            @else
                <h2 class="text-center"><b>Fill in the following questions carefully and our counsellor will provide the best solution for you.</b></h2>
                <!-- <p class="text-center">Our consultation services are free. Take a second to enter your details and we can give you the best solution.</p> -->
            @endif

            @if(App::getLocale() == 'id')
                <span>Berapa usiamu sekarang? (*)</span>
            @else
                <span>Which age group do you belong to? (*)</span>
            @endif
            <select name="age_group" id="age_group" required>
                <option value="<16" data-score="0"><16</option>
                <option value="16-18" data-score="0">16-18</option>
                <option value="19-23" data-score="0">19-23</option>
                <option value="24-30" data-score="0">24-30</option>
                <option value="31-40" data-score="0">31-40</option>
                <option value=">40" data-score="0">>40</option>
            </select>

            @if(App::getLocale() == 'id')
                <span>Saya adalah seorang: (*)</span>
                <select name="gender" id="gender" required>
                    <option value="Male">Laki-laki</option>
                    <option value="Female">Perempuan</option>
                    <option value="Preferable_not">Rahasia</option>
                </select>
                <span>Saat ini kamu tinggal di : (*)</span>
                    <select name="current_country" required class="curetnt_country">
                        <optgroup label="Indonesia">
                            <option data-countrycode="ID " value="Aceh">Aceh</option>
                            <option data-countrycode="ID " value="Sumatra - Sumatra Utara">Sumatra - Sumatra Utara</option>
                            <option data-countrycode="ID " value="Sumatra - Sumatra Barat">Sumatra - Sumatra Barat</option>
                            <option data-countrycode="ID " value="Sumatra - Riau/Kep. Riau">Sumatra - Riau/Kep. Riau</option>
                            <option data-countrycode="ID " value="Sumatra - Jambi/Bengkulu">Sumatra - Jambi/Bengkulu</option>
                            <option data-countrycode="ID " value="Sumatra - Sumatra Selatan/Lampung/Bangka Belitung">Sumatra - Sumatra Selatan/Lampung/Bangka Belitung</option>
                            <option data-countrycode="ID " value="Jawa - DKI Jakarta" selected>Jawa - DKI Jakarta</option>
                            <option data-countrycode="ID " value="Jawa - Banten">Jawa - Banten</option>
                            <option data-countrycode="ID " value="Jawa - Jawa Barat">Jawa - Jawa Barat</option>
                            <option data-countrycode="ID " value="Jawa - Jawa Tengah/Yogyakarta">Jawa - Jawa Tengah/Yogyakarta</option>
                            <option data-countrycode="ID " value="Jawa - Jawa Timur">Jawa - Jawa Timur</option>
                            <option data-countrycode="ID " value="Kalimantan - KalBar">Kalimantan - Kalimantan Barat</option>
                            <option data-countrycode="ID " value="Kalimantan - KalTeng">Kalimantan - Kalimantan Tengah</option>
                            <option data-countrycode="ID " value="Kalimantan - KalTim">Kalimantan - Kalimantan Timur</option>
                            <option data-countrycode="ID " value="Kalimantan - KalSel">Kalimantan - Kalimantan Selatan</option>
                            <option data-countrycode="ID " value="Sulawesi - Sulawesi Utara/Gorontalo">Sulawesi - Sulawesi Utara/Gorontalo</option>
                            <option data-countrycode="ID " value="Sulawesi - Sulawesi Barat/Tengah">Sulawesi - Sulawesi Barat/Tengah</option>
                            <option data-countrycode="ID " value="Sulawesi - Sulawesi Selatan">Sulawesi - Sulawesi Selatan</option>
                            <option data-countrycode="ID " value="Sulawesi - Sulawesi Tenggara">Sulawesi - Sulawesi Tenggara</option>
                            <option data-countrycode="ID " value="Bali">Bali</option>
                            <option data-countrycode="ID " value="Nusa Tenggara">Nusa Tenggara</option>
                            <option data-countrycode="ID " value="Maluku">Maluku</option>
                            <option data-countrycode="ID " value="Papua">Papua</option>
                        </optgroup>
                        <option data-countrycode="SEA" value="Southeast Asia">Asia Tenggara</option>
                        <option data-countrycode="Asia" value="Other Asia">Negara Asia Lainnya</option>
                        <option data-countrycode="AU" value="Australia">Australia</option>
                        <option data-countrycode="NZ" value="New Zealand">Selandia Baru</option>
                        <option data-countrycode="NZ" value="Middle East">Timur Tengah</option>
                        <option data-countrycode="EU" value="Europe">Eropa</option>
                        <option data-countrycode="USA" value="USA">Amerika Serikat</option>
                        <option data-countrycode="SA" value="South America">Amerika Selatan</option>
                        <option data-countrycode="Others" value="Others">Negara Lainnya</option>
                    </select>
            @else
                <span>I identify my gender as: (*)</span>
                <select name="gender" id="gender" required>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Preferable_not">Preferable not to say</option>
                </select>
                <span>Are you currently in: (*)</span>
                    <select name="current_country" required class="curetnt_country">
                        <option data-countrycode="AF " value="Afghanistan">Afghanistan</option>
                        <option data-countrycode="AX " value="Åland Islands">Åland Islands</option>
                        <option data-countrycode="AL " value="Albania">Albania</option>
                        <option data-countrycode="DZ " value="Algeria">Algeria</option>
                        <option data-countrycode="AS " value="American Samoa">American Samoa</option>
                        <option data-countrycode="AD " value="Andorra">Andorra</option>
                        <option data-countrycode="AO " value="Angola">Angola</option>
                        <option data-countrycode="AI " value="Anguilla">Anguilla</option>
                        <option data-countrycode="AQ " value="Antarctica">Antarctica</option>
                        <option data-countrycode="AG " value="Antigua and Barbuda">Antigua and Barbuda</option>
                        <option data-countrycode="AR " value="Argentina">Argentina</option>
                        <option data-countrycode="AM " value="Armenia">Armenia</option>
                        <option data-countrycode="AW " value="Aruba">Aruba</option>
                        <option data-countrycode="AU " value="Australia" selected>Australia</option>
                        <option data-countrycode="AT " value="Austria">Austria</option>
                        <option data-countrycode="AZ " value="Azerbaijan">Azerbaijan</option>
                        <option data-countrycode="BS " value="The Bahamas">The Bahamas</option>
                        <option data-countrycode="BH " value="Bahrain">Bahrain</option>
                        <option data-countrycode="BD " value="Bangladesh">Bangladesh</option>
                        <option data-countrycode="BB " value="Barbados">Barbados</option>
                        <option data-countrycode="BY " value="Belarus">Belarus</option>
                        <option data-countrycode="BE " value="Belgium">Belgium</option>
                        <option data-countrycode="BZ " value="Belize">Belize</option>
                        <option data-countrycode="BJ " value="Benin">Benin</option>
                        <option data-countrycode="BM " value="Bermuda">Bermuda</option>
                        <option data-countrycode="BT " value="Bhutan">Bhutan</option>
                        <option data-countrycode="BO " value="Bolivia">Bolivia</option>
                        <option data-countrycode="BQ " value="Bonaire">Bonaire</option>
                        <option data-countrycode="BA " value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                        <option data-countrycode="BW " value="Botswana">Botswana</option>
                        <option data-countrycode="BV " value="Bouvet Island">Bouvet Island</option>
                        <option data-countrycode="BR " value="Brazil">Brazil</option>
                        <option data-countrycode="IO " value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option data-countrycode="UM " value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                        <option data-countrycode="VG " value="Virgin Islands (British)">Virgin Islands (British)</option>
                        <option data-countrycode="VI " value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                        <option data-countrycode="BN " value="Brunei">Brunei</option>
                        <option data-countrycode="BG " value="Bulgaria">Bulgaria</option>
                        <option data-countrycode="BF " value="Burkina Faso">Burkina Faso</option>
                        <option data-countrycode="BI " value="Burundi">Burundi</option>
                        <option data-countrycode="KH " value="Cambodia">Cambodia</option>
                        <option data-countrycode="CM " value="Cameroon">Cameroon</option>
                        <option data-countrycode="CA " value="Canada">Canada</option>
                        <option data-countrycode="CV " value="Cape Verde">Cape Verde</option>
                        <option data-countrycode="KY " value="Cayman Islands">Cayman Islands</option>
                        <option data-countrycode="CF " value="Central African Republic">Central African Republic</option>
                        <option data-countrycode="TD " value="Chad">Chad</option>
                        <option data-countrycode="CL " value="Chile">Chile</option>
                        <option data-countrycode="CN " value="China">China</option>
                        <option data-countrycode="CX " value="Christmas Island">Christmas Island</option>
                        <option data-countrycode="CC " value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option data-countrycode="CO " value="Colombia">Colombia</option>
                        <option data-countrycode="KM " value="Comoros">Comoros</option>
                        <option data-countrycode="CG " value="Republic of the Congo">Republic of the Congo</option>
                        <option data-countrycode="CD " value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                        <option data-countrycode="CK " value="Cook Islands">Cook Islands</option>
                        <option data-countrycode="CR " value="Costa Rica">Costa Rica</option>
                        <option data-countrycode="HR " value="Croatia">Croatia</option>
                        <option data-countrycode="CU " value="Cuba">Cuba</option>
                        <option data-countrycode="CW " value="Curaçao">Curaçao</option>
                        <option data-countrycode="CY " value="Cyprus">Cyprus</option>
                        <option data-countrycode="CZ " value="Czech Republic">Czech Republic</option>
                        <option data-countrycode="DK " value="Denmark">Denmark</option>
                        <option data-countrycode="DJ " value="Djibouti">Djibouti</option>
                        <option data-countrycode="DM " value="Dominica">Dominica</option>
                        <option data-countrycode="DO " value="Dominican Republic">Dominican Republic</option>
                        <option data-countrycode="EC " value="Ecuador">Ecuador</option>
                        <option data-countrycode="EG " value="Egypt">Egypt</option>
                        <option data-countrycode="SV " value="El Salvador">El Salvador</option>
                        <option data-countrycode="GQ " value="Equatorial Guinea">Equatorial Guinea</option>
                        <option data-countrycode="ER " value="Eritrea">Eritrea</option>
                        <option data-countrycode="EE " value="Estonia">Estonia</option>
                        <option data-countrycode="ET " value="Ethiopia">Ethiopia</option>
                        <option data-countrycode="FK " value="Falkland Islands">Falkland Islands</option>
                        <option data-countrycode="FO " value="Faroe Islands">Faroe Islands</option>
                        <option data-countrycode="FJ " value="Fiji">Fiji</option>
                        <option data-countrycode="FI " value="Finland">Finland</option>
                        <option data-countrycode="FR " value="France">France</option>
                        <option data-countrycode="GF " value="French Guiana">French Guiana</option>
                        <option data-countrycode="PF " value="French Polynesia">French Polynesia</option>
                        <option data-countrycode="TF " value="French Southern and Antarctic Lands">French Southern and Antarctic Lands</option>
                        <option data-countrycode="GA " value="Gabon">Gabon</option>
                        <option data-countrycode="GM " value="The Gambia">The Gambia</option>
                        <option data-countrycode="GE " value="Georgia">Georgia</option>
                        <option data-countrycode="DE " value="Germany">Germany</option>
                        <option data-countrycode="GH " value="Ghana">Ghana</option>
                        <option data-countrycode="GI " value="Gibraltar">Gibraltar</option>
                        <option data-countrycode="GR " value="Greece">Greece</option>
                        <option data-countrycode="GL " value="Greenland">Greenland</option>
                        <option data-countrycode="GD " value="Grenada">Grenada</option>
                        <option data-countrycode="GP " value="Guadeloupe">Guadeloupe</option>
                        <option data-countrycode="GU " value="Guam">Guam</option>
                        <option data-countrycode="GT " value="Guatemala">Guatemala</option>
                        <option data-countrycode="GG " value="Guernsey">Guernsey</option>
                        <option data-countrycode="GN " value="Guinea">Guinea</option>
                        <option data-countrycode="GW " value="Guinea-Bissau">Guinea-Bissau</option>
                        <option data-countrycode="GY " value="Guyana">Guyana</option>
                        <option data-countrycode="HT " value="Haiti">Haiti</option>
                        <option data-countrycode="HM " value="Heard Island and McDonald Islands">Heard Island and
                            McDonald Islands
                        </option>
                        <option data-countrycode="VA " value="Holy See">Holy See</option>
                        <option data-countrycode="HN " value="Honduras">Honduras</option>
                        <option data-countrycode="HK " value="Hong Kong">Hong Kong</option>
                        <option data-countrycode="HU " value="Hungary">Hungary</option>
                        <option data-countrycode="IS " value="Iceland">Iceland</option>
                        <option data-countrycode="IN " value="India">India</option>
                        <option data-countrycode="ID " value="Indonesia">Indonesia</option>
                        <option data-countrycode="CI " value="Ivory Coast">Ivory Coast</option>
                        <option data-countrycode="IR " value="Iran">Iran</option>
                        <option data-countrycode="IQ " value="Iraq">Iraq</option>
                        <option data-countrycode="IE " value="Republic of Ireland">Republic of Ireland</option>
                        <option data-countrycode="IM " value="Isle of Man">Isle of Man</option>
                        <option data-countrycode="IL " value="Israel">Israel</option>
                        <option data-countrycode="IT " value="Italy">Italy</option>
                        <option data-countrycode="JM " value="Jamaica">Jamaica</option>
                        <option data-countrycode="JP " value="Japan">Japan</option>
                        <option data-countrycode="JE " value="Jersey">Jersey</option>
                        <option data-countrycode="JO " value="Jordan">Jordan</option>
                        <option data-countrycode="KZ " value="Kazakhstan">Kazakhstan</option>
                        <option data-countrycode="KE " value="Kenya">Kenya</option>
                        <option data-countrycode="KI " value="Kiribati">Kiribati</option>
                        <option data-countrycode="KW " value="Kuwait">Kuwait</option>
                        <option data-countrycode="KG " value="Kyrgyzstan">Kyrgyzstan</option>
                        <option data-countrycode="LA " value="Laos">Laos</option>
                        <option data-countrycode="LV " value="Latvia">Latvia</option>
                        <option data-countrycode="LB " value="Lebanon">Lebanon</option>
                        <option data-countrycode="LS " value="Lesotho">Lesotho</option>
                        <option data-countrycode="LR " value="Liberia">Liberia</option>
                        <option data-countrycode="LY " value="Libya">Libya</option>
                        <option data-countrycode="LI " value="Liechtenstein">Liechtenstein</option>
                        <option data-countrycode="LT " value="Lithuania">Lithuania</option>
                        <option data-countrycode="LU " value="Luxembourg">Luxembourg</option>
                        <option data-countrycode="MO " value="Macau">Macau</option>
                        <option data-countrycode="MK " value="Republic of Macedonia">Republic of Macedonia</option>
                        <option data-countrycode="MG " value="Madagascar">Madagascar</option>
                        <option data-countrycode="MW " value="Malawi">Malawi</option>
                        <option data-countrycode="MY " value="Malaysia">Malaysia</option>
                        <option data-countrycode="MV " value="Maldives">Maldives</option>
                        <option data-countrycode="ML " value="Mali">Mali</option>
                        <option data-countrycode="MT " value="Malta">Malta</option>
                        <option data-countrycode="MH " value="Marshall Islands">Marshall Islands</option>
                        <option data-countrycode="MQ " value="Martinique">Martinique</option>
                        <option data-countrycode="MR " value="Mauritania">Mauritania</option>
                        <option data-countrycode="MU " value="Mauritius">Mauritius</option>
                        <option data-countrycode="YT " value="Mayotte">Mayotte</option>
                        <option data-countrycode="MX " value="Mexico">Mexico</option>
                        <option data-countrycode="FM " value="Federated States of Micronesia">Federated States of
                            Micronesia
                        </option>
                        <option data-countrycode="MD " value="Moldova">Moldova</option>
                        <option data-countrycode="MC " value="Monaco">Monaco</option>
                        <option data-countrycode="MN " value="Mongolia">Mongolia</option>
                        <option data-countrycode="ME " value="Montenegro">Montenegro</option>
                        <option data-countrycode="MS " value="Montserrat">Montserrat</option>
                        <option data-countrycode="MA " value="Morocco">Morocco</option>
                        <option data-countrycode="MZ " value="Mozambique">Mozambique</option>
                        <option data-countrycode="MM " value="Myanmar">Myanmar</option>
                        <option data-countrycode="NA " value="Namibia">Namibia</option>
                        <option data-countrycode="NR " value="Nauru">Nauru</option>
                        <option data-countrycode="NP " value="Nepal">Nepal</option>
                        <option data-countrycode="NL " value="Netherlands">Netherlands</option>
                        <option data-countrycode="NC " value="New Caledonia">New Caledonia</option>
                        <option data-countrycode="NZ " value="New Zealand">New Zealand</option>
                        <option data-countrycode="NI " value="Nicaragua">Nicaragua</option>
                        <option data-countrycode="NE " value="Niger">Niger</option>
                        <option data-countrycode="NG " value="Nigeria">Nigeria</option>
                        <option data-countrycode="NU " value="Niue">Niue</option>
                        <option data-countrycode="NF " value="Norfolk Island">Norfolk Island</option>
                        <option data-countrycode="KP " value="North Korea">North Korea</option>
                        <option data-countrycode="MP " value="Northern Mariana Islands">Northern Mariana Islands
                        </option>
                        <option data-countrycode="NO " value="Norway">Norway</option>
                        <option data-countrycode="OM " value="Oman">Oman</option>
                        <option data-countrycode="PK " value="Pakistan">Pakistan</option>
                        <option data-countrycode="PW " value="Palau">Palau</option>
                        <option data-countrycode="PS " value="Palestine">Palestine</option>
                        <option data-countrycode="PA " value="Panama">Panama</option>
                        <option data-countrycode="PG " value="Papua New Guinea">Papua New Guinea</option>
                        <option data-countrycode="PY " value="Paraguay">Paraguay</option>
                        <option data-countrycode="PE " value="Peru">Peru</option>
                        <option data-countrycode="PH " value="Philippines" selected>Philippines</option>
                        <option data-countrycode="PN " value="Pitcairn Islands">Pitcairn Islands</option>
                        <option data-countrycode="PL " value="Poland">Poland</option>
                        <option data-countrycode="PT " value="Portugal">Portugal</option>
                        <option data-countrycode="PR " value="Puerto Rico">Puerto Rico</option>
                        <option data-countrycode="QA " value="Qatar">Qatar</option>
                        <option data-countrycode="XK " value="Republic of Kosovo">Republic of Kosovo</option>
                        <option data-countrycode="RE " value="Réunion">Réunion</option>
                        <option data-countrycode="RO " value="Romania">Romania</option>
                        <option data-countrycode="RU " value="Russia">Russia</option>
                        <option data-countrycode="RW " value="Rwanda">Rwanda</option>
                        <option data-countrycode="BL " value="Saint Barthélemy">Saint Barthélemy</option>
                        <option data-countrycode="SH " value="Saint Helena">Saint Helena</option>
                        <option data-countrycode="KN " value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option data-countrycode="LC " value="Saint Lucia">Saint Lucia</option>
                        <option data-countrycode="MF " value="Saint Martin">Saint Martin</option>
                        <option data-countrycode="PM " value="Saint Pierre and Miquelon">Saint Pierre and Miquelon
                        </option>
                        <option data-countrycode="VC " value="Saint Vincent and the Grenadines">Saint Vincent and the
                            Grenadines
                        </option>
                        <option data-countrycode="WS " value="Samoa">Samoa</option>
                        <option data-countrycode="SM " value="San Marino">San Marino</option>
                        <option data-countrycode="ST " value="São Tomé and Príncipe">São Tomé and Príncipe</option>
                        <option data-countrycode="SA " value="Saudi Arabia">Saudi Arabia</option>
                        <option data-countrycode="SN " value="Senegal">Senegal</option>
                        <option data-countrycode="RS " value="Serbia">Serbia</option>
                        <option data-countrycode="SC " value="Seychelles">Seychelles</option>
                        <option data-countrycode="SL " value="Sierra Leone">Sierra Leone</option>
                        <option data-countrycode="SG " value="Singapore">Singapore</option>
                        <option data-countrycode="SX " value="Sint Maarten">Sint Maarten</option>
                        <option data-countrycode="SK " value="Slovakia">Slovakia</option>
                        <option data-countrycode="SI " value="Slovenia">Slovenia</option>
                        <option data-countrycode="SB " value="Solomon Islands">Solomon Islands</option>
                        <option data-countrycode="SO " value="Somalia">Somalia</option>
                        <option data-countrycode="ZA " value="South Africa">South Africa</option>
                        <option data-countrycode="GS " value="South Georgia">South Georgia</option>
                        <option data-countrycode="KR " value="South Korea">South Korea</option>
                        <option data-countrycode="SS " value="South Sudan">South Sudan</option>
                        <option data-countrycode="ES " value="Spain">Spain</option>
                        <option data-countrycode="LK " value="Sri Lanka">Sri Lanka</option>
                        <option data-countrycode="SD " value="Sudan">Sudan</option>
                        <option data-countrycode="SR " value="Suriname">Suriname</option>
                        <option data-countrycode="SJ " value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                        <option data-countrycode="SZ " value="Swaziland">Swaziland</option>
                        <option data-countrycode="SE " value="Sweden">Sweden</option>
                        <option data-countrycode="CH " value="Switzerland">Switzerland</option>
                        <option data-countrycode="SY " value="Syria">Syria</option>
                        <option data-countrycode="TW " value="Taiwan">Taiwan</option>
                        <option data-countrycode="TJ " value="Tajikistan">Tajikistan</option>
                        <option data-countrycode="TZ " value="Tanzania">Tanzania</option>
                        <option data-countrycode="TH " value="Thailand">Thailand</option>
                        <option data-countrycode="TL " value="East Timor">East Timor</option>
                        <option data-countrycode="TG " value="Togo">Togo</option>
                        <option data-countrycode="TK " value="Tokelau">Tokelau</option>
                        <option data-countrycode="TO " value="Tonga">Tonga</option>
                        <option data-countrycode="TT " value="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option data-countrycode="TN " value="Tunisia">Tunisia</option>
                        <option data-countrycode="TR " value="Turkey">Turkey</option>
                        <option data-countrycode="TM " value="Turkmenistan">Turkmenistan</option>
                        <option data-countrycode="TC " value="Turks and Caicos Islands">Turks and Caicos Islands
                        </option>
                        <option data-countrycode="TV " value="Tuvalu">Tuvalu</option>
                        <option data-countrycode="UG " value="Uganda">Uganda</option>
                        <option data-countrycode="UA " value="Ukraine">Ukraine</option>
                        <option data-countrycode="AE " value="United Arab Emirates">United Arab Emirates</option>
                        <option data-countrycode="GB " value="United Kingdom">United Kingdom</option>
                        <option data-countrycode="US " value="United States">United States</option>
                        <option data-countrycode="UY " value="Uruguay">Uruguay</option>
                        <option data-countrycode="UZ " value="Uzbekistan">Uzbekistan</option>
                        <option data-countrycode="VU " value="Vanuatu">Vanuatu</option>
                        <option data-countrycode="VE " value="Venezuela">Venezuela</option>
                        <option data-countrycode="VN " value="Vietnam">Vietnam</option>
                        <option data-countrycode="WF " value="Wallis and Futuna">Wallis and Futuna</option>
                        <option data-countrycode="EH " value="Western Sahara">Western Sahara</option>
                        <option data-countrycode="YE " value="Yemen">Yemen</option>
                        <option data-countrycode="ZM " value="Zambia">Zambia</option>
                        <option data-countrycode="ZW " value="Zimbabwe">Zimbabwe</option>
                        <option data-countrycode="Others" value="Others">Others</option>
                    </select>
            @endif

            @if(App::getLocale() == 'id')
                <span>Saya adalah warga negara: (*)</span>
            @else
                <span>What is your nationality? (*)</span>
            @endif
            <select name="nationality" id="nationality" required="">
                <option data-countrycode="AF " value="Afghanistan">Afghanistan</option>
                <option data-countrycode="AX " value="Åland Islands">Åland Islands</option>
                <option data-countrycode="AL " value="Albania">Albania</option>
                <option data-countrycode="DZ " value="Algeria">Algeria</option>
                <option data-countrycode="AS " value="American Samoa">American Samoa</option>
                <option data-countrycode="AD " value="Andorra">Andorra</option>
                <option data-countrycode="AO " value="Angola">Angola</option>
                <option data-countrycode="AI " value="Anguilla">Anguilla</option>
                <option data-countrycode="AQ " value="Antarctica">Antarctica</option>
                <option data-countrycode="AG " value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option data-countrycode="AR " value="Argentina">Argentina</option>
                <option data-countrycode="AM " value="Armenia">Armenia</option>
                <option data-countrycode="AW " value="Aruba">Aruba</option>
                <option data-countrycode="AU " value="Australia" selected>Australia</option>
                <option data-countrycode="AT " value="Austria">Austria</option>
                <option data-countrycode="AZ " value="Azerbaijan">Azerbaijan</option>
                <option data-countrycode="BS " value="The Bahamas">The Bahamas</option>
                <option data-countrycode="BH " value="Bahrain">Bahrain</option>
                <option data-countrycode="BD " value="Bangladesh">Bangladesh</option>
                <option data-countrycode="BB " value="Barbados">Barbados</option>
                <option data-countrycode="BY " value="Belarus">Belarus</option>
                <option data-countrycode="BE " value="Belgium">Belgium</option>
                <option data-countrycode="BZ " value="Belize">Belize</option>
                <option data-countrycode="BJ " value="Benin">Benin</option>
                <option data-countrycode="BM " value="Bermuda">Bermuda</option>
                <option data-countrycode="BT " value="Bhutan">Bhutan</option>
                <option data-countrycode="BO " value="Bolivia">Bolivia</option>
                <option data-countrycode="BQ " value="Bonaire">Bonaire</option>
                <option data-countrycode="BA " value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                <option data-countrycode="BW " value="Botswana">Botswana</option>
                <option data-countrycode="BV " value="Bouvet Island">Bouvet Island</option>
                <option data-countrycode="BR " value="Brazil">Brazil</option>
                <option data-countrycode="IO " value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option data-countrycode="UM " value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                <option data-countrycode="VG " value="Virgin Islands (British)">Virgin Islands (British)</option>
                <option data-countrycode="VI " value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                <option data-countrycode="BN " value="Brunei">Brunei</option>
                <option data-countrycode="BG " value="Bulgaria">Bulgaria</option>
                <option data-countrycode="BF " value="Burkina Faso">Burkina Faso</option>
                <option data-countrycode="BI " value="Burundi">Burundi</option>
                <option data-countrycode="KH " value="Cambodia">Cambodia</option>
                <option data-countrycode="CM " value="Cameroon">Cameroon</option>
                <option data-countrycode="CA " value="Canada">Canada</option>
                <option data-countrycode="CV " value="Cape Verde">Cape Verde</option>
                <option data-countrycode="KY " value="Cayman Islands">Cayman Islands</option>
                <option data-countrycode="CF " value="Central African Republic">Central African Republic</option>
                <option data-countrycode="TD " value="Chad">Chad</option>
                <option data-countrycode="CL " value="Chile">Chile</option>
                <option data-countrycode="CN " value="China">China</option>
                <option data-countrycode="CX " value="Christmas Island">Christmas Island</option>
                <option data-countrycode="CC " value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                <option data-countrycode="CO " value="Colombia">Colombia</option>
                <option data-countrycode="KM " value="Comoros">Comoros</option>
                <option data-countrycode="CG " value="Republic of the Congo">Republic of the Congo</option>
                <option data-countrycode="CD " value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                <option data-countrycode="CK " value="Cook Islands">Cook Islands</option>
                <option data-countrycode="CR " value="Costa Rica">Costa Rica</option>
                <option data-countrycode="HR " value="Croatia">Croatia</option>
                <option data-countrycode="CU " value="Cuba">Cuba</option>
                <option data-countrycode="CW " value="Curaçao">Curaçao</option>
                <option data-countrycode="CY " value="Cyprus">Cyprus</option>
                <option data-countrycode="CZ " value="Czech Republic">Czech Republic</option>
                <option data-countrycode="DK " value="Denmark">Denmark</option>
                <option data-countrycode="DJ " value="Djibouti">Djibouti</option>
                <option data-countrycode="DM " value="Dominica">Dominica</option>
                <option data-countrycode="DO " value="Dominican Republic">Dominican Republic</option>
                <option data-countrycode="EC " value="Ecuador">Ecuador</option>
                <option data-countrycode="EG " value="Egypt">Egypt</option>
                <option data-countrycode="SV " value="El Salvador">El Salvador</option>
                <option data-countrycode="GQ " value="Equatorial Guinea">Equatorial Guinea</option>
                <option data-countrycode="ER " value="Eritrea">Eritrea</option>
                <option data-countrycode="EE " value="Estonia">Estonia</option>
                <option data-countrycode="ET " value="Ethiopia">Ethiopia</option>
                <option data-countrycode="FK " value="Falkland Islands">Falkland Islands</option>
                <option data-countrycode="FO " value="Faroe Islands">Faroe Islands</option>
                <option data-countrycode="FJ " value="Fiji">Fiji</option>
                <option data-countrycode="FI " value="Finland">Finland</option>
                <option data-countrycode="FR " value="France">France</option>
                <option data-countrycode="GF " value="French Guiana">French Guiana</option>
                <option data-countrycode="PF " value="French Polynesia">French Polynesia</option>
                <option data-countrycode="TF " value="French Southern and Antarctic Lands">French Southern and Antarctic Lands</option>
                <option data-countrycode="GA " value="Gabon">Gabon</option>
                <option data-countrycode="GM " value="The Gambia">The Gambia</option>
                <option data-countrycode="GE " value="Georgia">Georgia</option>
                <option data-countrycode="DE " value="Germany">Germany</option>
                <option data-countrycode="GH " value="Ghana">Ghana</option>
                <option data-countrycode="GI " value="Gibraltar">Gibraltar</option>
                <option data-countrycode="GR " value="Greece">Greece</option>
                <option data-countrycode="GL " value="Greenland">Greenland</option>
                <option data-countrycode="GD " value="Grenada">Grenada</option>
                <option data-countrycode="GP " value="Guadeloupe">Guadeloupe</option>
                <option data-countrycode="GU " value="Guam">Guam</option>
                <option data-countrycode="GT " value="Guatemala">Guatemala</option>
                <option data-countrycode="GG " value="Guernsey">Guernsey</option>
                <option data-countrycode="GN " value="Guinea">Guinea</option>
                <option data-countrycode="GW " value="Guinea-Bissau">Guinea-Bissau</option>
                <option data-countrycode="GY " value="Guyana">Guyana</option>
                <option data-countrycode="HT " value="Haiti">Haiti</option>
                <option data-countrycode="HM " value="Heard Island and McDonald Islands">Heard Island and
                    McDonald Islands
                </option>
                <option data-countrycode="VA " value="Holy See">Holy See</option>
                <option data-countrycode="HN " value="Honduras">Honduras</option>
                <option data-countrycode="HK " value="Hong Kong">Hong Kong</option>
                <option data-countrycode="HU " value="Hungary">Hungary</option>
                <option data-countrycode="IS " value="Iceland">Iceland</option>
                <option data-countrycode="IN " value="India">India</option>
                <option data-countrycode="ID " value="Indonesia">Indonesia</option>
                <option data-countrycode="CI " value="Ivory Coast">Ivory Coast</option>
                <option data-countrycode="IR " value="Iran">Iran</option>
                <option data-countrycode="IQ " value="Iraq">Iraq</option>
                <option data-countrycode="IE " value="Republic of Ireland">Republic of Ireland</option>
                <option data-countrycode="IM " value="Isle of Man">Isle of Man</option>
                <option data-countrycode="IL " value="Israel">Israel</option>
                <option data-countrycode="IT " value="Italy">Italy</option>
                <option data-countrycode="JM " value="Jamaica">Jamaica</option>
                <option data-countrycode="JP " value="Japan">Japan</option>
                <option data-countrycode="JE " value="Jersey">Jersey</option>
                <option data-countrycode="JO " value="Jordan">Jordan</option>
                <option data-countrycode="KZ " value="Kazakhstan">Kazakhstan</option>
                <option data-countrycode="KE " value="Kenya">Kenya</option>
                <option data-countrycode="KI " value="Kiribati">Kiribati</option>
                <option data-countrycode="KW " value="Kuwait">Kuwait</option>
                <option data-countrycode="KG " value="Kyrgyzstan">Kyrgyzstan</option>
                <option data-countrycode="LA " value="Laos">Laos</option>
                <option data-countrycode="LV " value="Latvia">Latvia</option>
                <option data-countrycode="LB " value="Lebanon">Lebanon</option>
                <option data-countrycode="LS " value="Lesotho">Lesotho</option>
                <option data-countrycode="LR " value="Liberia">Liberia</option>
                <option data-countrycode="LY " value="Libya">Libya</option>
                <option data-countrycode="LI " value="Liechtenstein">Liechtenstein</option>
                <option data-countrycode="LT " value="Lithuania">Lithuania</option>
                <option data-countrycode="LU " value="Luxembourg">Luxembourg</option>
                <option data-countrycode="MO " value="Macau">Macau</option>
                <option data-countrycode="MK " value="Republic of Macedonia">Republic of Macedonia</option>
                <option data-countrycode="MG " value="Madagascar">Madagascar</option>
                <option data-countrycode="MW " value="Malawi">Malawi</option>
                <option data-countrycode="MY " value="Malaysia">Malaysia</option>
                <option data-countrycode="MV " value="Maldives">Maldives</option>
                <option data-countrycode="ML " value="Mali">Mali</option>
                <option data-countrycode="MT " value="Malta">Malta</option>
                <option data-countrycode="MH " value="Marshall Islands">Marshall Islands</option>
                <option data-countrycode="MQ " value="Martinique">Martinique</option>
                <option data-countrycode="MR " value="Mauritania">Mauritania</option>
                <option data-countrycode="MU " value="Mauritius">Mauritius</option>
                <option data-countrycode="YT " value="Mayotte">Mayotte</option>
                <option data-countrycode="MX " value="Mexico">Mexico</option>
                <option data-countrycode="FM " value="Federated States of Micronesia">Federated States of
                    Micronesia
                </option>
                <option data-countrycode="MD " value="Moldova">Moldova</option>
                <option data-countrycode="MC " value="Monaco">Monaco</option>
                <option data-countrycode="MN " value="Mongolia">Mongolia</option>
                <option data-countrycode="ME " value="Montenegro">Montenegro</option>
                <option data-countrycode="MS " value="Montserrat">Montserrat</option>
                <option data-countrycode="MA " value="Morocco">Morocco</option>
                <option data-countrycode="MZ " value="Mozambique">Mozambique</option>
                <option data-countrycode="MM " value="Myanmar">Myanmar</option>
                <option data-countrycode="NA " value="Namibia">Namibia</option>
                <option data-countrycode="NR " value="Nauru">Nauru</option>
                <option data-countrycode="NP " value="Nepal">Nepal</option>
                <option data-countrycode="NL " value="Netherlands">Netherlands</option>
                <option data-countrycode="NC " value="New Caledonia">New Caledonia</option>
                <option data-countrycode="NZ " value="New Zealand">New Zealand</option>
                <option data-countrycode="NI " value="Nicaragua">Nicaragua</option>
                <option data-countrycode="NE " value="Niger">Niger</option>
                <option data-countrycode="NG " value="Nigeria">Nigeria</option>
                <option data-countrycode="NU " value="Niue">Niue</option>
                <option data-countrycode="NF " value="Norfolk Island">Norfolk Island</option>
                <option data-countrycode="KP " value="North Korea">North Korea</option>
                <option data-countrycode="MP " value="Northern Mariana Islands">Northern Mariana Islands
                </option>
                <option data-countrycode="NO " value="Norway">Norway</option>
                <option data-countrycode="OM " value="Oman">Oman</option>
                <option data-countrycode="PK " value="Pakistan">Pakistan</option>
                <option data-countrycode="PW " value="Palau">Palau</option>
                <option data-countrycode="PS " value="Palestine">Palestine</option>
                <option data-countrycode="PA " value="Panama">Panama</option>
                <option data-countrycode="PG " value="Papua New Guinea">Papua New Guinea</option>
                <option data-countrycode="PY " value="Paraguay">Paraguay</option>
                <option data-countrycode="PE " value="Peru">Peru</option>
                <option data-countrycode="PH " value="Philippines" selected>Philippines</option>
                <option data-countrycode="PN " value="Pitcairn Islands">Pitcairn Islands</option>
                <option data-countrycode="PL " value="Poland">Poland</option>
                <option data-countrycode="PT " value="Portugal">Portugal</option>
                <option data-countrycode="PR " value="Puerto Rico">Puerto Rico</option>
                <option data-countrycode="QA " value="Qatar">Qatar</option>
                <option data-countrycode="XK " value="Republic of Kosovo">Republic of Kosovo</option>
                <option data-countrycode="RE " value="Réunion">Réunion</option>
                <option data-countrycode="RO " value="Romania">Romania</option>
                <option data-countrycode="RU " value="Russia">Russia</option>
                <option data-countrycode="RW " value="Rwanda">Rwanda</option>
                <option data-countrycode="BL " value="Saint Barthélemy">Saint Barthélemy</option>
                <option data-countrycode="SH " value="Saint Helena">Saint Helena</option>
                <option data-countrycode="KN " value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option data-countrycode="LC " value="Saint Lucia">Saint Lucia</option>
                <option data-countrycode="MF " value="Saint Martin">Saint Martin</option>
                <option data-countrycode="PM " value="Saint Pierre and Miquelon">Saint Pierre and Miquelon
                </option>
                <option data-countrycode="VC " value="Saint Vincent and the Grenadines">Saint Vincent and the
                    Grenadines
                </option>
                <option data-countrycode="WS " value="Samoa">Samoa</option>
                <option data-countrycode="SM " value="San Marino">San Marino</option>
                <option data-countrycode="ST " value="São Tomé and Príncipe">São Tomé and Príncipe</option>
                <option data-countrycode="SA " value="Saudi Arabia">Saudi Arabia</option>
                <option data-countrycode="SN " value="Senegal">Senegal</option>
                <option data-countrycode="RS " value="Serbia">Serbia</option>
                <option data-countrycode="SC " value="Seychelles">Seychelles</option>
                <option data-countrycode="SL " value="Sierra Leone">Sierra Leone</option>
                <option data-countrycode="SG " value="Singapore">Singapore</option>
                <option data-countrycode="SX " value="Sint Maarten">Sint Maarten</option>
                <option data-countrycode="SK " value="Slovakia">Slovakia</option>
                <option data-countrycode="SI " value="Slovenia">Slovenia</option>
                <option data-countrycode="SB " value="Solomon Islands">Solomon Islands</option>
                <option data-countrycode="SO " value="Somalia">Somalia</option>
                <option data-countrycode="ZA " value="South Africa">South Africa</option>
                <option data-countrycode="GS " value="South Georgia">South Georgia</option>
                <option data-countrycode="KR " value="South Korea">South Korea</option>
                <option data-countrycode="SS " value="South Sudan">South Sudan</option>
                <option data-countrycode="ES " value="Spain">Spain</option>
                <option data-countrycode="LK " value="Sri Lanka">Sri Lanka</option>
                <option data-countrycode="SD " value="Sudan">Sudan</option>
                <option data-countrycode="SR " value="Suriname">Suriname</option>
                <option data-countrycode="SJ " value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                <option data-countrycode="SZ " value="Swaziland">Swaziland</option>
                <option data-countrycode="SE " value="Sweden">Sweden</option>
                <option data-countrycode="CH " value="Switzerland">Switzerland</option>
                <option data-countrycode="SY " value="Syria">Syria</option>
                <option data-countrycode="TW " value="Taiwan">Taiwan</option>
                <option data-countrycode="TJ " value="Tajikistan">Tajikistan</option>
                <option data-countrycode="TZ " value="Tanzania">Tanzania</option>
                <option data-countrycode="TH " value="Thailand">Thailand</option>
                <option data-countrycode="TL " value="East Timor">East Timor</option>
                <option data-countrycode="TG " value="Togo">Togo</option>
                <option data-countrycode="TK " value="Tokelau">Tokelau</option>
                <option data-countrycode="TO " value="Tonga">Tonga</option>
                <option data-countrycode="TT " value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option data-countrycode="TN " value="Tunisia">Tunisia</option>
                <option data-countrycode="TR " value="Turkey">Turkey</option>
                <option data-countrycode="TM " value="Turkmenistan">Turkmenistan</option>
                <option data-countrycode="TC " value="Turks and Caicos Islands">Turks and Caicos Islands
                </option>
                <option data-countrycode="TV " value="Tuvalu">Tuvalu</option>
                <option data-countrycode="UG " value="Uganda">Uganda</option>
                <option data-countrycode="UA " value="Ukraine">Ukraine</option>
                <option data-countrycode="AE " value="United Arab Emirates">United Arab Emirates</option>
                <option data-countrycode="GB " value="United Kingdom">United Kingdom</option>
                <option data-countrycode="US " value="United States">United States</option>
                <option data-countrycode="UY " value="Uruguay">Uruguay</option>
                <option data-countrycode="UZ " value="Uzbekistan">Uzbekistan</option>
                <option data-countrycode="VU " value="Vanuatu">Vanuatu</option>
                <option data-countrycode="VE " value="Venezuela">Venezuela</option>
                <option data-countrycode="VN " value="Vietnam">Vietnam</option>
                <option data-countrycode="WF " value="Wallis and Futuna">Wallis and Futuna</option>
                <option data-countrycode="EH " value="Western Sahara">Western Sahara</option>
                <option data-countrycode="YE " value="Yemen">Yemen</option>
                <option data-countrycode="ZM " value="Zambia">Zambia</option>
                <option data-countrycode="ZW " value="Zimbabwe">Zimbabwe</option>
            </select>

            <div class="form-group text-center">
                @if(App::getLocale() == 'id')
                    <a class="btnn" id="step_prelim_question">LANJUT</a>
                @else
                    <a class="btnn" id="step_prelim_question">NEXT</a>
                @endif
            </div>



        </div>
    </div>


    <!-- Survey 01 -->
    <div class="container question" id="survey03">
        <div class="max">

            @if(App::getLocale() == 'id')
                <span>Apa pendidikan terakhirmu ? (*)</span>
                <select name="highest_academy" required>
                    <option value="highschool">Sekolah Menengah Atas</option>
                    <option value="college">Masih Kuliah</option>
                    <option value="diploma">Diploma</option>
                    <option value="bachelor">Program Sarjana</option>
                    <option value="master">Program Master</option>
                </select>
            @else
                <span>Your Highest Academic Qualification? (*)</span>
                <select name="highest_academy" required>
                    <option value="highschool">High School</option>
                    <option value="college">College</option>
                    <option value="diploma">Diploma</option>
                    <option value="bachelor">Bachelor</option>
                    <option value="master">Master</option>
                </select>
            @endif

            @if(App::getLocale() == 'id')
                <span>Kapan kamu lulus ? (*)</span>
                <select name="when_graduate" required>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="before_2015">Sebelum 2015</option>
                </select>
            @else
                <span>When did you graduate? (*)</span>
                <select name="when_graduate" required>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="before_2015">Before 2015</option>
                </select>
            @endif

            <div class="form-group text-center">
                @if(App::getLocale() == 'id')
                    <a class="btnn" id="Q3A1" data-value="" data-score="3">LANJUT</a>
                @else
                    <a class="btnn" id="Q3A1" data-value="" data-score="3">NEXT</a>
                @endif
            </div>

        </div>
    </div>

    <!-- Survey 03 -->
    <div class="container question" id="survey05">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2><b>Silahkan masukkan detail di bawah ini dan kamu akan mendapatkan universitas sesuai dengan kebutuhan mu.</b></h2>
            @else
                <h2><b>Alright! Just enter your details and we’ll find you the university that perfectly suits your preferences.</b></h2>
            @endif

            @if(App::getLocale() == 'id')
                <span>Apa bidang yang paling kamu minati?</span>
            @else
                <span>Which area of study are you most interested in?</span>
            @endif
            <p style="background:red;color:white;" class="hidden course_survey03">Please choose one of these options</p>
            <select name="course_university_survey03" id="course_university_survey03">
                <option class="grey" disabled="disabled" selected="selected">Select your option</option>
                <option value="Agriculture and Veterinary Medicine">Agriculture and Veterinary Medicine</option>
                <option value="Applied and Pure Sciences">Applied and Pure Sciences</option>
                <option value="Architecture and Construction">Architecture and Construction</option>
                <option value="Business and Management">Business and Management</option>
                <option value="Computer Science and IT">Computer Science and IT</option>
                <option value="Creative Arts and Design">Creative Arts and Design</option>
                <option value="Education and Training">Education and Training</option>
                <option value="Engineering">Engineering</option>
                <option value="Health and Medicine">Health and Medicine</option>
                <option value="Humanities">Humanities</option>
                <option value="Law">Law</option>
                <option value="MBA">MBA</option>
                <option value="Personal Care and Fitness">Personal Care and Fitness</option>
                <option value="Social Studies and Media">Social Studies and Media</option>
                <option value="Travel and Hospitality">Travel and Hospitality</option>
            </select>

            @if(App::getLocale() =='id')
                <span>Kemana kamu berencana melanjutkan pendidikan?</span>
                <p style="background:red;color:white;" class="hidden destination_survey03">Pilih salah satu di bawah ini</p>
            @else
                <span>Where is your chosen study destination?</span>
                <p style="background:red;color:white;" class="hidden destination_survey03">Please choose one of these options</p>
            @endif
            <select name="destination_university_survey03" id="destination_university_survey03" class="bs-select form-control" data-live-search="true">
                <option value="Australia" data-score="1">Australia</option>
                <option value="New Zealand" data-score="1">New Zealand</option>
                <option value="Canada" data-score="1">Canada</option>
            </select>

            @if(App::getLocale() == 'id')
                <span>Kapan kamu berencana mau mendaftarkan diri ke Universitas di luar negeri?</span>
                <p style="background:red;color:white;" class="hidden time_survey03">Pilih salah satu di bawah ini</p>

                <div class="small time_apply_survey03">
                    <a class="select" data-value="Within 6 months" data-score="3">Dalam 6 bulan</a>
                    <a class="select" data-value="Within a year" data-score="2">Satu tahun lagi</a>
                    <a class="select" data-value="Within 2 years" data-score="1">Lebih dari 2 tahun lagi</a>
                    <a class="select" data-value="Unsure" data-score="0">Belum pasti</a>
                </div>

                <span>Jenjang pendidikan yang kamu mau ambil?</span>
                <p style="background:red;color:white;" class="hidden time_survey03">Please fill this level of study.</p>
                <div class="small level_study">
                    <a class="select" data-value="english_language" >Pendidikan Bahasa Inggris</a>
                    <a class="select" data-value="undergraduate">Sarjana</a>
                    <a class="select" data-value="postgraduate">Pasca Sarjana</a>
                </div>

                <a class="btnn" id="Q5A1">LANJUT</a>
            @else
                <span>When are you planning to apply?</span>
                <p style="background:red;color:white;" class="hidden time_survey03">Please choose one of these options</p>

                <div class="small time_apply_survey03">
                    <a class="select" data-value="Within 6 months" data-score="3">Within 6 Months</a>
                    <a class="select" data-value="Within a year" data-score="2">Within a year</a>
                    <a class="select" data-value="Within 2 years" data-score="1">Longer within 2 years</a>
                    <a class="select" data-value="Unsure" data-score="0">Unsure</a>
                </div>

                <span>In what Level of study?</span>
                <p style="background:red;color:white;" class="hidden time_survey03">Please fill this level of study.</p>
                <div class="small level_study">
                    <a class="select" data-value="english_language" >English Language</a>
                    <a class="select" data-value="undergraduate" >Undergraduate</a>
                    <a class="select" data-value="postgraduate" >Postgraduate</a>
                </div>

                <a class="btnn" id="Q5A1">SUBMIT</a>
            @endif

        </div>
    </div>

    <!-- Survey 07 -->
    <div class="container question" id="survey09">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2><b>Kemana Liburan terakhirmu?</b></h2>
                <p style="background:red;color:white;" class="hidden course_survey03">Tolong isi di bawah ini</p>
                <select name="where_last_holiday" id="where_last_holiday">
                    <option value="domestically">Domestik</option>
                    <option value="asia">Asia</option>
                    <option value="china">China</option>
                    <option value="europe">Europe</option>
                    <option value="australia">Australia</option>
                    <option value="usa">USA</option>
                    <option value="others">Lainnya</option>
                </select>
                <h2><b>Adakah keluarga dekatmu yang sedang belajar atau bekerja di luar negeri?</b></h2>
                <div class="small family_overseas">
                    <a class="select" data-value="yes" >YA</a>
                    <a class="select" data-value="no" >TIDAK</a>
                </div>
                <h2><b>Sebaiknya kapan kami menghubungimu?</b></h2>
                <div class="small prefer_time_called">
                    <a class="select" data-value="morning">Pagi</a>
                    <a class="select" data-value="afternoon">Sore</a>
                </div>
                <h2><b>Apakah Kamu pernah datang ke Education Expo dalam 1 tahun ini?</b></h2>
                {{--<p style="background:red;color:white;" class="hidden come_to_expo_err">Pilih salah satu di bawah ini</p>--}}
                <div class="small come_to_expo">
                    <a class="select expo_selection_y" data-value="yes">YA</a>
                    <a class="select expo_selection_n" data-value="no">TIDAK</a>
                </div>
                <div>
                    <p style="background:red;color:white;" class="hidden expo_name_err">Mohon isi nama Education Expo yang dikunjungi</p>
                    <input type="text" class="form-control expo_name" placeholder="Nama Education Expo?" style="display: none;" required>
                </div>
                <h2><b>Apakah kamu mampu belajar di luar negeri tanpa beasiswa?</b></h2>
                <div class="small scholarship">
                    <a class="select" data-value="yes">YA</a>
                    <a class="select" data-value="no">TIDAK</a>
                </div>
                <div class="small yn">
                    <a class="btnn" id="Q9A1">LANJUT</a>
                </div>
            @else
                <h2><b>Where did you go on your last holiday?</b></h2>
                <p style="background:red;color:white;" class="hidden course_survey03">Please fill your Last Holiday</p>
                <select name="where_last_holiday" id="where_last_holiday">
                    <option value="domestically">Domestically</option>
                    <option value="asia">Asia</option>
                    <option value="china">China</option>
                    <option value="europe">Europe</option>
                    <option value="australia">Australia</option>
                    <option value="usa">USA</option>
                    <option value="others">Others</option>
                </select>
                <h2><b>Do you have close family that are studying or working overseas?</b></h2>
                <div class="small family_overseas">
                    <a class="select" data-value="yes" >YES</a>
                    <a class="select" data-value="no" >NO</a>
                </div>
                <h2><b>What time do you prefer to be called?</b></h2>
                <div class="small prefer_time_called">
                    <a class="select" data-value="morning">MORNING</a>
                    <a class="select" data-value="afternoon">AFTERNOON</a>
                </div>
                <h2><b>Do you ever come to Education Expo in this past 1 year?</b></h2>
                {{--<p style="background:red;color:white;" class="hidden come_to_expo_err">Please choose one of these options</p>--}}
                <div class="small come_to_expo">
                    <a class="select expo_selection_y" data-value="yes">YES</a>
                    <a class="select expo_selection_n" data-value="no">NO</a>
                </div>
                <div>
                    <p style="background:red;color:white;" class="hidden expo_name_err">Please fill the name of Education Expo</p>
                    <input type="text" class="form-control expo_name" placeholder="Name of the Education Expo?" style="display: none;" required>
                </div>
                <h2><b>Are you able to study abroad without scholarship?</b></h2>
                <div class="small scholarship">
                    <a class="select" data-value="yes">YES</a>
                    <a class="select" data-value="no">NO</a>
                </div>
                <div class="small yn">
                    <a class="btnn" id="Q9A1">SUBMIT</a>
                </div>
            @endif
        </div>
    </div>

    <!-- Survey TAKEN IELTS / TOEFL TEST -->
    <div class="container question" id="survey10">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2><b>Apakah kamu pernah mengambil ujian TOEFL / IELTS?</b></h2>
                <div class="small yn">
                    <a class="select" id="Q10A1" data-score="1">PERNAH</a>
                    <a class="select" id="Q10A2" data-score="0">TIDAK PERNAH</a>
                </div>
            @else
                <h2><b>Have you taken Academic IELTS/TOEFL test?</b></h2>
                <div class="small yn">
                    <a class="select" id="Q10A1" data-score="1">YES</a>
                    <a class="select" id="Q10A2" data-score="0">NO</a>
                </div>
            @endif
        </div>
    </div>

    <div class="container question" id="survey12">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2><b>Pilih salah satu tes bahasa inggris yang pernah kamu ambil : </b></h2>
                <p style="background:red;color:white;" class="hidden toefl_or_ielts">Pilih salah satu di bawah ini</p>
                <div class="small toefl_or_ielts">
                    <a class="select" data-value="IELTS" data-score="0" id="Q12A1">IELTS</a>
                    <a class="select" data-value="TOEFL" data-score="0" id="Q12A2">TOEFL</a>
                </div>
            @else
                <h2><b>Choose one from below test that you ever take : </b></h2>
                <p style="background:red;color:white;" class="hidden toefl_or_ielts">Please Choose One of Below</p>
                <div class="small toefl_or_ielts">
                    <a class="select" data-value="IELTS" data-score="0" id="Q12A1">IELTS</a>
                    <a class="select" data-value="TOEFL" data-score="0" id="Q12A2">TOEFL</a>
                </div>
            @endif
        </div>
    </div>

    <!-- Survey 08 Time take TOEFL-->
    <div class="container question" id="survey14">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2><b>Kapan Anda mengambil ujian nya?</b></h2>
                <p style="background:red;color:white;" class="hidden toefl_ielts_time">Pilih salah satu di bawah</p>

                <div class="small time_apply_toefl">
                    <a class="select" data-value="2018" data-score="2">2018</a>
                    <a class="select" data-value="2017" data-score="2">2017</a>
                    <a class="select" data-value="2016" data-score="2">2016</a>
                    <a class="select" data-value="2015" data-score="1">2015</a>
                    <a class="select" data-value="before_2015"  data-score="0">Sebelum 2015</a>
                </div>
                <h2><b>Berapa nilainya?</b></h2>
                <p>Kosongkan jika kamu lupa</p>

                <input type="text" placeholder="Masukkan nilai disini..." class="toefl_ielts_score form-control"
                       style="cursor: pointer;background: none;border: none;border-bottom: 1px solid #3d3d3d;display: block;width: 100%;margin-bottom: 30px;font-size: 20px;";
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                />
                <a class="btnn" id="Q14A1">Lanjut</a>
            @else
                <h2><b>When did you take your test?</b></h2>
                <p style="background:red;color:white;" class="hidden toefl_ielts_time">Please Choose One of Below</p>

                <div class="small time_apply_toefl">
                    <a class="select" data-value="2018" data-score="2">2018</a>
                    <a class="select" data-value="2017" data-score="2">2017</a>
                    <a class="select" data-value="2016" data-score="2">2016</a>
                    <a class="select" data-value="2015" data-score="1">2015</a>
                    <a class="select" data-value="before_2015"  data-score="0">Before 2015</a>
                </div>
                <h2><b>Please input your test score :</b></h2>
                <p>Just leave it blank if you've forgotten your score</p>
                <input type="number" placeholder="Your score here..." class="toefl_ielts_score form-control" max=999 step=0.1
                       style="cursor: pointer;background: none;border: none;border-bottom: 1px solid #3d3d3d;display: block;width: 100%;margin-bottom: 30px;padding: 10px 5px;font-size: 20px;";
                />
                <a class="btnn" id="Q14A1">SUBMIT</a>
            @endif

        </div>
    </div>

    <!-- Survey ANYTHING TO ADD -->
    <div class="container question" id="survey11">
        <div class="max">
            @if(App::getLocale() == 'id')
                <h2><b>Apakah ada informasi lain yang mau kamu tambahkan?</b></h2>
                <p style="background:red;color:white;" class="hidden additional_info_error">Mohon isi kolom di bawah terlebih dahulu</p>
                <textarea name="anything_to_add" id="anything_to_add" class="form-control" rows="10" required style="resize: none;"></textarea>
                <a class="btnn" id="Q11A1" style="margin-top:10px;">SELESAI</a>
            @else
                <h2><b>Anything else you want to add?</b></h2>
                <p style="background:red;color:white;" class="hidden additional_info_error">Please fill in the fields below first</p>
                <textarea name="anything_to_add" id="anything_to_add" class="form-control" rows="10" required style="resize: none;"></textarea>
                <a class="btnn" id="Q11A1" style="margin-top:10px;">SUBMIT</a>
            @endif
        </div>
    </div>

    {{ csrf_field() }}
    <input type="hidden" name="id_user_candidate" id="id_user_candidate" value="{{$lead_info->id}}"/>

</div>

<!-- Load JS here for greater good =============================-->
<script src="{{ asset('ext/sqlp/js/jquery.js') }}"></script>

<script>
    // @if(isset($source))
    // var source = '{{$source}}';
    // @else
    // var source = null;
    // @endif

    @if(isset($lang))
    var lang = '{{$lang}}';
            @else
    var lang = null;
    @endif
</script>

<script src="{{ asset('ext/sqlp/custom/survey.js') }}"></script>
@if(false)
    <script>
        $.get(
            'https://restcountries.eu/rest/v1/all',
            {
            },
            function (data) {
                $.each(data, function (key, element) {
                    $('#nationality').append('<option data-countrycode="'+element.alpha2Code+' "value="' + element.name + '">' + element.name + '</option>');
                });
            }
        );
    </script>
@endif
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-83022576-9', 'auto');
    ga('send', 'pageview');
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-596f299c41f5ca57"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
</body>
</html>
