<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>StudyQuery</title>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
  <link href="{{asset('ext/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{asset('ext/css/normalize.css')}}"> <!-- CSS reset -->
  <link rel="stylesheet" href="{{asset('ext/css/bootstrap.min.css')}}"> <!-- Bootstrap Grid -->
  <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
  <link rel="stylesheet" href="{{asset('ext/css/animate.min.css')}}"><!-- Animate -->
	<link rel="stylesheet" href="{{asset('ext/css/style-quick.css')}}"> <!-- Resource style -->
  <link rel="stylesheet" href="{{asset('ext/css/magnific-popup.css')}}"> <!-- Resource style -->
  <!-- Font GFontMont -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>

  <header id="main-nav">
    <div class="container">
    <div class="logo hidden"><a href="www.studyquery.co.id" target="_blank"><img src="images/logo-sq-green.png" alt=""></a></div>
    </div>
  </header><!-- //Main Nav -->

  <section id="header">
    <div class="container">
      <div class="row">
        <div class="col-md-6 feature-item">
          <a href="www.studyquery.co.id" target="_blank"><img src="{{asset('ext/images/logo-sq-green.png')}}" alt="" style="position: absolute;margin-top:-80px;"></a>
          <h2 class="wow fadeInUp" style="color:#038FA2;font-weight:bolder;margin-bottom:20px;margin-top: 100px;">Smart Choice for Smart Students</h2>
          <p class="wow fadeInUp" data-wow-delay=".2s" style="color:#333">Please fill the form to start your initial assesment.</p>

          <div class="social-media">
            <div class="ig-media">
              <a href="https://www.instagram.com/sqtotherescue/" target="_blank"><img src="{{asset('ext/sqlp/images/quick/instagram-sq.png')}}"></a>
            </div>
            <div class="fb-media">
              <a href="https://www.facebook.com/studyqueryid" target="_blank"><img src="{{asset('ext/sqlp/images/quick/facebook-sq.png')}}"></a>
            </div>
            <div class="fb-media">
              <a href="https://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=studyquery" target="_blank"><img src="{{asset('ext/sqlp/images/quick/twitter-sq.png')}}"></a>
            </div>
          </div>
          <p class="wow fadeInUp" data-wow-delay=".2s" style="color:#333; font-size: 16px;font-weight:bold;">Follow us on social media!</p>
          <!-- <a href="https://vimeo.com/74220509" class="btn btn-lg video-btn wow fadeInUp lightbox mfp-iframe" data-wow-delay=".3s" data-effect="mfp-zoom-in"><i class="fa fa-play"></i> Home</a> -->
        </div>
        <div class="col-md-6 feature-item">
          <form style="padding:10px 30px 30px 30px;background-color: #fff;border: 10px solid #038FA2;position: relative;">
            <h4 style="color:#038FA2;font-weight:bolder;margin-bottom:20px;">Consult with Experts</h4>
            <img src="{{asset('ext/sqlp/images/quick/clip.png')}}" style="position: absolute;top:-62px;left:200px;">
            <div class="form-group">
              <label for="email">Name:</label>
              <input type="text" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="email">Email address:</label>
              <input type="email" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="email">Mobile number:</label>
              <input type="text" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="email">Nationality:</label>
              <input type="text" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="email">Current location:</label>
              <input type="text" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="email">Course preference:</label>
              <input type="text" class="form-control" id="email">
            </div>
            <!-- <div class="checkbox">
              <label><input type="checkbox"> Remember me</label>
            </div> -->
            <button type="submit" class="btn btn-default btn-lp">Submit</button>
          </form>
          <p style="font-size: 14px; font-style: italic;padding-top: 10px;">
We take your privacy seriously. We will not sell, share or distribute your contact to any third party.</p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
      <div class="col-lg-12 col-md-12 text-center">
          <p style="font-size: 16px; margin-top: 100px;">© Study Query 2017. All rights reserved.</p>
      </div>
      </div>
    </div>
  </section><!-- //Header -->

  <script src="js/jquery-2.1.4.min.js"></script> <!-- jQuery -->
  <script src="js/bootstrap.min.js"></script>  <!-- Bootstrap -->
  <script src="js/wow.min.js"></script>  <!-- wow -->
  <script src="js/jquery.magnific-popup.min.js"></script>  <!-- wow -->
  <!-- <script src="js/main.js"></script> -->  <!-- Main Script -->
</body>
</html>
