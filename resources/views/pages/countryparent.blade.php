@extends('layout.frontend')

@section('body')
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
				@if(App::getLocale() == 'id')
                <a class="navbar-brand page-scroll" href="{{url('parent')}}">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
				@else
				<a class="navbar-brand page-scroll" href="{{url('parent')}}?lang=en">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
				@endif
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    @if(App::getLocale() == 'id')
						<li>
							<a class="page-scroll" href="{{url('parent/aboutus')}}" style="text-transform: capitalize;">Tentang StudyQuery</a>
						</li>
						<li>
							<a class="page-scroll" href="{{url('parent/country')}}" style="text-transform: capitalize;">Mengapa Australia</a>
						</li>
						<li>
							<a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Ikuti Sosial Media</a>
						</li>
					@else
						<li>
							<a class="page-scroll" href="{{url('parent/aboutus')}}?lang=en" style="text-transform: capitalize;">About StudyQuery</a>
						</li>
						<li>
							<a class="page-scroll" href="{{url('parent/country')}}?lang=en" style="text-transform: capitalize;">Why Australia</a>
						</li>
						<li>
							<a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Follow Social Media</a>
						</li>
					@endif
                    <li>
                        <div class="form-group">
                          <select class="form-control" id="sel1" name="change_language">
							@if(App::getLocale() == 'id')
								<option value="id" selected>Bahasa Indonesia</option>
								<option value="en">English</option>
							@else
								<option value="id">Bahasa Indonesia</option>
								<option value="en" selected>English</option>
							@endif
                          </select>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro" style="height: 50vh; background:url({{asset('ext/images/sq-id/country-aus.jpg')}}) no-repeat center center scroll;background-size: cover;">
        <div class="intro-body">
            <div class="container" style="margin-top: 100px;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-left: 0px;">
                        <h1 class="about-heading" style="font-size: 28px;">Australia</h1>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <a href="#about" class="btn btn-circle page-scroll scroll-down-icon">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div> -->
                <!-- <hr style="float:left;width:400px;"> -->
                <!-- <div class="divider-wrap"><div style="margin-top: 10px; height: 1px; margin-bottom: 10px;" data-width="100%" data-animate="" data-animation-delay="" data-color="default" class="divider-border"></div></div> -->
            </div>
        </div>
    </header>

    <!-- New Design About Section Tabs -->
    <div class="container" style="margin-top: 50px;margin-bottom: 50px;">
    <div class="row">
        <div class="col-sm-3">
             @if(App::getLocale() == 'id')
            <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well">
              <li class="active"><a href="#vtab1" data-toggle="tab">Tentang</a></li>
              <li><a href="#vtab2" data-toggle="tab">Akomodasi</a></li>
              <li><a href="#vtab3" data-toggle="tab">Petualangan</a></li>
              <li><a href="#vtab4" data-toggle="tab">Kampus</a></li>
              <li><a href="#vtab5" data-toggle="tab">Luar Kampus</a></li>
            </ul>
            @else
            <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well">
              <li class="active"><a href="#vtab1" data-toggle="tab">About</a></li>
              <li><a href="#vtab2" data-toggle="tab">Accommodation</a></li>
              <li><a href="#vtab3" data-toggle="tab">Adventures</a></li>
              <li><a href="#vtab4" data-toggle="tab">Campus</a></li>
              <li><a href="#vtab5" data-toggle="tab">Outside Campus</a></li>
            </ul>
            @endif
        </div>
        <div class="col-sm-9">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="vtab1">
                <div class="col-sm-7">
                    @if(App::getLocale() == 'id')
                    <p>Australia adalah negara dan kontinen yang dikelilingi oleh lautan India dan Pasifik. Ibu kotanya, Canberra. Dengan kota besar - Sydney, Brisbane, Melbourne, Perth, Adelaide - adalah pesisir. Negara ini dikenal dengan Sydney Opera House, Great Barrier Reef, padang gurun padang pasir pedalaman yang luas yang disebut Outback, dan spesies hewan unik seperti kanguru dan duck-billed platypuses.
                    Ibu Kota: Canberra<br/>
                    Bahasa: Inggris<br/>
                    Currency: AUD<br/>
                    Luas wilayah: 7,692,024 km2<br/>
                    Populasi: 23.78 juta<br/>
                    Kode panggilan/ Mobile: +61/ +61 4<br/>
                    Colokan listrik:<br/>
                    Pasokan listrik: 220-240 volt<br/>
                    Colokan listrik: <img src="{{asset('ext/images/sq-id/power-plug.png')}}" width="30px"></p>
                    @else
                    <p>Australia is a country and continent surrounded by the Indian and Pacific oceans. Its major cities – Sydney, Brisbane, Melbourne, Perth, Adelaide – are coastal. Its capital, Canberra, is inland. The country is known for its Sydney Opera House, the Great Barrier Reef, a vast interior desert wilderness called the Outback, and unique animal species like kangaroos and duck-billed platypuses.
                    Capital: Canberra<br/>
                    Language: Inggris<br/>
                    Currency: AUD<br/>
                    Total Area: 7,692,024 km2<br/>
                    Population: 23.78 mill<br />
                    Dialing code/ Mobile: +61/ +61 4<br/>
                    Electricity Supply: 220-240 volt<br/>
                    Power Plug: <img src="{{asset('ext/images/sq-id/power-plug.png')}}" width="30px"></p>
                    @endif
                </div>
                <div class="col-sm-5">
                    <section id="cd-google-map">
                        <div id="google-container"></div>
                        <div id="cd-zoom-in"></div>
                        <div id="cd-zoom-out"></div>
                        <address>Sydney, Australia</address>
                    </section>
                </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="vtab2">
                    @if(App::getLocale() == 'id')
                    <p>Siswa internasional di Australia memiliki berbagai pilihan akomodasi. Namun, para siswa perlu tahu bahwa perumahan di universitas-universitas Australia sangat berbeda dari apa yang akan digunakan banyak siswa internasional, karena universitas-universitas di Australia hanya menyediakan sedikit atau tidak ada sama sekali. Kebanyakan siswa di universitas Australia tinggal di luar kampus, baik melalui program homestay, hostel, atau properti sewaan. Biaya akan bervariasi tergantung pada kota dan jenis akomodasi Anda.</p>
                    @else
                    <p>International students in Australia have a large range of accommodation options. However, students need to be aware that housing at Australian universities is very different from what many international students will be used to, because Australian universities provide little or no university housing. It is not nonexistent, but most students at Australian universities live off-campus, whether through homestay programs, hostels, or rental properties. The costs will vary depending on your chosen state, city, and type of accommodation.</p>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="vtab3">
                    @if(App::getLocale() == 'id')
                    <p>Pilihan akomodasi jangka pendek yang mungkin ingin Anda pertimbangkan saat pertama kali tiba di Australia antara lain; Hotel atau hostel. Perumahan sementara yang mungkin ditawarkan melalui institusi anda saat awal memulai pendidikan. Bicaralah dengan internasional support staff di institusi anda atau periksa situs web mereka untuk mendapatkan rinciannya.</p>
                    @else
                    <p>Short-term accommodation options you might want to consider when you first arrive in Australia include; hotel or hostel. Temporary housing which may be offered through your institution while you get settled. Talk to your institution's international support staff or check their website for details.</p>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="vtab4">
                    @if(App::getLocale() == 'id')
                    <p>Kehidupan kampus bisa menjadi pilihan terbaik untuk meminimalkan perjalanan. Meskipun sebagian besar siswa Australia tinggal di luar kampus, beberapa universitas di Australia menyediakan perumahan bagi siswa mereka melalui residential colleges, halls of residence, atau apartemen. Harga dan ketersediaan akomodasi bervariasi sesuai dengan universitas, jadi hubungi institusi anda secara langsung untuk mengetahui pilihan akomodasi yang tersedia dan bandingkan biaya bila mengatur akomodasi anda sendiri, karena pilihan akomodasi ini terbatas dan sangat populer.</p>
                    @else
                    <p>Campus living can be a great option to minimize travel. Although the majority of Australian students live off-campus, some Australian universities do provide housing for their students through residential colleges, halls of residence, or apartments. Prices and availability of accommodations vary according to the university, so contact your institution directly to find out the accommodation options they have available and how the costs compare with organizing your own accommodation, as these accommodation options are limited and very popular.</p>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="vtab5">
                    @if(App::getLocale() == 'id')
                    <p>Banyak siswa di Australia memilih untuk berbagi biaya sewa properti dengan beberapa teman serumah. Siswa yang memilih jenis perumahan ini dapat pindah ke rumah yang sudah ada penghuninya, atau mereka bisa menyewa rumah yang belum berpenghuni dengan teman-teman. Hal ini bisa dilakukan melalui agen real estat atau pribadi. Saat menyewa properti, anda perlu membayar uang jaminan atau 'obligasi' (biasanya sewa empat minggu), dan juga sewa di muka (biasanya biasanya empat minggu). Ikatan ini untuk memperbaiki kerusakan yang anda, teman rumah atau tamu rumah anda sebabkan di properti tersebut saat menyewa.</p>
                    @else
                    <p>Many students in Australia choose to share the cost of a rental property with several housemates. Students who choose this type of housing may either move into a pre-established household, or they may set up a household with friends. This can be done through a real estate agent or privately. When renting a property you will need to pay a security deposit or 'bond' (which is usually four weeks rent), as well as rent in advance (also usually four weeks). The bond is held to repair any damage that you, your house mates or house guests cause to the property while renting. Some, or all, of this amount may be refunded to you once your tenancy agreement has terminated.
                    With homestay, you will live with an Australian family in their home. Homestay can be a good option for younger students as you will have all the comforts of an established home, often with meals and cleaning included. And it is a great way for international students to fully integrate themselves into Australian life. Families offering homestay accommodation to international students are thoroughly screened to ensure they can provide a suitable living environment for students.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
    
    <!-- About Section -->
    <!-- <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
				@if(App::getLocale() == 'id')
                <h2 style="font-weight: 400; font-size: 16px;margin-bottom:0px !important;text-transform: capitalize;margin-top:5px;color:#3b3e46;text-align:left;">Tentang</h2>
				@else
                <h2 style="font-weight: 400; font-size: 16px;margin-bottom:0px !important;text-transform: capitalize;margin-top:5px;color:#3b3e46;text-align:left;">About</h2>
				@endif
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
				@if(App::getLocale() == 'id')
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;"><b>Australia</b> adalah negara dan kontinen yang dikelilingi oleh lautan India dan Pasifik. Ibu kotanya, Canberra. Dengan kota besar - Sydney, Brisbane, Melbourne, Perth, Adelaide - adalah pesisir. Negara ini dikenal dengan Sydney Opera House, Great Barrier Reef, padang gurun padang pasir pedalaman yang luas yang disebut Outback, dan spesies hewan unik seperti kanguru dan duck-billed platypuses.</p>
                <ul class="ul-country" style="list-style: none; text-align:left;font-family:inherit;padding-left:0px;margin-top:10px;">
                <li>Ibu Kota: Canberra</li>
                <li>Bahasa: Inggris</li>
                <li>Currency: AUD</li>
                <li>Luas wilayah: 7,692,024 km2</li>
                <li>Populasi: 23.78 juta</li>
                <li>Kode panggilan/ Mobile: +61/ +61 4</li>
                <li>Pasoan listrik: 220-240 volt</li>
                <li>Colokan listrik: <img src="{{asset('ext/images/sq-id/power-plug.png')}}" width="30px"></li>
                </ul>
				@else
				<p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;"><b>Australia</b> is a country and continent surrounded by the Indian and Pacific oceans. Its major cities – Sydney, Brisbane, Melbourne, Perth, Adelaide – are coastal. Its capital, Canberra, is inland. The country is known for its Sydney Opera House, the Great Barrier Reef, a vast interior desert wilderness called the Outback, and unique animal species like kangaroos and duck-billed platypuses.</p>
                <ul class="ul-country" style="list-style: none; text-align:left;font-family:inherit;padding-left:0px;margin-top:10px;">
                <li>Capital: Canberra</li>
                <li>Language: Inggris</li>
                <li>Currency: AUD</li>
                <li>Total Area: 7,692,024 km2</li>
                <li>Population: 23.78 juta</li>
                <li>Dialing code/ Mobile: +61/ +61 4</li>
                <li>Electricity Supply: 220-240 volt</li>
                <li>Power Plug: <img src="{{asset('ext/images/sq-id/power-plug.png')}}" width="30px"></li>
                </ul>
				@endif
            </div>
        </div>      
    </section>

   
    <section id="about" class="container content-section text-center" style="padding-top:25px;">
        <div class="row">
			@if(App::getLocale() == 'id')
            <div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-bottom:0px !important;text-transform: capitalize;margin-top:5px;color:#3b3e46;text-align:left;">Akomodasi</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">Siswa internasional di Australia memiliki berbagai pilihan akomodasi. Namun, para siswa perlu tahu bahwa perumahan di universitas-universitas Australia sangat berbeda dari apa yang akan digunakan banyak siswa internasional, karena universitas-universitas di Australia hanya menyediakan sedikit atau tidak ada sama sekali. Kebanyakan siswa di universitas Australia tinggal di luar kampus, baik melalui program homestay, hostel, atau properti sewaan. Biaya akan bervariasi tergantung pada kota dan jenis akomodasi Anda.</p>
            </div>
			@else
			<div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-bottom:0px !important;text-transform: capitalize;margin-top:5px;color:#3b3e46;text-align:left;">Accommodation</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">International students in Australia have a large range of accommodation options. However, students need to be aware that housing at Australian universities is very different from what many international students will be used to, because Australian universities provide little or no university housing. It is not nonexistent, but most students at Australian universities live off-campus, whether through homestay programs, hostels, or rental properties. The costs will vary depending on your chosen state, city, and type of accommodation.</p>
            </div>
			@endif
        </div>      
    </section>

   
    <section id="about" class="container content-section text-center" style="padding-top: 25px;">
        <div class="row">
			@if(App::getLocale() == 'id')
            <div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-top:5px;margin-bottom:0px !important;text-transform: capitalize;color:#3b3e46;text-align:left;">Saat memulai petualangan</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">Pilihan akomodasi jangka pendek yang mungkin ingin Anda pertimbangkan saat pertama kali tiba di Australia antara lain; Hotel atau hostel. Perumahan sementara yang mungkin ditawarkan melalui institusi anda saat awal memulai pendidikan. Bicaralah dengan internasional support staff di institusi anda atau periksa situs web mereka untuk mendapatkan rinciannya.</p>
            </div>
			@else
			 <div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-top:5px;margin-bottom:0px !important;text-transform: capitalize;color:#3b3e46;text-align:left;">Getting started</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">Short-term accommodation options you might want to consider when you first arrive in Australia include; hotel or hostel. Temporary housing which may be offered through your institution while you get settled. Talk to your institution's international support staff or check their website for details.</p>
            </div>
			@endif
        </div>      
    </section>

     <section id="about" class="container content-section text-center" style="padding-top: 25px;">
        <div class="row">
			@if(App::getLocale() == 'id')
            <div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-top:5px;margin-bottom:0px !important;text-transform: capitalize;color:#3b3e46;text-align:left;">Akomodasi di kampus</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">Kehidupan kampus bisa menjadi pilihan terbaik untuk meminimalkan perjalanan. Meskipun sebagian besar siswa Australia tinggal di luar kampus, beberapa universitas di Australia menyediakan perumahan bagi siswa mereka melalui residential colleges, halls of residence, atau apartemen. Harga dan ketersediaan akomodasi bervariasi sesuai dengan universitas, jadi hubungi institusi anda secara langsung untuk mengetahui pilihan akomodasi yang tersedia dan bandingkan biaya bila mengatur akomodasi anda sendiri, karena pilihan akomodasi ini terbatas dan sangat populer.</p>
            </div>
			@else
			<div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-top:5px;margin-bottom:0px !important;text-transform: capitalize;color:#3b3e46;text-align:left;">On-campus accommodation</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">Campus living can be a great option to minimize travel. Although the majority of Australian students live off-campus, some Australian universities do provide housing for their students through residential colleges, halls of residence, or apartments. Prices and availability of accommodations vary according to the university, so contact your institution directly to find out the accommodation options they have available and how the costs compare with organizing your own accommodation, as these accommodation options are limited and very popular.</p>
            </div>
			@endif
        </div>      
    </section>

     <section id="about" class="container content-section text-center" style="padding-top: 25px;padding-bottom:100px;">
        <div class="row">
			@if(App::getLocale() == 'id')
            <div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-top:5px;margin-bottom:0px !important;text-transform: capitalize;color:#3b3e46;text-align:left;">Akomodasi di luar kampus</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">Banyak siswa di Australia memilih untuk berbagi biaya sewa properti dengan beberapa teman serumah. Siswa yang memilih jenis perumahan ini dapat pindah ke rumah yang sudah ada penghuninya, atau mereka bisa menyewa rumah yang belum berpenghuni dengan teman-teman. Hal ini bisa dilakukan melalui agen real estat atau pribadi. Saat menyewa properti, anda perlu membayar uang jaminan atau 'obligasi' (biasanya sewa empat minggu), dan juga sewa di muka (biasanya biasanya empat minggu). Ikatan ini untuk memperbaiki kerusakan yang anda, teman rumah atau tamu rumah anda sebabkan di properti tersebut saat menyewa. </p>
            </div>
			@else
			<div class="col-lg-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 16px;margin-top:5px;margin-bottom:0px !important;text-transform: capitalize;color:#3b3e46;text-align:left;">Off-campus accommodation</h2>
            </div>
            <div class="col-lg-10 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <p style="font-size: 16px; color:#7e8890;text-align:justify;margin-bottom:0px;">
				Many students in Australia choose to share the cost of a rental property with several housemates. Students who choose this type of housing may either move into a pre-established household, or they may set up a household with friends. This can be done through a real estate agent or privately. When renting a property you will need to pay a security deposit or 'bond' (which is usually four weeks rent), as well as rent in advance (also usually four weeks). The bond is held to repair any damage that you, your house mates or house guests cause to the property while renting. Some, or all, of this amount may be refunded to you once your tenancy agreement has terminated.
				With homestay, you will live with an Australian family in their home. Homestay can be a good option for younger students as you will have all the comforts of an established home, often with meals and cleaning included. And it is a great way for international students to fully integrate themselves into Australian life. Families offering homestay accommodation to international students are thoroughly screened to ensure they can provide a suitable living environment for students.
 				</p>
            </div>
			@endif
        </div>      
    </section> -->

    <!-- Contact Section -->
            <section class="page-section bg-scroll" data-background="/img/footer_banner.jpg" id="contact" style="background-image: url('{{asset('ext/images/sq-id/footer_banner.jpg')}}');">
                <div class="container relative">
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                            @if(App::getLocale() == 'id')
                            <div class="section-title white" style="color:#fff">
                                Mulai Perjalanan Anda
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Isilah form di bawah ini, para konselor berpengalaman kami akan menghubungi anda secepatnya.</h2>
							@else
							<div class="section-title white" style="color:#fff">
                                START YOUR JOURNEY
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Fill out the form below, our counsellors will contact you as soon as possible.</h2>
							@endif
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- form-->
					
					
					@include('includes.insertleadform')
					
					
                    </div>
                    <!-- End form -->
                    
                </div>
            </section>
            <!-- End Contact Section -->
				
			<form class="hidden submit_change_language" method="get" action="{{url('parent/country')}}" >
				<input type="hidden" name="lang" value="id"/>
				<a href="javascript:void(0);" onclick="parentNode.submit();">Submit</a>	
			</form>
				
				
@endsection