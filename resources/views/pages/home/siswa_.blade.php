<!DOCTYPE html>
<html>
    <head>
        <title>Study Query | Smart Choices for Smart Students</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="author" content="Expecto">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <!-- Favicons -->
        <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
        <link rel="apple-touch-icon" href="{{asset('ext/images/apple-touch-icon.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('ext/images/apple-touch-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('ext/images/apple-touch-icon-114x114.png')}}">
        <!-- CSS -->
        <link rel="stylesheet" href="{{asset('ext/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('ext/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('ext/css/style-responsive.css')}}">

        <link rel="stylesheet" href="{{asset('ext/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('ext/css/animations.min.css')}}">
        <link rel="stylesheet" href="{{asset('ext/css/vertical-rhythm.min.css')}}">
        <link rel="stylesheet" href="{{asset('ext/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('ext/css/magnific-popup.css')}}">        
        <link rel="stylesheet" href="{{asset('ext/css/color.css')}}">
        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="{{asset('ext/css/grayscale.css')}}" rel="stylesheet">
        
    </head>
    <div class="preloader" id="preloader">
        <div class="item">
            <div class="spinner">
        </div>
    </div>
    </div>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <!-- <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li> -->
                    <li>
                        <div class="form-group">
                          <select class="form-control" id="sel1">
                            <option>Bahasa Indonesia</option>
                            <option>English</option>
                          </select>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro" style="padding:100px;">
        <div class="intro-body">
            <div class="container" style="margin-top: 100px;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-left: 0px;">
                        <h1 class="brand-heading">Belajar di Australia Bukanlah Hanya Mimpi.</h1>
                        <p class="intro-text">Dapatkan gelar sarjana dari universitas-universitas Go8? Sarjana terlalu lama? atau bahasa Inggris Anda kurang? Study Query siap membantu!</p>
                        <p style="font-size: 14px;background: linear-gradient(to bottom right,#78c878 0,#427742 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 16px 15px;width: 210px;box-shadow:0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08);">Scroll untuk info  <i class="fa fa-long-arrow-down" aria-hidden="true"></i></p>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <a href="#about" class="btn btn-circle page-scroll scroll-down-icon">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div> -->
                <!-- <hr style="float:left;width:400px;"> -->
                <!-- <div class="divider-wrap"><div style="margin-top: 10px; height: 1px; margin-bottom: 10px;" data-width="100%" data-animate="" data-animation-delay="" data-color="default" class="divider-border"></div></div> -->
                <div class="col-lg-12 col-md-12">
                        <a href="#about" class="btn btn-circle page-scroll scroll-down-icon">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
            </div>
        </div>
    </header>


    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;">Keuntungan melalui Study Query</h2>
                <p style="font-size: 16px; color:#7e8890;">Ini yang akan kamu lakukan dan bagaimana Anda mendapatkan keuntungannya melalui Study Query!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <img src="{{asset('ext/images/sq-id/02.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Informasi yang Lengkap</h4>
                <p style="font-size: 16px; color:#7e8890;">Kami akan dengan senang hati mencari dan mencocokkan anda dengan institusi ataupun course yang sesuai dengan situasi dan nilai anda.</p>
            </div>
            <div class="col-lg-4 col-md-4 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <img src="{{asset('ext/images/sq-id/01.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Layanan Personal</h4>
                <p style="font-size: 16px; color:#7e8890;">Kami akan menghubungkan anda dengan konselor pendidikan yang berpengalaman dan yang sesuai dengan profile kamu, yang akan menemani, membantu dan memberikan solusi kepada anda dari awal sampai akhir.</p>
            </div>
            <div class="col-lg-4 col-md-4 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <img src="{{asset('ext/images/sq-id/03.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Tidak ada Biaya</h4>
                <p style="font-size: 16px; color:#7e8890;">Semua layanan dari Study Query tidak dipungut gratis! Kami dapat membantu anda dari pendaftaran sampai enrollment gratis!</p>
            </div>
        </div>
    </section>

    <!-- Download Section -->
    <section id="download" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2 style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">Nilai IELTS anda tidak mencukupi untuk masuk Universitas?</h2>
                    <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg btn-sq-cta" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">Study Query dapat Membantu!</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container-fluid content-section text-center" style="padding-bottom: 350px;background: url({{asset('ext/images/sq-id/about-banner.jpg')}}) no-repeat center bottom;background-size: 100% auto;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2 style="font-size: 28px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Pilihan Pandai untuk Murid Pintar!</h2>
                <p style="font-size: 16px; color:#7e8890;">Study Query adalah satu-satunya layanan yang mempermudah bagi para pelajar untuk melanjutkan studi di luar negeri dengan tepat, cepat dan tidak ribet! Kami mempunyai kantor di berbagai negara, dan kami bisa membantu untuk mendaftarkan anda di lebih dari 10.000 courses di Australia.

Universitas apapun yang kamu cari, ataupun jurusan apapun yang kamu inginkan, kami bisa membantu meraih cita-cita anda! Terlebih, layanan kami tidak dipungut biaya, Jadi jangan ragu lagi, langsung saja isi form diatas dan kami akan manghubungi anda secepatnya!</p>
                
                <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg btn-sq-cta" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">Find out More!</a>
            </div>
        </div>
    </section>


    <!-- Social Media Section -->
    <section id="sos-med" class="container-fluid content-section text-center" style="padding-bottom: 150px;">
        <!-- Socmed Section -->
            
                <div class="container relative">
                    
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                            <div class="section-title">
                                STUDY QUERY DI SOSIAL MEDIA<span class="st-point">.</span>
                            </div>
                            <h2 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform:lowercase;margin-top:5px;font-style: italic;">Kenali ceritanya. Dapatkan inspirasi. Mulai cerita Anda!</h2>
                            <div class="section-line mb-60 mb-xxs-30"></div>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- Gallery Grid -->
                    <ul class="works-grid clearfix" id="work-grid">
                        
                        <!-- list Item-->
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                            <a href="https://www.instagram.com/p/BWhKW_SA6wq/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/19985427_955316874610819_8113372030222467072_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="150">
                            <a href="https://www.instagram.com/p/BWb1c3_Azp0/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/20066137_1897416197164638_1815388891113324544_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="350">
                            <a href="https://www.instagram.com/p/BWWlIaVAV6F/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19933044_107629543220879_863794227614580736_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="250">
                            <a href="https://www.instagram.com/p/BWRXfzjAr5J/?taken-by=sqtotherescue" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19931529_108448109800897_2235865708355387392_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="200">
                            <a href="https://www.instagram.com/p/BWM7WMOgvYo/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/19761473_256565388163382_7351691762466816000_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="300">
                            <a href="https://www.instagram.com/p/BWJeTsEgAJC/?taken-by=sqtotherescue" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/19765059_113235292637243_5823145427607748608_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        
                        <!-- End list Item -->
                    </ul>
                    <!-- End Gallery Grid -->
                    
                    <!-- Blog Posts Grid -->
                    <div class="row multi-columns-row">
                        <div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="100">
                            
                            <!-- Post -->
                            <div class="blog-item">
                                
                                <!-- Image -->
                                <div class="blog-media relative">    
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/883265818505861/?type=3&theater" class="lp-item animate-init" target="_blank">
                                        <div class="lp-image" style="background-image: url(https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/19959142_883265818505861_394195201327101407_n.jpg?oh=c35653d0d2e53cca8062564ac3852ac6&oe=59F9299E);"></div>
                                        
                                        <div class="lp-date">
                                            <span class="lp-date-num">14</span>
                                            Jul
                                        </div>
                                        <div class="lp-more">
                                            Read More
                                        </div>
                                    </a>
                                </div>
                                
                                <!-- Post Title -->
                                <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px;">Happy Bastille Day - Vive la France!</p></h2>
                                
                                <!-- Author, Categories, Comments -->
                                <!-- @if(false) -->
                                <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                                    <span class="number">5</span>&nbsp;Likes
                                    <span class="separator">&mdash;</span>
                                    <span class="number">0</span>&nbsp;Comments
                                </div>
                                <!-- @endif -->
                                
                                <!-- Text Intro -->
                                <div class="blog-item-body">
                                    <p style="font-size: 16px; color:#7e8890;">
                                        #HappyBastilleDay #France
                                    </p>
                                </div>
                                
                                <!-- Read More Link -->
                                <div class="blog-item-foot">
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/883265818505861/?type=3&theater" target="_blank" class="btn btn-mod  btn-small">Read More <i class="fa fa-angle-right"></i></a>
                                </div>
                                
                            </div>
                            <!-- End Post -->
                            
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="225">
                            <!-- Post -->
                            <div class="blog-item">
                                
                                <!-- Image -->
                                <div class="blog-media relative">    
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/882045941961182/?type=3&theater" class="lp-item animate-init" target="_blank">
                                        <div class="lp-image" style="background-image: url(https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/19990057_882045941961182_1057742807838589852_n.jpg?oh=8d536744d6f7b51b09597f4f3da548fa&oe=59CD2490);"></div>
                                        
                                        <div class="lp-date">
                                            <span class="lp-date-num">11</span>
                                            Jul
                                        </div>

                                        <div class="lp-more">
                                            Read More
                                        </div>
                                    </a>
                                </div>
                                <!-- Post Title -->
                                <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px;">Favorite destination</p></h2>
                               <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                                    <span class="number">5</span>&nbsp;Likes
                                    <span class="separator">&mdash;</span>
                                    <span class="number">0</span>&nbsp;Comments
                                </div>
                                <!-- Text Intro -->
                                <div class="blog-item-body">
                                    <p style="font-size: 16px; color:#7e8890;">
                                        Instagram of the Day: Watching the sun go down over Sydney's skyline is a beautiful sight that everyone should experience.
                                    </p>
                                </div>
                                
                                <!-- Read More Link -->
                                <div class="blog-item-foot">
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/882045941961182/?type=3&theater" target="_blank" class="btn btn-mod  btn-small">Read More <i class="fa fa-angle-right"></i></a>
                                </div>
                                
                            </div>
                            <!-- End Post -->
                            
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="350">
                            
                            <!-- Post -->
                            <div class="blog-item">
                                
                                <!-- Image -->
                                <div class="blog-media relative">    
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/880681298764313/?type=3&theater" class="lp-item animate-init" target="_blank">
                                        <div class="lp-image" style="background-image: url(https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/19875223_880681298764313_8267833947052418072_n.jpg?oh=504d51f34a406f3b9e8b5dd2aea95862&oe=5A0A4387);"></div>
                                    
                                        <div class="lp-date">
                                            <span class="lp-date-num">9</span>
                                            Jul
                                        </div>
                                        <div class="lp-more">
                                            Read More
                                        </div>
                                    </a>
                                </div>
                                
                                <!-- Post Title -->
                                <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px;">Tired of boring education?</p></h2>
                                <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                                    <span class="number">5</span>&nbsp;Likes
                                    <span class="separator">&mdash;</span>
                                    <span class="number">0</span>&nbsp;Comments
                                </div>
                                <!-- Text Intro -->
                                <div class="blog-item-body">
                                    <p style="font-size: 16px; color:#7e8890;">
                                        Tired of boring education? Australia is one destination that always inspire! 
                                    </p>
                                </div>
                                
                                <!-- Read More Link -->
                                <div class="blog-item-foot">
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/880681298764313/?type=3&theater" target="_blank" class="btn btn-mod  btn-small">Read More <i class="fa fa-angle-right"></i></a>
                                </div>
                                
                            </div>
                            <!-- End Post -->
                            
                        </div>
                    </div>
                    <!-- End Blog Posts Grid -->
                    
                </div>
           
    </section>

    <!-- Contact Section -->
            <section class="page-section bg-scroll" data-background="{{asset('ext/images/sq-id/footer_banner.jpg')}}" id="contact" style="background-image: url('{{asset('ext/images/sq-id/footer_banner.jpg')}}');">
                <div class="container relative">
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                            <div class="section-title white" style="color:#fff">
                                Mulai Perjalanan Anda
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Isilah form di bawah ini, para konselor berpengalaman kami akan menghubungi anda secepatnya.</h2>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- form-->
                    <div class="row">
                        <div class="contact">
                            
                            
                            <!-- Contact Form -->                            
                            <form class="form contact-form" id="contact_form" autocomplete="off" method="post" enctype="multipart/form-data" action="{{url('insertlead')}}">
                                <div class="clearfix mb-20 mb-xs-0">
                                    
                                    
                                    <input type="hidden" name="utm_source" value="{{$utm_source}}"/>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="first_name" id="first-name" class="ci-field form-control" placeholder="First Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="last_name" id="last-name" class="ci-field form-control" placeholder="Last Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="phone" id="phone" class="ci-field form-control" placeholder="Phone Number" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="ci-field form-control" placeholder="Email" required>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="cf-left-col">
                                    </div>
                                    <div class="cf-right-col">
                                    </div>
                                   
                                    
                                    <button class="btn btn-default btn-lg btn-sq-cta" id="submit_btn" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;"> Next! </button>
                                </div>
                                <!-- Send Button -->
                                
                                
                                <div id="result"></div>
                                
                            </form>                            
                            <!-- End Contact Form -->
                            </div>
                        </div>
                    </div>
                    <!-- End form -->
                    
                </div>
            </section>
            <!-- End Contact Section -->


    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>© Study Query 2017. All rights reserved.</p>
        </div>
    </footer>
   <!-- JS -->
        <script type="text/javascript" src="{{asset('ext/js/fixed-nav.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/greyscale.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery-1.11.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery-migrate-1.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.easing.1.3.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/bootstrap.min.js')}}"></script>        
        <script type="text/javascript" src="{{asset('ext/js/SmoothScroll.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.scrollTo.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.localScroll.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.ba-hashchange.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.viewport.mini.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.countTo.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.appear.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.sticky.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.parallax-1.1.3.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/jquery.fitvids.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/isotope.pkgd.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/imagesloaded.pkgd.min.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('ext/js/jquery.magnific-popup.min.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('ext/js/all.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/contact-form.js')}}"></script>
        <script type="text/javascript" src="{{asset('ext/js/animations.min.js')}}"></script>
        
        <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
              ga('create', 'UA-83022576-9', 'auto');
              ga('send', 'pageview');
        </script>
        <script>
        $(window).load(function(){
        
        $(".preloader").delay(1000).fadeOut("slow")
          
        });
        </script>
        
        
    </body>
</html>
