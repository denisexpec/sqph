@extends('layout.frontend')

@section('body')
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <header id="main-header" class="the-header the-origin-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        @if(App::getLocale() == 'id')
                        <a class="navbar-brand page-scroll" href="{{url('parent')}}">
                            <img src="{{asset('ext/images/sq-id/logo.png')}}">
                        </a>
                        @else
                        <a class="navbar-brand page-scroll" href="{{url('parent')}}?lang=en">
                            <img src="{{asset('ext/images/sq-id/logo.png')}}">
                        </a>
                        @endif
                        <a href="#0" id="nav-menu-trigger" class="menu-toggle flip pull-right all-caps" style="text-transform: uppercase;font-size: 16px;font-weight: bolder;letter-spacing: .5px;margin-left: 20px;">Menu &nbsp;<i class="fa fa-bars" aria-hidden="true"></i></a> <!-- Menu Toggle -->
                        <div class="form-group">
                          <select class="form-control" id="sel1" name="change_language" style="width:160px;float:right;">
                            @if(App::getLocale() == 'id')
                                <option value="id" selected>Bahasa Indonesia</option>
                                <option value="en">English</option>
                            @else
                                <option value="id">Bahasa Indonesia</option>
                                <option value="en" selected>English</option>
                            @endif
                          </select>
                        </div>
                    </div> <!--/ .col-lg-12 -->
                </div> <!--/ .row -->
            </div> <!--/ .container -->
    </header>

    
    <!-- Navigation -->
    <nav id="nav-wrapper" class="">
            <a class="nav-close" href="#0"><span class="icon-cross2"></span></a>
            <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    @if(App::getLocale() == 'id')
                    <li>
                        <a class="page-scroll" href="{{url('parent/aboutus')}}" style="text-transform: capitalize;">Tentang StudyQuery</a>
                    </li>
                    <li>
                        <a class="page-scroll hidden" href="{{url('parent/country')}}" style="text-transform: capitalize;">Mengapa Australia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Ikuti Sosial Media</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#lead-form" style="text-transform: capitalize;">Hubungi Kami</a>
                    </li>
                    @else
                    <li>
                        <a class="page-scroll" href="{{url('parent/aboutus')}}?lang=en" style="text-transform: capitalize;">About StudyQuery</a>
                    </li>
                    <li>
                        <a class="page-scroll hidden" href="{{url('parent/country')}}?lang=en" style="text-transform: capitalize;">Why Australia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Follow Social Media</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#lead-form" style="text-transform: capitalize;">Contact Us</a>
                    </li>
                    @endif  
                </ul>
        </nav>

     <!-- Section Form Redesign -->
    <section id="hero" class="breaking" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="50" style="background-position: 50% 0px; background: url({{asset('ext/images/background-parent.jpg')}}) no-repeat center top;background-size: cover;">
        <div class="hero-split-right"></div>
                <div class="container">
                    <div class="vertical-center-wrapper">
                        <div class="vertical-center-table">
                            <div class="vertical-center-content">
                                <div class="hero-content row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 margin-top-40">
                                        @if(App::getLocale() == 'id')
                                        <h1 class="lead zero-bottom text-shadow-medium brand-heading" style="visibility: visible; ">Kami siap membantu anak Anda kuliah di Luar Negeri!</h1>
                                        <p class="text-shadow-xsmall" style="visibility: visible;">Seribu satu hal yang perlu dipersiapkan dalam perencanaan pendidikan anak anda ke Australia. Kami sangat mengerti dan siap membantu anak anda untuk mendapatkan pendidikan yang terbaik, mulai dari proses pendaftaran sampai memilih tempat tinggal yang aman dan nyaman di Australia.</p>
                                        @else
                                        <h1 class="lead zero-bottom text-shadow-medium brand-heading" style="visibility: visible; ">We're Here To Help Your Child Study Abroad!</h1>
                                        <p class="text-shadow-xsmall" style="visibility: visible;">A thousand things to prepare in your child's education plan to Australia. We understand and are ready to help your child get the best education, from the enrollment process to selecting a safe and comfortable residence in Australia.</p>
                                        @endif
                                    </div> <!--/ .col-lg-6 -->
                                    <div class="hero-form-wrapper col-lg-5 col-lg-offset-1 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 centered" style="visibility: visible; ">
                                        @if(App::getLocale() == 'id')
                                        <h4 class="all-caps margin-bot-15" style="margin-bottom: 10px;text-align: center;">Konsultasi Gratis Dengan Kami</h4>
                                        <p class="zero-bottom" style="font-size: 14px; font-style: italic;margin-bottom: 10px;text-align: center;">
                                            Isi formulir di bawah ini, konselor kami akan menghubungi Anda sesegera mungkin.
                                        @else
                                        <h4 class="all-caps margin-bot-15" style="margin-bottom: 10px;text-align: center;">FREE Consultation With Experts</h4>
                                        <p class="zero-bottom" style="font-size: 14px; font-style: italic;margin-bottom: 10px;text-align: center;">
                                            Fill out the form below, our counsellors will contact you as soon as possible.
                                        @endif
                                        </p>
                                        @include('includes.insertleadformhead')
                                    </div> <!--/ .hero-form-wrapper -->
                                </div> <!--/ .row -->
                            </div> <!--/ .vertical-center-content -->
                        </div> <!--/ .vertical-center-table -->
                    </div> <!-- / .vertical-center-wrapper -->
                </div> <!--/ .container -->
    </section>

    
	
	<!--CONTACT-->
	
	<!--END CONTACT-->

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
				@if(App::getLocale() == 'id')
                <h2 style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;">Keuntungan Melalui Study Query</h2>
                <p style="font-size: 14px; color:#7e8890;">Ini yang akan kami lakukan dan bagaimana anda mendapatkan keuntungannya melalui Study Query!</p>
				@else
				<h2 style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;">Study Query Benefits</h2>
                <p style="font-size: 14px; color:#7e8890;">Here's what we'll do and how you can get benefits from Study Query!</p>
				@endif
            </div>
        </div>
        <div class="row">
			@if(App::getLocale() == 'id')
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/02.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Informasi yang Lengkap</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query memiliki pengetahuan seputar dunia pendidikan di luar negeri yang akan membantu anak Anda untuk mengetahui dan menemukan program kuliah yang sesuai dengan situasi dan nilai anak Anda.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/01.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Layanan Personal</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query akan memberikan pelayanan konsultasi melalui konselor pendidikan berpengalaman yang akan membantu memberikan solusi pendidikan kepada anak Anda dari awal sampai akhir.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/03.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Tidak ada Biaya</h4>
                <p style="font-size: 14px; color:#7e8890;">Semua layanan konsultasi Study Query tidak dipungut biaya! Kami membantu anak Anda dari pendaftaran sampai selesai secara gratis!</p>
            </div>
			@else
			<div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/02.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Wealth of Knowledge</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query has a wealth of knowledge about education abroad that will help your children know and find the right courses that fits situation and grade.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/01.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Tailored Services</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query will provide consulting services through experienced education counsellors who will give your children educational solutions throughout the whole application process.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/03.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Free of Charge</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query consulting services are absolutely free! We help your children from registration until you get your studies for free!</p>
            </div>
			@endif
        </div>
    </section>

    <!-- Download Section -->
    <section id="download" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-6 col-md-6">
                    @if(App::getLocale() == 'id')
                    <h2 class="mob__Title" style="line-height: 40px;font-size: 21px;text-align:right;text-transform:uppercase;font-weight:bold;margin-bottom:0px;">NILAI IELTS ANAK ANDA TIDAK MENCUKUPI UNTUK MASUK UNIVERSITAS? ATAU INGIN ANAK ANDA KULIAH SAMBIL BEKERJA?</h2>
                    @else
                    <h2 class="mob__Title" style="line-height: 40px;font-size: 21px;text-align:right;text-transform:uppercase;font-weight:bold;margin-bottom:0px;">YOUR CHILD MISSED IELTS REQUIREMENTS TO UNIVERSITY? YOUR CHILD WANT TO STUDY WHILE WORKING?</h2>
                    @endif
                </div>
                <div class="col-lg-6 col-md-6">
                    @if(App::getLocale() == 'id')
                    <h2 class="mob__Medium" style="font-size:40px;text-align:left;"><b style="color:#0090a3;">Study Query</b> dapat membantu!</h2>
                    @else
                    <h2 class="mob__Medium" style="font-size:40px;text-align:left;max-width:400px;"><b style="color:#0090a3;">Study Query</b> can help!</h2>
                    @endif
                </div>
               <!--  <div class="col-lg-8 col-lg-offset-2">
                    @if(App::getLocale() == 'id')
                    <h2 style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">NILAI IELTS ANDA TIDAK MENCUKUPI UNTUK MASUK UNIVERSITAS? ATAU INGIN KULIAH SAMBIL BEKERJA?</h2>
                    <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">STUDY QUERY DAPAT MEMBANTU!</a>
                    @else
                    <h2 style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">MISSED YOUR IELTS REQUIREMENTS TO UNIVERSITY? WANT TO STUDY WHILE WORKING?</h2>
                    <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">STUDY QUERY CAN HELP!</a>
                    @endif
                </div> -->
            </div>
                <!-- <div class="col-lg-8 col-lg-offset-2">
					@if(App::getLocale() == 'id')
                    <h2 class="cta-title" style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">NILAI IELTS ANAK ANDA TIDAK MENCUKUPI UNTUK MASUK UNIVERSITAS?</h2>
                    <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form btn-bantu" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">STUDY QUERY DAPAT MEMBANTU!</a>
					@else
					<h2 style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">MISSED YOUR CHILDREN IELTS REQUIREMENTS TO UNIVERSITY?</h2>
                    <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">STUDY QUERY CAN HELP!</a>
					@endif
                </div> -->
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container-fluid content-section text-center" style="padding-bottom: 350px;background: url({{asset('ext/images/sq-id/about-banner.jpg')}}) no-repeat center bottom;background-size: 100% auto;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
				@if(App::getLocale() == 'id')
                <h2 style="font-size: 28px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Pilihan Pandai untuk Murid Pintar!</h2>
                <p style="font-size: 14px; color:#7e8890;">Study Query adalah satu-satunya layanan yang mempermudah bagi para pelajar untuk melanjutkan studi di luar negeri dengan tepat, cepat dan terpercaya. Kami akan membantu anda di semua proses, TANPA BIAYA!

Study Query bisa mendaftarkan anda di lebih dari 10.000 jurusan di institusi-intitusi pendidikan yang terbaik. Universitas apapun yang kamu cari, ataupun jurusan apapun yang kamu inginkan, kami bisa membantu meraihcita-cita anda! Jadi jangan ragu lagi, langsung saja isi form data anda dan kami akan menghubungi anda secepatnya!</p>
                @else
				<h2 style="font-size: 28px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Truly the smart choice for smart students!</h2>
                <p style="font-size: 14px; color:#7e8890;">
				Study Query is the only service that makes it easy for students to continue studying abroad with precise, fast and reliable assistance. We will support you throughout the entire process, ABSOLUTELY FREE!

Study Query can enroll you in over 10,000 courses within the top educational providers. Whatever university or course you are looking for, we can help you achieve your goals! Do not hesitate anymore, just fill your data form and we will contact you as soon as possible!

				</p>
				@endif
                <a href="#lead-form" class="btn btn-default page-scroll btn-lg btn-sq-cta" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">Find out More!</a>
            </div>
        </div>
    </section>


    <!-- Social Media Section -->
    <section id="sosmed-foot" class="container-fluid content-section text-center" style="padding-bottom: 150px;">
        <!-- Socmed Section -->
            
                <div class="container relative">
                    
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
							@if(App::getLocale() == 'id')
                            <div class="section-title" style="text-transform:capitalize;">
                                Study Query di Media Sosial
                            </div>
                            <h2 class="section-heading" style="font-size: 14px; font-weight:normal; margin-top: 25px;text-transform: capitalize;margin-top:5px;font-family:'Raleway',sans-serif;">Ketahui berita dan perkembangan terkini seputar sekolah di luar negeri dari akun media sosial Study Query</h2>
							@else
							 <div class="section-title" style="text-transform:capitalize;">
                                Study Query in Social Media<span class="st-point">.</span>
                            </div>
                            <h2 class="section-heading" style="font-size: 14px; font-weight:normal; margin-top: 25px;text-transform: capitalize;margin-top:5px;font-family:'Raleway',sans-serif;">Get up to speed with news and developments straight from Study Query’s social media updates</h2>
							@endif
                            <div class="section-line mb-60 mb-xxs-30"></div>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- Gallery Grid -->
					@include('includes.instagram')
                    <!-- End Gallery Grid -->
                    
                    <!-- Blog Posts Grid -->
                    <div class="row multi-columns-row">
						@include('includes.facebook')
                    </div>
                    <!-- End Blog Posts Grid -->
                    
                </div>
           
    </section>

    <!-- Contact Section -->
	<!-- <section class="page-section bg-scroll" data-background="{{asset('ext/images/sq-id/footer_banner.jpg')}}" id="into_form" style="background-image: url('{{asset('ext/images/sq-id/footer_banner.jpg')}}');" id="into_form">
                <div class="container relative"> -->
                    <!-- Section Headings -->
                    <!-- <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
							@if(App::getLocale() == 'id')
                            <div class="section-title white" style="color:#fff;font-size:33px;">
                                Konsultasi gratis dengan kami
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Isilah form di bawah ini, para konselor berpengalaman kami akan menghubungi anda secepatnya.</h2>
							@else
							<div class="section-title white" style="color:#fff">
                                START YOUR JOURNEY
                            </div>
                            <h2 class="section-heading white" style="color: #fff;font-family: inherit;font-weight:lighter; text-transform: lowercase;">Fill out the form below, our counsellors will contact you as soon as possible.</h2>
							@endif
                        </div>
                    </div> -->
                    <!-- End Section Headings -->
                    
                    <!-- form-->
                    <!-- <div class="row">
                        <div class="contact"> -->
                        <section id="lead-form">
                            @include('includes.insertleadformparent')
						</section>
					<!-- </div>
				</div> -->
                    <!-- End form -->
                    
                <!-- </div>
            </section> -->
            <!-- End Contact Section -->
			
			
			<form class="hidden submit_change_language" method="get" action="{{url('parent')}}" >
				<input type="hidden" name="lang" value="id"/>
				<a href="javascript:void(0);" onclick="parentNode.submit();">Submit</a>	
			</form>

@endsection