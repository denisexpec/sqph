@extends('layout.frontend')

@section('body')
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
				@if(App::getLocale() == 'id')
                <a class="navbar-brand page-scroll" href="{{url('')}}">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
				@else
				<a class="navbar-brand page-scroll" href="{{url('')}}?lang=en">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
				@endif
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
					@if(App::getLocale() == 'id')
                    <li>
						<a class="page-scroll" href="{{url('aboutus')}}" style="text-transform: capitalize;">Tentang StudyQuery</a>
					</li>
                    <li>
                        <a class="page-scroll hidden" href="{{url('country')}}" style="text-transform: capitalize;">Mengapa Australia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Ikuti Sosial Media</a>
                    </li>
					<li>
                        <a class="page-scroll" href="#lead-form" style="text-transform: capitalize;">Hubungi Kami</a>
                    </li>
					@else
					<li>
                        <a class="page-scroll" href="{{url('aboutus')}}?lang=en" style="text-transform: capitalize;">About StudyQuery</a>
                    </li>
                    <li>
                        <a class="page-scroll hidden" href="{{url('country')}}?lang=en" style="text-transform: capitalize;">Why Australia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Follow Social Media</a>
                    </li>
					<li>
                        <a class="page-scroll" href="#lead-form" style="text-transform: capitalize;">Contact Us</a>
                    </li>
					@endif
                    <li>
                        <div class="form-group">
                          <select class="form-control" id="sel1" name="change_language">
							@if(App::getLocale() == 'id')
								<option value="id" selected>Bahasa Indonesia</option>
								<option value="en">English</option>
							@else
								<option value="id">Bahasa Indonesia</option>
								<option value="en" selected>English</option>
							@endif
                          </select>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro" style="position: relative;">
        <div class="intro-body">
            <div class="container" style="margin-top:0px;">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-md-offset-2 heading__BannerL" style="margin-left: 0px;">
						@if(App::getLocale() == 'id')
							<h1 class="brand-heading">Buka Pintu Petualangan Barumu dengan kuliah di Luar Negeri!</h1>
							<p class="intro-text">Dapatkan gelar akademik yang diakui seluruh dunia dan pengalaman hidup yang tak terlupakan seumur hidupmu.</p>
                        @else
							<h1 class="brand-heading">Open Your New Door To Adventure By Studying Abroad!</h1>
							<p class="intro-text">Get an internationally recognized academic degree and unforgettable life experiences for the rest of your life.</p>
						@endif
                        @if(App::getLocale() == 'id')
                        <a href="#about" class="page-scroll"><button class="home_Banner-But hidden" style="font-size: 14px;float:left;margin-bottom: 30px;background: linear-gradient(to bottom right,#78c878 0,#427742 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 15px 15px;width: 210px;box-shadow:0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08);">Scroll ke bawah <i class="fa fa-long-arrow-down" aria-hidden="true"></i></button></a>
                        @else
                        <a href="#about" class="page-scroll"><button class="home_Banner-But hidden" style="font-size: 14px;float:left;margin-bottom: 30px;background: linear-gradient(to bottom right,#78c878 0,#427742 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 15px 15px;width: 210px;box-shadow:0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08);">Scroll to explore <i class="fa fa-long-arrow-down" aria-hidden="true"></i></button></a>
                        @endif
                    </div>
                    <div id="form__Home" class="col-lg-5 col-md-5 heading__BannerR">
                        @include('includes.insertleadformhead')
                    </div>
                </div>

            </div>
            <!-- <a href="#about" class="btn btn-circle page-scroll scroll-down-icon" style="bottom:0 !important;">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a> -->
        </div>	
    </header>
	
	
	<!-- CONTACT -->

	<!-- END CONTACT -->

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
				@if(App::getLocale() == 'id')
                <h2 style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;">Keuntungan Melalui Study Query</h2>
                <p style="font-size: 14px; color:#7e8890;">Ini yang akan kami lakukan dan bagaimana anda mendapatkan keuntungannya melalui Study Query!</p>
				@else
				<h2 style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;">Study Query Benefits</h2>
                <p style="font-size: 14px; color:#7e8890;">Here's what we'll do and how you can get benefits from Study Query!</p>
				@endif
            </div>
        </div>
        <div class="row">
			@if(App::getLocale() == 'id')
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/02.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Informasi yang Lengkap</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query memiliki pengetahuan seputar dunia pendidikan di luar negeri yang akan membantu anda untuk mengetahui dan menemukan program kuliah yang sesuai dengan situasi dan nilai anda.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/01.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Layanan Personal</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query akan memberikan pelayanan konsultasi melalui konselor pendidikan berpengalaman yang akan membantu memberikan solusi pendidikan kepada anda dari awal sampai akhir.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/03.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Tidak ada Biaya</h4>
                <p style="font-size: 14px; color:#7e8890;">Semua layanan konsultasi Study Query tidak dipungut biaya! Kami membantu anda dari pendaftaran sampai selesai secara  gratis!</p>
            </div>
			@else
			<div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/02.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Wealth of Knowledge</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query has a wealth of knowledge about education abroad that will help you know and find the right courses that fits your situation and grade.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/01.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Tailored Services</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query will provide consulting services through experienced education counsellors who will give you educational solutions throughout the whole application process.</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="{{asset('ext/images/sq-id/03.png')}}">
                <h4 style="font-size: 18px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Free of Charge</h4>
                <p style="font-size: 14px; color:#7e8890;">Study Query consulting services are absolutely free! We help you from registration until you get your studies for free!</p>
            </div>
			@endif
        </div>
    </section>

    <!-- Download Section -->
    <section id="download" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-6 col-md-6">
                    @if(App::getLocale() == 'id')
                    <h2 style="line-height: 40px;font-size: 21px;text-align:right;text-transform:uppercase;font-weight:bold;margin-bottom:0px;">NILAI IELTS ANDA TIDAK MENCUKUPI UNTUK MASUK UNIVERSITAS? ATAU INGIN KULIAH SAMBIL BEKERJA?</h2>
                    @else
                    <h2 style="line-height: 40px;font-size: 21px;text-align:right;text-transform:uppercase;font-weight:bold;margin-bottom:0px;">MISSED YOUR IELTS REQUIREMENTS TO UNIVERSITY? WANT TO STUDY WHILE WORKING?</h2>
                    @endif
                </div>
                <div class="col-lg-6 col-md-6">
                    @if(App::getLocale() == 'id')
                    <h2 style="font-size:40px;text-align:left;"><b style="color:#0090a3;">Study Query</b> dapat membantu!</h2>
                    @else
                    <h2 style="font-size:40px;text-align:left;"><b style="color:#0090a3;">Study Query</b> can help!</h2>
                    @endif
                </div>
               <!--  <div class="col-lg-8 col-lg-offset-2">
					@if(App::getLocale() == 'id')
                    <h2 style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">NILAI IELTS ANDA TIDAK MENCUKUPI UNTUK MASUK UNIVERSITAS? ATAU INGIN KULIAH SAMBIL BEKERJA?</h2>
                    <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">STUDY QUERY DAPAT MEMBANTU!</a>
					@else
					<h2 style="line-height: 40px;font-size: 36px;text-transform: uppercase;font-weight: bold;">MISSED YOUR IELTS REQUIREMENTS TO UNIVERSITY? WANT TO STUDY WHILE WORKING?</h2>
                    <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">STUDY QUERY CAN HELP!</a>
					@endif
                </div> -->
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container-fluid content-section text-center" style="padding-bottom: 350px;background: url({{asset('ext/images/sq-id/about-banner.jpg')}}) no-repeat center bottom;background-size: 100% auto;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
				@if(App::getLocale() == 'id')
                <h2 style="font-size: 28px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Pilihan Pandai untuk Murid Pintar!</h2>
                <p style="font-size: 14px; color:#7e8890;">Study Query adalah satu-satunya layanan yang mempermudah bagi para pelajar untuk melanjutkan studi di luar negeri dengan tepat, cepat dan terpercaya. Kami akan membantu anda di semua proses, TANPA BIAYA!

Study Query bisa mendaftarkan anda di lebih dari 10.000 jurusan di institusi-intitusi pendidikan yang terbaik. Universitas apapun yang kamu cari, ataupun jurusan apapun yang kamu inginkan, kami bisa membantu meraihcita-cita anda! Jadi jangan ragu lagi, langsung saja isi form data anda dan kami akan menghubungi anda secepatnya!</p>
                @else
				<h2 style="font-size: 28px; font-weight:lighter; margin-top: 25px;text-transform: capitalize;">Truly the smart choice for smart students!</h2>
                <p style="font-size: 14px; color:#7e8890;">
				Study Query is the only service that makes it easy for students to continue studying abroad with precise, fast and reliable assistance. We will support you throughout the entire process, ABSOLUTELY FREE!

Study Query can enroll you in over 10,000 courses within the top educational providers. Whatever university or course you are looking for, we can help you achieve your goals! Do not hesitate anymore, just fill your data form and we will contact you as soon as possible!

				</p>
				@endif
                <a href="javascript:void(0);" class="btn btn-default btn-lg btn-sq-cta into_form" style="font-size: 12px;background: #0890A1
;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">Find out More!</a>
            </div>
        </div>
    </section>


    <!-- Social Media Section -->
    <section id="sosmed-foot" class="container-fluid content-section text-center" style="padding-bottom: 150px;background:#F2F3F4
;">
        <!-- Socmed Section -->
            
                <div class="container relative">
                    
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
							@if(App::getLocale() == 'id')
                            <div class="section-title" style="text-transform:capitalize;">
                                Study Query di Media Sosial
                            </div>
                            <h2 class="section-heading" style="font-size: 14px; font-weight:normal; margin-top: 25px;text-transform: capitalize;margin-top:5px;font-family:'Raleway',sans-serif;">Ketahui berita dan perkembangan terkini seputar sekolah di luar negeri dari akun media sosial Study Query</h2>
							@else
							 <div class="section-title" style="text-transform:capitalize;">
                                Study Query in Social Media<span class="st-point">.</span>
                            </div>
                            <h2 class="section-heading" style="font-size: 14px; font-weight:normal; margin-top: 25px;text-transform: capitalize;margin-top:5px;font-family:'Raleway',sans-serif;">Get up to speed with news and developments straight from Study Query’s social media updates</h2>
							@endif
                            <div class="section-line mb-60 mb-xxs-30"></div>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- Gallery Grid -->
                    <ul class="works-grid clearfix" id="work-grid">
                        
                        <!-- list Item-->
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                            <a href="https://www.instagram.com/p/BWhKW_SA6wq/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/19985427_955316874610819_8113372030222467072_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="150">
                            <a href="https://www.instagram.com/p/BWb1c3_Azp0/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/20066137_1897416197164638_1815388891113324544_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="350">
                            <a href="https://www.instagram.com/p/BWWlIaVAV6F/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19933044_107629543220879_863794227614580736_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="250">
                            <a href="https://www.instagram.com/p/BWRXfzjAr5J/?taken-by=sqtotherescue" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19931529_108448109800897_2235865708355387392_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="200">
                            <a href="https://www.instagram.com/p/BWM7WMOgvYo/?taken-by=sqtotherescue" class="work-lightbox-link mfp-image" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/19761473_256565388163382_7351691762466816000_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        <li class="work-item mix photography branding animate-init" data-anim-type="fade-in-down-big" data-anim-delay="300">
                            <a href="https://www.instagram.com/p/BWJeTsEgAJC/?taken-by=sqtotherescue" target="_blank">
                                <div class="work-img">
                                    <img src="https://scontent-sin6-1.cdninstagram.com/t51.2885-15/e35/19765059_113235292637243_5823145427607748608_n.jpg" alt="Work" />
                                </div>
                            </a>
                        </li>
                        
                        <!-- End list Item -->
                    </ul>
                    <!-- End Gallery Grid -->
                    
                    <!-- Blog Posts Grid -->
                    <div class="row multi-columns-row">
                        <div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="100">
                            
                            <!-- Post -->
                            <div class="blog-item">
                                
                                <!-- Image -->
                                <div class="blog-media relative">    
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/883265818505861/?type=3&theater" class="lp-item animate-init" target="_blank">
                                        <div class="lp-image" style="background-image: url(https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/19959142_883265818505861_394195201327101407_n.jpg?oh=c35653d0d2e53cca8062564ac3852ac6&oe=59F9299E);"></div>
                                        
                                        <div class="lp-date">
                                            <span class="lp-date-num">14</span>
                                            Jul
                                        </div>
                                        <div class="lp-more">
                                            Read More
                                        </div>
                                    </a>
                                </div>
                                
                                <!-- Post Title -->
                                <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px;">Happy Bastille Day - Vive la France!</p></h2>
                                
                                <!-- Author, Categories, Comments -->
                                <!-- @if(false) -->
                                <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                                    <span class="number">5</span>&nbsp;Likes
                                    <span class="separator">&mdash;</span>
                                    <span class="number">0</span>&nbsp;Comments
                                </div>
                                <!-- @endif -->
                                
                                <!-- Text Intro -->
                                <div class="blog-item-body">
                                    <p style="font-size: 14px; color:#7e8890;">
                                        #HappyBastilleDay #France
                                    </p>
                                </div>
                                
                                <!-- Read More Link -->
                                <div class="blog-item-foot">
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/883265818505861/?type=3&theater" target="_blank" class="btn btn-mod btn-small" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 15px 20px;">Read More <!-- <i class="fa fa-angle-right"></i> --></a>
                                </div>
                                
                            </div>
                            <!-- End Post -->
                            
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="225">
                            <!-- Post -->
                            <div class="blog-item">
                                
                                <!-- Image -->
                                <div class="blog-media relative">    
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/882045941961182/?type=3&theater" class="lp-item animate-init" target="_blank">
                                        <div class="lp-image" style="background-image: url(https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/19990057_882045941961182_1057742807838589852_n.jpg?oh=8d536744d6f7b51b09597f4f3da548fa&oe=59CD2490);"></div>
                                        
                                        <div class="lp-date">
                                            <span class="lp-date-num">11</span>
                                            Jul
                                        </div>

                                        <div class="lp-more">
                                            Read More
                                        </div>
                                    </a>
                                </div>
                                <!-- Post Title -->
                                <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px;">Favorite destination</p></h2>
                               <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                                    <span class="number">5</span>&nbsp;Likes
                                    <span class="separator">&mdash;</span>
                                    <span class="number">0</span>&nbsp;Comments
                                </div>
                                <!-- Text Intro -->
                                <div class="blog-item-body">
                                    <p style="font-size: 14px; color:#7e8890;">
                                        Instagram of the Day: Watching the sun go down over Sydney's skyline is a beautiful sight that everyone should experience.
                                    </p>
                                </div>
                                
                                <!-- Read More Link -->
                                <div class="blog-item-foot">
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/882045941961182/?type=3&theater" target="_blank" class="btn btn-mod btn-small" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 15px 20px;">Read More <!-- <i class="fa fa-angle-right"></i> --></a>
                                </div>
                                
                            </div>
                            <!-- End Post -->
                            
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4 animate-init" data-anim-type="fade-in-up-big" data-anim-delay="350">
                            
                            <!-- Post -->
                            <div class="blog-item">
                                
                                <!-- Image -->
                                <div class="blog-media relative">    
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/880681298764313/?type=3&theater" class="lp-item animate-init" target="_blank">
                                        <div class="lp-image" style="background-image: url(https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/19875223_880681298764313_8267833947052418072_n.jpg?oh=504d51f34a406f3b9e8b5dd2aea95862&oe=5A0A4387);"></div>
                                    
                                        <div class="lp-date">
                                            <span class="lp-date-num">9</span>
                                            Jul
                                        </div>
                                        <div class="lp-more">
                                            Read More
                                        </div>
                                    </a>
                                </div>
                                
                                <!-- Post Title -->
                                <h2 class="blog-item-title" style="margin-bottom:15px;"><p class="small strong" style="margin-bottom:15px;">Tired of boring education?</p></h2>
                                <div class="blog-item-data mb-10" style="margin-bottom:15px;">
                                    <span class="number">5</span>&nbsp;Likes
                                    <span class="separator">&mdash;</span>
                                    <span class="number">0</span>&nbsp;Comments
                                </div>
                                <!-- Text Intro -->
                                <div class="blog-item-body">
                                    <p style="font-size: 14px; color:#7e8890;">
                                        Tired of boring education? Australia is one destination that always inspire! 
                                    </p>
                                </div>
                                
                                <!-- Read More Link -->
                                <div class="blog-item-foot">
                                    <a href="https://www.facebook.com/StudyQuery/photos/a.749886305177147.1073741828.563745953791184/880681298764313/?type=3&theater" target="_blank" class="btn btn-mod btn-small" style="font-size: 11px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius: 50px;padding: 15px 20px;">Read More <!-- <i class="fa fa-angle-right"></i> --></a>
                                </div>
                                
                            </div>
                            <!-- End Post -->
                            
                        </div>
                    </div>
                    <!-- End Blog Posts Grid -->
                    
                </div>
           
    </section>

    <!-- Contact Section -->
    <section id="lead-form">
	@include('includes.insertleadform')
	<!-- End Contact Section -->
    </section>
			
			
			<form class="hidden submit_change_language" method="get" action="{{url('')}}" >
				<input type="hidden" name="lang" value="id"/>
				<a href="javascript:void(0);" onclick="parentNode.submit();">Submit</a>	
			</form>
@endsection