<!DOCTYPE html>
<html lang="en">
   <head>
        <meta charset="utf-8">
        <title>StudyQuery | Report</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="_token" content="{{ csrf_token() }}">
        <!-- master css -->
        <link href="{{ asset('ext/sqlp/css/master.css') }}" rel="stylesheet">
        <link href="{{ asset('ext/sqlp/css/color.css') }}" rel="stylesheet">
        <link href="{{asset('ext/sqlp/css/style2.css')}}" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('ext/sqlp/images/favicon.ico') }}">
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'><!--[if lt IE 9]>
    <![endif]-->
		<link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet">
		
		
        <link href="{{asset('ext/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('ext/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('ext/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('ext/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('ext/assets/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
		
		<style>
			.datepicker{
				background:white;
			}
		</style>
		
    </head>
    <body>
        <div class="full" style="background:none">
            <div class="container main" id="survey01">
			
				@if(false)
				<div class="text-left">
					<?php 
						$counter = 0; 
						$total_value = 0; 
					?>
						@foreach($data_lead as $lead)
							<?php 
								if(isset($lead->date_response)){
									//Convert them to timestamps.
									$date1Timestamp = strtotime($lead->created_at);
									$date2Timestamp = strtotime($lead->date_response);

									//Calculate the difference.
									$difference = $date2Timestamp - $date1Timestamp;
									
									$total_value += $difference;
									$counter++;
								}
							?>
						@endforeach
						
						@if($counter == 0)
							<p>Average Response Time : - minute.</p>
						@else
							<?php $calc = $total_value / $counter; ?>
							<p>Average Response Time : <?php echo gmdate("i",$calc); ?> minute.</p>
						@endif
				</div>
				@endif
				
                <div>
					@if($active_link == 'all')
						<a href="{{url('report/x9878NBkPvKCzhJz2KKS')}}" class="btnn" style="background: #BCD432;color: #000;">All</a>
						<a href="{{url('report/ph/x9878NBkPvKCzhJz2KKS')}}?country=philippines" class="btnn">Philippines</a>
						<a href="{{url('report/others/x9878NBkPvKCzhJz2KKS')}}?country=others" class="btnn" >Others</a>
					@elseif($active_link == 'Philippines')
						<a href="{{url('report/x9878NBkPvKCzhJz2KKS')}}" class="btnn" >All</a>
						<a href="{{url('report/ph/x9878NBkPvKCzhJz2KKS')}}?country=philippines" class="btnn" style="background: #BCD432;color: #000;">Philippines</a>
						<a href="{{url('report/others/x9878NBkPvKCzhJz2KKS')}}?country=others" class="btnn" >Others</a>
					@elseif($active_link == 'others')
						<a href="{{url('report/x9878NBkPvKCzhJz2KKS')}}" class="btnn" >All</a>
						<a href="{{url('report/ph/x9878NBkPvKCzhJz2KKS')}}?country=philippines" class="btnn">Philippines</a>
						<a href="{{url('report/others/x9878NBkPvKCzhJz2KKS')}}?country=others" class="btnn" style="background: #BCD432;color: #000;">Others</a>
					@endif
					
				</div>
				
				<form action="{{url('report/x9878NBkPvKCzhJz2KKS/reportfilter')}}" method="post">
						{{csrf_field()}}
                        <?php $all = 'all'; ?>
                        <input type="hidden" name="choose_country" value="@if(empty($_GET['country'])){{$all}}@else{{$_GET['country']}}@endif"/>
                        <div class="form-group">
							<label class="control-label col-md-2" style="text-align:left;margin-left:-16px;">From Date</label>
							<div class="col-md-3">
								@if(isset($from_date))
								<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="from_date" value="{{$from_date}}" />
								@else
								<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="from_date" value="" />
								@endif
								
								<span class="help-block"> Select date </span>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<label class="control-label col-md-2" style="text-align:left;margin-left:-16px;">Until Date</label>
							<div class="col-md-3">
								@if(isset($until_date))
									<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="until_date" value="{{$until_date}}" />
								@else
									<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="until_date" value="" />
								@endif
								
							<span class="help-block"> Select date </span>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<label class="control-label col-md-3" style="visibility:hidden;">Submit this Form : </label>
							<div class="col-md-2" style="text-align:left;margin-left:-15px;">
								<input type="submit" value="Submit" class="btn btn-success"/>
							</div>
						</div>
					</form>
				
				
				
                <div id="wrapper_content" style="position:relative">
					<div class="text-right">
						@if(isset($from_date) && isset($until_date))
						<a href="{{url('exportlead')}}?country={{$active_link}}&from_date={{$from_date}}&until_date={{$until_date}}" class="btn btn-primary">Export to Excel</a>
						@else
						<a href="{{url('exportlead')}}?country={{$active_link}}" class="btn btn-primary">Export to Excel</a>
						@endif
					</div>
						
				<table class="table table-bordered datatable">
                    <thead>
                        <tr>
							<th></th>
                            <th class="text-center">Country Group</th>
                            <th class="text-center">Lead Score</th>
                            <th class="text-center">Phone Number</th>
                            <th class="text-center">Contact Detail</th>
                            <th class="text-center" style="width:20%">Answer</th>
                            <th class="text-center">Email</th>
							<th class="text-center">LeadSource</th>
                            <th class="text-center">Date Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--This is ID From Candidate -->
                        @foreach($data_lead as $survey)
                            <tr>
								<td>
									@if(!empty($survey->lead_rating))
										
											<div class="candidate_information ">
												<p class="candidate_type_value">
												@if($survey->lead_rating == 4)
													<span class='label label-default'>cold</span>
												@elseif($survey->lead_rating == 2)
													<span class='label label-warning'>warm</span>
												@elseif($survey->lead_rating == 3)
													<span class='label label-warning'>luke warm</span>
												@elseif($survey->lead_rating == 1)
													<span class='label label-danger'>hot</span>
												@endif
												</p>

												<p class="candidate_type_lead_status">
													@if($survey->lead_status == 7)
														<span class='label label-warning'>junk</span>
													@elseif($survey->lead_status == 6)
														<span class='label label-warning'>closed</span>
													@elseif($survey->lead_status == 5)
														<span class='label label-default'>converted</span>
													@elseif($survey->lead_status == 4)
														<span class='label label-danger'>insufficient</span>
													@elseif($survey->lead_status == 3)
														<span class='label label-danger'>contacted</span>
													@elseif($survey->lead_status == 2)
														<span class='label label-danger'>attempted</span>
													@elseif($survey->lead_status == 1)
														<span class='label label-danger'>open</span>
													@endif
												</p>
											
												
												<p class="candidate_note_value">Note : {{ $survey->note_counselor or '' }}</p>
												
												<button class="edit_candidate_type form-control">EDIT STATUS</button>
											</div>
                                        
											<div class="form-group hidden">
												@if($survey->lead_rating == 4)
													<select class="form-control" id="candidate_type" required>
														<option value="4" selected>COLD</option>
														<option value="2">WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@elseif($survey->lead_rating == 2) 
													<select class="form-control" id="candidate_type" required>
														<option value="4" >cold</option>
														<option value="2"selected>WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@elseif($survey->lead_rating == 3)  
													<select class="form-control" id="candidate_type" required>
														<option value="4" >COLD</option>
														<option value="2">WARM</option>
														<option value="3" selected>LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@elseif($survey->lead_rating == 1)  
													<select class="form-control" id="candidate_type" required>
														<option value="4">COLD</option>
														<option value="2">WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1" selected>HOT</option>
													</select>
												@else
													<select class="form-control" id="candidate_type" required>
														<option disabled selected>Please Choose Candidate Type</option>
														<option value="4">COLD</option>
														<option value="2">WARM</option>
														<option value="3">WARM</option>
														<option value="1">HOT</option>
													</select>
												@endif
												
												@if($survey->lead_status == 1)
													<select name="lead_status" class="form-control">
														<option value="1" selected>OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 2)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2" selected>ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 3)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3" selected>CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 4)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4" selected>INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 5)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5" selected>CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 6)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6" selected>CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 7)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
                                                                                                                <option value="7" selected>JUNK</option>
													</select>
												@else
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@endif
												
												<textarea rows="3" cols="50" class="form-control" id="candidate_note" style="margin:10px 0px;" placeholder="Take a note of Candidate here">@if($survey->note_counselor){{$survey->note_counselor}}@endif</textarea>
												
												<input type="submit" value="Submit" class="form-control candidate_submit" data-id="{{$survey->id}}"/>
											</div> 
											
									@else
											<div class="candidate_information hidden">
												<p class="candidate_type_value">Candidate Type : {{$survey->lead_rating or 'Not Decided'}}</p>
												<p class="candidate_type_lead_status"></p>
												<p class="candidate_note_value">Note : {{ $survey->note_counselor or '' }}</p>
												
												<button class="edit_candidate_type form-control">EDIT STATUS</button>
											</div>
											
											<div class="form-group">
												@if($survey->lead_rating == 4)
													<select class="form-control" id="candidate_type" required>
														<option value="4" selected>COLD</option>
														<option value="2">WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@elseif($survey->lead_rating == 2) 
													<select class="form-control" id="candidate_type" required>
														<option value="4" >COLD</option>
														<option value="2"selected>WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@elseif($survey->lead_rating == 3)  
													<select class="form-control" id="candidate_type" required>
														<option value="4" >JUNK</option>
														<option value="2">WARM</option>
														<option value="3" selected>LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@elseif($survey->lead_rating == 1)  
													<select class="form-control" id="candidate_type" required>
														<option value="4" >COLD</option>
														<option value="2">WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1" selected>HOT</option>
													</select>
												@else
													<span class="hidden candidate_type_info" style="color:red;">Candidate Type cannot leave blank.</span>
													<select class="form-control" id="candidate_type" required>
														<option disabled selected>Please Choose Candidate Type</option>
														<option value="4">COLD</option>
														<option value="2">WARM</option>
														<option value="3">LUKE WARM</option>
														<option value="1">HOT</option>
													</select>
												@endif
												
												
												
												@if($survey->lead_status == 1)
													<select name="lead_status" class="form-control">
														<option value="1" selected>OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 2)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2" selected>ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 3)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3" selected>CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 4)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4" selected>INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 5)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5" selected>CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 6)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6" selected>CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@elseif($survey->lead_status == 7)
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
                                                                                                                <option value="7" selected>JUNK</option>
													</select>
												@else
													<select name="lead_status" class="form-control">
														<option value="1">OPEN</option>
														<option value="2">ATTEMPTED</option>
														<option value="3">CONTACTED</option>
														<option value="4">INSUFFICIENT</option>
														<option value="5">CONVERTED</option>
														<option value="6">CLOSED</option>
														<option value="7">JUNK</option>
													</select>
												@endif
												
												
												<textarea rows="3" cols="50" class="form-control" id="candidate_note" style="margin:10px 0px;" placeholder="Take a note of Candidate here">@if($survey->note_counselor){{$survey->note_counselor}}@endif</textarea>
												<input type="submit" value="Submit" class="form-control candidate_submit" data-id="{{$survey->id}}"/>
											</div> 
                                        @endif
                                    </div> 
									
									
									@if(false)
										<p>________________</p>
										
										@if(!empty($survey->lead_status))
											<div class="candidate_information_status">
												<p class="candidate_type_value">
												@if($survey->lead_status == 7)
													<span class='label label-warning'>junk</span>
												@elseif($survey->lead_status == 6)
													<span class='label label-warning'>closed</span>
												@elseif($survey->lead_status == 5)
													<span class='label label-default'>converted</span>
												@elseif($survey->lead_status == 4)
													<span class='label label-danger'>contacted</span>
												@elseif($survey->lead_status == 3)
													<span class='label label-danger'>insufficient</span>
												@elseif($survey->lead_status == 2)
													<span class='label label-danger'>attempted</span>
												@elseif($survey->lead_status == 1)
													<span class='label label-danger'>open</span>
												@endif
												</p>
												<button class="edit_candidate_status form-control">EDIT</button>
											</div>
										@endif
									@endif
								
								</td>
								<td>
								@if(isset($survey->leadsurvey))
									{{$survey->leadsurvey->nationality}}
								@else
									Indonesia
								@endif
								</td>
                                <td>
									@if(isset($survey->leadsurvey))
										{{$survey->leadsurvey->current_score}}
									@else
										-
									@endif
								</td>
                                <td>{{$survey->mobile_phone}}</td>
                                <td>{{$survey->first_name}} {{$survey->last_name}}</td>
                                <td>
									@if(isset($survey->leadsurvey->age_group))
										Age Group : {{$survey->leadsurvey->age_group}} <br/>
										Gender : {{$survey->leadsurvey->gender}} <br/>
										Nationality : {{$survey->leadsurvey->nationality}} <br/>
										
										@if($survey->leadsurvey->current_location)
										Current Location : {{$survey->leadsurvey->current_location}} <br/>
										@endif
										
										@if($survey->leadsurvey->last_academic)
										Last Academic : {{$survey->leadsurvey->last_academic}} <br/>
										@endif
										
										@if($survey->leadsurvey->when_graduate)
										When Graduate : {{$survey->leadsurvey->when_graduate}} <br/>
										@endif
										
										@if($survey->leadsurvey->area_study_interest)
										Area Study Interest : {{$survey->leadsurvey->area_study_interest}} <br/>
										@endif
										
										@if($survey->leadsurvey->study_destination)
										Study Destination : {{$survey->leadsurvey->study_destination}} <br/>
										@endif
										
										@if($survey->leadsurvey->planning_apply)
										Planning Apply : {{$survey->leadsurvey->planning_apply}} <br/>
										@endif
										
										@if($survey->leadsurvey->level_of_study)
										Level of Study : {{$survey->leadsurvey->level_of_study}} <br/>
										@endif
										
										
										@if($survey->leadsurvey->ever_take_ielts == 'yes')
											@if($survey->leadsurvey->test_taken)
											Test Take : {{$survey->leadsurvey->test_taken}}<br/>
											@endif
											
											@if($survey->leadsurvey->when_take_test)
											When Take Test : {{$survey->leadsurvey->when_take_test}}<br/>
											@endif
											
											@if($survey->leadsurvey->test_score)
											Test Score : {{$survey->leadsurvey->test_score}}<br/>
											@endif
										@endif
										
										@if($survey->leadsurvey->last_holiday)
										Last Holiday : {{ucfirst($survey->leadsurvey->last_holiday)}} <br/>
										@endif
										
										@if($survey->leadsurvey->close_family_overseas)
										Have family overseas : {{$survey->leadsurvey->close_family_overseas}}<br/>
										@endif
										
										@if($survey->leadsurvey->prefer_time)
										Prefer time called : {{$survey->leadsurvey->prefer_time}} <br/>
										@endif
										
										@if($survey->leadsurvey->additional_note)
											Additional Note : {{$survey->leadsurvey->additional_note}}
										@endif
										
									@else
										Age Group : Haven't Decided <br/>
										Gender : Haven't Decided <br/>
										Nationality : Haven't Decided <br/>
									@endif
									
								</td>
                                <td>{{$survey->email}}</td>
								<td>
								
								@if(isset($survey->lead_source))
									Lead Source : {{$survey->lead_source}} <br/>
								@endif
								
								@if(isset($survey->utm_source))
									UTM Source : {{$survey->utm_source}} <br/>
								@endif
								
								@if(isset($survey->campaign))
									UTM Campaign : {{$survey->campaign}} <br/>
								@endif
							
								@if(isset($survey->adset))
									UTM Adset : {{$survey->adset}}<br/>
								@endif
							
								@if(isset($survey->ads))
									UTM Ads : {{$survey->ads}} <br/>
								@endif
								
								
								</td>
                                <td>{{$survey->created_at}}</td>
                            </tr> 
                        @endforeach
                    </tbody>
                </table> 
                
                </div>
				
                
            </div>
        </div>
		
		
        <!-- Load JS here for greater good =============================-->
        <!-- jQuery -->
		<script src="{{asset('ext/vendor/jquery/jquery.js')}}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.js"></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
		
		
		<script src="{{asset('ext/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('ext/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('ext/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('ext/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('ext/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('ext/assets/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
        <script src="{{asset('ext/assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

		<script>
		$(document).ready(function(){
			$('.datatable').DataTable({
				"aaSorting": []
				// processing: true,
				// serverSide: true,

			});
		});
		</script>
        <script>
            $('.edit_candidate_type').on('click',function(e){
               var parent_div = $(this).parent().parent();
               parent_div.find('.candidate_information').addClass('hidden');
               parent_div.find('.form-group').removeClass('hidden');
			   
            });
        </script>
		
		
        <script>
            $('.candidate_submit').on('click',function(e){
                
                var head_of_head_candidate_form = $(this).parent().parent();
                var head_candidate_form = $(this).parent();  
                var candidate_id = $(this).attr('data-id');                
                var value_candidate_type = head_candidate_form.find('#candidate_type').val();
                
                if(value_candidate_type == null){
                    head_candidate_form.find('.candidate_type_info').removeClass('hidden');
                    return;
                }else{
                    head_candidate_form.find('.candidate_type_info').addClass('hidden');
                }
                
                var value_candidate_note = head_candidate_form.find('#candidate_note').val();
                
				var value_lead_status = head_candidate_form.find('select[name=lead_status]').val();
				
				console.log('lead status = '+value_lead_status);
                console.log(candidate_id);
                
                var data_content = {
                    'candidate_id' : candidate_id,
                    'candidate_type' : value_candidate_type,
                    'candidate_note' : value_candidate_note,
					'lead_status' : value_lead_status,
                    '_token' : $('meta[name="_token"]').attr('content'),
                };
                
                
                $.ajax({
                    url: window.location.origin+'/setcandidatetype',
                    method: "POST",
                    data: data_content,
                    dataType: "text",
                    async: true,

                    beforeSend: function(){
                        $('#loading').removeClass('hidden');
                    }
                }).done(function(e){
                    $('#loading').addClass('hidden');
                    head_candidate_form.addClass('hidden');
                    head_of_head_candidate_form.find('.candidate_information').removeClass('hidden');
                    var this_is_candidate_information_section = head_of_head_candidate_form.find('.candidate_information');
                    var json_data = jQuery.parseJSON(e);
                    var penentu = "";
                    var lead_status = "";
					
					if(json_data.candidate_type_controller == 4){
                        penentu = "<span class='label label-default'>cold</span>";
                    }else if(json_data.candidate_type_controller == 3){
                        penentu = "<span class='label label-warning'>luke warm</span>";
                    }else if(json_data.candidate_type_controller == 2){
                        penentu = "<span class='label label-warning'>warm</span>";
                    }else if(json_data.candidate_type_controller == 1){
                        penentu = "<span class='label label-danger'>hot</span>";
                    }
                    
					
					console.log('lead status ='+json_data.lead_status);
					
					if(json_data.lead_status == 7){
                        lead_status = "<span class='label label-warning'>junk</span>";
                    }else if(json_data.lead_status == 6){
                        lead_status = "<span class='label label-warning'>closed</span>";
                    }else if(json_data.lead_status == 5){
                        lead_status = "<span class='label label-default'>converted</span>";
                    }else if(json_data.lead_status == 4){
                        lead_status = "<span class='label label-danger'>insufficient</span>";
                    }else if(json_data.lead_status == 3){
                        lead_status = "<span class='label label-danger'>contacted</span>";
                    }else if(json_data.lead_status == 2){
                        lead_status = "<span class='label label-danger'>attempted</span>";
                    }else if(json_data.lead_status == 1){
                        lead_status = "<span class='label label-danger'>open</span>";
                    }
					
					
                    this_is_candidate_information_section.find('p.candidate_type_value').empty().append(penentu);
                    this_is_candidate_information_section.find('p.candidate_type_lead_status').empty().append(lead_status);
                    this_is_candidate_information_section.find('p.candidate_note_value').text("Note : "+json_data.candidate_note_controller);
                    
                });
            });
        </script>
		<script>
			$('.date-picker').datepicker({
				format: 'yyyy-mm-dd',
			});
		</script>
		
    </body>
</html>
