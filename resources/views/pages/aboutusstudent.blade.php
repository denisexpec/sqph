@extends('layout.frontend')

@section('body')
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <header id="main-header" class="the-header the-origin-header navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header" style="float:none;">
                
                @if(App::getLocale() == 'id')
                <a class="navbar-brand" href="{{url('student')}}">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
                @else
                <a class="navbar-brand" href="{{url('student')}}?lang=en">
                    <img src="{{asset('ext/images/sq-id/logo.png')}}">
                </a>
                @endif

                <!-- <a href="#0" id="nav-menu-trigger" class="menu-toggle flip pull-right all-caps" style="text-transform: uppercase;font-size: 16px;font-weight: bolder;letter-spacing: .5px;margin-left: 20px;">Menu &nbsp;<i class="fa fa-bars" aria-hidden="true"></i></a> -->
                <nav class="navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar-collapse" class="navbar-collapse collapse">
                    <div class="sw tp att">
                        <ul class="navbar-nav">
                            <li class="m adp ">
                                <a class="nh page-scroll" href="{{url('student/aboutus')}}?lang=en">About</a>
                            </li>
                            <li class="m adp ">
                                <a class="nh page-scroll js-scroll-trigger" href="#sosmed-foot">Follow Us</a>
                            </li>
                            <li class="m adp ">
                                <a class="nh page-scroll js-scroll-trigger" href="#lead-form">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                    </div>
                    </nav> 
                {{--<div class="form-group">--}}
                          {{--<select class="form-control" id="sel1" name="change_language" style="width:160px;float:right;">--}}
                            {{--@if(App::getLocale() == 'id')--}}
                                {{--<option value="id" selected>Bahasa Indonesia</option>--}}
                                {{--<option value="en">English</option>--}}
                            {{--@else--}}
                                {{--<option value="id">Bahasa Indonesia</option>--}}
                                {{--<option value="en" selected>English</option>--}}
                            {{--@endif--}}
                          {{--</select>--}}
                        {{--</div>--}}
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </header>

    <!-- Navigation -->
    <nav id="nav-wrapper" class="">
            <a class="nav-close" href="#0"><span class="icon-cross2"></span></a>
            <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    @if(App::getLocale() == 'id')
                    <li>
                        <a class="page-scroll" href="{{url('student/aboutus')}}" style="text-transform: capitalize;">Tentang StudyQuery</a>
                    </li>
                    <li>
                        <a class="page-scroll hidden" href="{{url('student/aboutus')}}" style="text-transform: capitalize;">Mengapa Australia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Ikuti Sosial Media</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#lead-form" style="text-transform: capitalize;">Hubungi Kami</a>
                    </li>
                    @else
                    <li>
                        <a class="page-scroll" href="{{url('student/aboutus')}}?lang=en" style="text-transform: capitalize;">About StudyQuery</a>
                    </li>
                    <li>
                        <a class="page-scroll hidden" href="{{url('student/aboutus')}}?lang=en" style="text-transform: capitalize;">Why Australia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sosmed-foot" style="text-transform: capitalize;">Follow Social Media</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#lead-form" style="text-transform: capitalize;">Contact Us</a>
                    </li>
                    @endif
                    {{--<li>--}}
                        {{--<select class="form-control" id="sel1" name="change_language" style="width:160px;float:right;">--}}
                        {{--@if(App::getLocale() == 'id')--}}
                            {{--<option value="id" selected>Bahasa Indonesia</option>--}}
                            {{--<option value="en">English</option>--}}
                        {{--@else--}}
                            {{--<option value="id">Bahasa Indonesia</option>--}}
                            {{--<option value="en" selected>English</option>--}}
                        {{--@endif--}}
                        {{--</select>--}}
                    {{--</li>--}}
                </ul>
        </nav>

    <!-- Intro Header -->
    <header class="intro" style="height: 50vh;background-position:center top; background: url({{asset('ext/images/sq-id/studyquery.jpg')}});background-size: cover;position:relative;">
        <div class="intro-body">
            <div class="container" style="margin-top: 100px;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-left: 0px;">
                        @if(App::getLocale() == 'id')
                        <h1 class="about-heading" style="font-size: 28px;">Tentang kami</h1>
                        @else
                        <h1 class="about-heading" style="font-size: 28px;">About Us</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </header>


    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 animate-init" data-anim-type="fade-in-down-big" data-anim-delay="100">
                <h2 style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;margin-bottom: 35px;">STUDY QUERY - A New World Awaits</h2>
                <!-- <p style="font-size: 16px; color:#7e8890;"></p> -->
            </div>
        </div>      
    </section>

    <!-- About Section -->
    <section id="about-sq" class="container-fluid content-section text-center" style="padding-bottom: 350px;background: url({{asset('ext/images/sq-id/about-banner.jpg')}}) no-repeat center bottom;background-size: 100% auto;padding-top:0px;margin-top: 50px;">
        <div class="container">
        <div class="row">
            <div class="col-lg-2">
                @if(App::getLocale() == 'id')
                <p>Tentang kami</p>
                @else
                <p>About us</p>
                @endif
            </div>
            <div class="col-lg-10">
                @if(App::getLocale() == 'id')
                <p style="font-size: 14px; color:#7e8890;text-align:justify;margin-bottom: 0px;">Selamat atas keputusan anda untuk belajar di luar negeri. anda akan mengambil langkah pertama menuju kehidupan baru dan kesempatan yang sangat berharga. Anda akan memiliki banyak pengalaman baru yang tidak terbatas seperti; lingkungan baru, bahasa baru, budaya baru dan teman baru. Dengan beberapa perencanaan dan penelitian sederhana anda dapat melakukan transisi yang mudah untuk menuju petualangan baru anda.</p>
                <hr style="border-top: 2px solid #038FA2;">
                <p style="font-size: 14px; color:#7e8890;text-align:justify;margin-bottom: 0px;">
				Study Query adalah agen konsultasi agen pendidikan yang berbasis di Sydney - Australia. Kami membantu dan membimbing siswa untuk memilih jurusan dan institusi pendidikan yang tepat untuk membuat pengalaman yang menyenangkan saat belajar di luar negeri.
                <hr style="border-top: 2px solid #038FA2;">
                
                <h2 class="sq__Title_au" style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;margin-bottom: 35px;line-height: 42px;">Study Query telah membantu<br/>
                lebih dari 10.000 siswa kuliah ke luar negeri</h2>
                <img id="world__Map" src="{{asset('ext/images/sq-id/world-map.png')}}" style="text-align: center; margin-top: 30px; margin-bottom: 30px;">
                
                <p style="font-size: 14px; color:#7e8890;text-align:justify;">
                Sebagian besar konselor kami pernah belajar di luar negeri sehingga mereka dapat merasakan kekhawatiran dan keraguan siswa tentang berada di lingkungan yang baru dan berbeda. Peka, sabar, pengertian, menjadi pendengar yang handal, dan berkarakter ramah adalah perlakuan kami kepada siswa.

                Konselor kami terlatih dan selalu memiliki pengetahuan terbaru tentang informasi pedidikan dari institusi-institusi pendidikan yang kami wakili. Ini termasuk persyaratan pendaftaran, kualitas studi yang diberikan masing-masing institusi, persyaratan visa dan imigrasi, informasi spesifik institusi dan banyak lagi.
                <br/><br/>
                Kami berusaha untuk memberikan layanan terbaik kepada siswa kami dengan memberikan informasi yang akurat, sehingga mereka dapat membuat keputusan tepat yang akan membentuk masa depan mereka.

                Kami berharap bisa menjadi bagian dari petualangan baru anda.

				</p>
				@else
				<p style="font-size: 14px; color:#7e8890;text-align:justify;margin-bottom: 0px;">
					Congratulations on your decision to study overseas. You will be taking the first steps towards a new life andabundant opportunities.

					You will have many new experiences including but not limited to new environment, new language, new culture and new friends. With some simple planning and research you can make a smooth transition towards your new adventure.</p>
                    <hr style="border-top: 2px solid #038FA2;">
                    <p style="font-size: 14px; color:#7e8890;text-align:justify;margin-bottom: 0px;">
					Study Query is an education agent consultant based in Sydney - Australia. We help and guide student to choose the right course and the right education provider to make an enjoyable experience while studying overseas.
				</p>
                <hr style="border-top: 2px solid #038FA2;">
                    <br/>
                    <h2 class="sq__Title_au" style="font-weight: 400; font-size: 28px;text-transform: capitalize;margin-bottom:10px; color:#3b3e46;margin-bottom: 35px;line-height:42px;">Study Query has helped<br/>
                    more than 10,000 students study abroad</h2>
                    <img id="world__Map" src="{{asset('ext/images/sq-id/world-map.png')}}" style="text-align: center; margin-top: 30px; margin-bottom: 30px;">
                    <br/>
                    <p style="font-size: 14px; color:#7e8890;text-align:justify;">
                    Most of our counsellors have experienced studying in a foreign country so they can relate to each student’s concerns and anxieties about being in a new and different environment. To be sensitive, patient, understanding and a reliable listening ear are but some of our teams student-friendly traits.

                    Our counsellors are fully trained and constantly updated in the knowledge and requirements of the different educational institution we are represent. This includes enrolment requirements, the quality of study each institution provides, the visa and immigration requirements, institution specific information and much more.
                    <br/><br/>
                    We strive to provide our student the best possible services so they can make the right informed decisions that will shape the rest of their lives.

                    We look forward to being a part of your new adventure.
				@endif
                
                
            </div><a href="#lead-form" class="btn btn-default page-scroll btn-lg btn-sq-cta" style="font-size: 12px;background: #0890A1;border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">Find out More!</a>
        </div></div>
    </section>

    <!-- About Section -->
    <!-- <section id="about-sq" class="container-fluid content-section text-center" style="padding-bottom: 350px;background: url({{asset('ext/images/sq-id/about-banner.jpg')}}) no-repeat center bottom;background-size: 100% auto;padding-top:0px;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
				@if(App::getLocale() == 'id')
                <p style="font-size: 16px; color:#7e8890;text-align:justify;">Selamat atas keputusan anda untuk belajar di luar negeri. anda akan mengambil langkah pertama menuju kehidupan baru dan kesempatan yang sangat berharga. Anda akan memiliki banyak pengalaman baru yang tidak terbatas seperti; lingkungan baru, bahasa baru, budaya baru dan teman baru. Dengan beberapa perencanaan dan penelitian sederhana anda dapat melakukan transisi yang mudah untuk menuju petualangan baru anda.<br/><br/>

				Study Query adalah agen konsultasi agen pendidikan yang berbasis di Sydney - Australia. Kami membantu dan membimbing siswa untuk memilih jurusan dan institusi pendidikan yang tepat untuk membuat pengalaman yang menyenangkan saat belajar di luar negeri. Dan menyediakan layanan pemasaran atas nama Student World International dan Fortrust Education Services. Representasi merupakan bagian dari kesepakatan kelembagaan antar entitas ini.

				Sebagian besar konselor kami pernah belajar di luar negeri sehingga mereka dapat merasakan kekhawatiran dan keraguan siswa tentang berada di lingkungan yang baru dan berbeda. Peka, sabar, pengertian, menjadi pendengar yang handal, dan berkarakter ramah adalah perlakuan kami kepada siswa.

				Konselor kami terlatih dan selalu memiliki pengetahuan terbaru tentang informasi pedidikan dari institusi-institusi pendidikan yang kami wakili. Ini termasuk persyaratan pendaftaran, kualitas studi yang diberikan masing-masing institusi, persyaratan visa dan imigrasi, informasi spesifik institusi dan banyak lagi.
				<br/><br/>
				Kami berusaha untuk memberikan layanan terbaik kepada siswa kami dengan memberikan informasi yang akurat, sehingga mereka dapat membuat keputusan tepat yang akan membentuk masa depan mereka.

				Kami berharap bisa menjadi bagian dari petualangan baru anda.

				</p>
				@else
				<p style="font-size: 16px; color:#7e8890;text-align:justify;">
					Congratulations on your decision to study overseas. You will be taking the first steps towards a new life andabundant opportunities.

					You will have many new experiences including but not limited to new environment, new language, new culture and new friends. With some simple planning and research you can make a smooth transition towards your new adventure.

					Study Query is an education agent consultant based in Sydney - Australia. We help and guide student to choose the right course and the right education provider to make an enjoyable experience while studying overseas. And provides marketing services on behalf of Student World International and Fortrust Education Services. Representations form part of institutional agreements between these entities.

					Most of our counsellors have experienced studying in aforeign country so they can relate to each student’s concerns and anxieties about being in a new and different environment. To be sensitive, patient, understanding and a reliable listening ear are but some of our teams student-friendly traits.

					Our counsellors are fully trained and constantly updated in the knowledge and requirements of the different educational institution we are represent. This includes enrolment requirements, the quality of study each institution provides, the visa and immigration requirements, institution specific information and much more.

					We strive to provide our student the best possible services so they can make the right informed decisions that will shape the rest of their lives.

					We look forward to being a part of your new adventure.


				</p>
				@endif
                
                <a href="#contact" class="btn btn-default page-scroll btn-lg btn-sq-cta" style="font-size: 12px;background: linear-gradient(to bottom right,#848484 0,#6f736f 100%);border: none;color: #fff;font-weight: bold;letter-spacing: 2px;border-radius:50px; padding: 20px 30px;">Find out More!</a>
            </div>
        </div>
    </section> -->

<!-- Social Media Section -->
    <section id="sosmed-foot" class="container-fluid content-section text-center" style="padding-bottom: 150px;background:#F2F3F4
;">
        <!-- Socmed Section -->
            
                <div class="container relative">
                    
                    <!-- Section Headings -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                            @if(App::getLocale() == 'id')
                            <div class="section-title" style="text-transform:capitalize;">
                                Study Query di Media Sosial
                            </div>
                            <h2 class="section-heading" style="font-size: 16px; font-weight:normal; margin-top: 25px;text-transform: capitalize;margin-top:5px;font-family:'Raleway',sans-serif;">Ketahui berita dan perkembangan terkini seputar sekolah di luar negeri dari akun media sosial Study Query</h2>
                            @else
                             <div class="section-title" style="text-transform:capitalize;">
                                Study Query in Social Media<span class="st-point">.</span>
                            </div>
                            <h2 class="section-heading" style="font-size: 16px; font-weight:normal; margin-top: 25px;text-transform: capitalize;margin-top:5px;font-family:'Raleway',sans-serif;">Get up to speed with news and developments straight from Study Query’s social media updates</h2>
                            @endif
                            <div class="section-line mb-60 mb-xxs-30"></div>
                        </div>
                    </div>
                    <!-- End Section Headings -->
                    
                    <!-- Gallery Grid -->	
					@include('includes.instagram')
                    <!-- End Gallery Grid -->
                    
                    <!-- Blog Posts Grid -->
                    <div class="row multi-columns-row">
						@include('includes.facebook')
                    </div>
                    <!-- End Blog Posts Grid -->      
                </div>
            </section>
    
    <!-- Contact Section -->
        <section id="lead-form">
            @include('includes.insertleadform')
            <!-- End Contact Section -->
        </section>
			
			<form class="hidden submit_change_language" method="get" action="{{url('student/aboutus')}}" >
				<input type="hidden" name="lang" value="id"/>
				<a href="javascript:void(0);" onclick="parentNode.submit();">Submit</a>	
			</form>
@endsection