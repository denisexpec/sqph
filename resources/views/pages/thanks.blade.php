<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('country') - Smart Choice for Smart Students</title>
        <meta name="description" content="Helping students from around the world to study abroard. Our counsellors can help you choose, apply and succesfully accepted in your university of choice, online and direct. We are the future of education services,">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- master css -->
        <!-- <link href="{{ asset('ext/sqlp/css/master.css') }}" rel="stylesheet">
        <link href="{{ asset('ext/sqlp/css/color2.css') }}" rel="stylesheet">

        <link href="{{ asset('ext/sqlp/css/hover.css') }}" rel="stylesheet">
        <link href="{{ asset('ext/sqlp/css/magnific-popup.css') }}" rel="stylesheet"> -->
        <link rel="stylesheet" href="{{ asset('ext/css/main.css') }}" />
        <link rel="shortcut icon" href="{{ asset('ext/images/favicon.ico') }}">
        <script src="http://fast.wistia.com/assets/external/E-v1.js"></script>

        <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
		
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '344245682617173'); // Insert your pixel ID here.
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=344245682617173&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
		
		
		<script>
		fbq('track', 'CompleteRegistration');
		</script>
		
		
    </head>
    <body>
				<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<div class="logo">
							<span class=""><a href="#" style="border-bottom: none;"><img src="{{ asset('ext/images/logo-sq.png')}}"></a></span>
						</div>
						<div class="content">
							<div class="inner">
								@if(App::getLocale() =='id')
								<h1 style="font-size:3.55rem;font-weight: bold;">Terima kasih </h1>
								<p>Kami akan segera menghubungimu untuk mewujudkan impianmu.</p>	
								@else
								<h1 style="font-size:3.55rem;font-weight: bold;">Thanks </h1>
								<p>We'll get in touch with you shortly.</p>
								@endif
							</div>
						</div>
						@if(false)
						<nav>
							<ul>
							@if($_GET)
								<li><a href="{{url('')}}">Home</a></li>
								<li><a href="{{url('aboutus')}}">About</a></li>
								<li><a href="{{url('country')}}">Country</a></li>
							@else
							@endif
							</ul>
						</nav>
						@endif
					</header>

					<!-- Footer -->
					<footer id="footer">
						<p class="copyright" style="margin-bottom: 25px;">&copy; Study Query 2017. All rights reserved.</p>
						<div class="row">
						<div class="col-lg-12 col-md-12">
		                    <a href="https://twitter.com/studyquery" target="_blank" style="border-bottom:transparent;"><i class="fa fa-twitter-square" aria-hidden="true" style="font-size: 40px; margin-right: 10px;"></i></a>
		                    <a href="https://www.instagram.com/sqtotherescue" target="_blank" style="border-bottom:transparent;"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 40px; margin-right: 10px;"></i></a>
		                    <a href="https://www.facebook.com/studyqueryph" target="_blank" style="border-bottom:transparent;"><i class="fa fa-facebook-square"aria-hidden="true" style="font-size: 40px; margin-right: 10px;"></i></a>
		                </div>
		                </div>
					</footer>

			</div>

		<!-- BG -->
			<div id="bg"></div>
		<!-- Scripts -->
		<script src="{{ asset('ext/js/jquery.min.js') }}"></script>
		<script src="{{ asset('ext/js/skel.min.js') }}"></script>
		<script src="{{ asset('ext/js/util.js') }}"></script>
		<script src="{{ asset('ext/js/main.js') }}"></script>
		<script src="{{ asset('ext/js/modernizr.custom.js') }}"></script>
		<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			  ga('create', 'UA-83022576-9', 'auto');
			  ga('send', 'pageview');
		</script>
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-596f299c41f5ca57"></script>
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
    </body>
</html>
