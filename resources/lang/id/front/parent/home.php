<?php

return [

    'header' => 'Kami siap membantu anak Anda kuliah di Australia!',
    'header-desc' => 'Seribu satu hal yang perlu dipersiapkan dalam perencanaan pendidikan anak anda ke Australia. Kami sangat mengerti dan siap membantu anak anda untuk mendapatkan pendidikan yang terbaik, mulai dari proses pendaftaran sampai memilih tempat tinggal yang aman dan nyaman di Australia.',
    
    
];
