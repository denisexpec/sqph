<?php

return [
    
    'btn-submit' => 'Submit',
    'btn-back' => 'Back',
    'yes' => 'Yes',
    'no' => 'No',

    'survey01-head' => 'YOUR WORLD-CLASS HIGHER EDUCATION STARTS NOW',
    'survey01-desc' => 'Explore the the BEST universities & colleges in Australia!',
    'survey01-btn' => 'LET\'S BEGIN',

    'survey02-head' => 'Education Service for You',
    'survey02-desc' => 'Consultation should be personalized and easy – take a second to enter your details to receive the best consultation experience you’ve ever had!',
    'survey02-q1' => 'Which age group do you belong to?',
    'survey02-q2' => 'I identify my gender as :',
    'survey02-q2a1' => 'Male',
    'survey02-q2a2' => 'Female',
    'survey02-q3' => 'What is your nationality?',
    'survey02-other' => 'Others',
    'survey02-nationality' => 'Please fill your nationality here.',
    'survey02-btn' => 'I\'m ready!',
    
    'survey03-head' => 'Your world-class education starts now!',
    'survey03-desc' => 'Choose one statement that best represents your condition',
    'survey03-q1a1' => 'I know exactly which course and country I want to go to',
    'survey03-q1a2' => 'Not sure about the course, but I know which country I want',
    'survey03-q1a3' => 'Help me find the perfect course and country!',
    
    'survey05-head' => 'Alright! Just enter your details and we’ll find you the university that perfectly suits your preferences.',
    'survey05-q1' => 'What area are you most interested in?',
    'survey05-q1d' => 'Please choose one of these options',
    'survey05-q1a0' => 'Select your option',
    'survey05-q1a1' => 'Agriculture and Veterinary Medicine',
    'survey05-q1a2' => 'Applied and Pure Sciences',
    'survey05-q1a3' => 'Architecture and Construction',
    'survey05-q1a4' => 'Business and Management',
    'survey05-q1a5' => 'Computer Science and IT',
    'survey05-q1a6' => 'Creative Arts and Design',
    'survey05-q1a7' => 'Education and Training',
    'survey05-q1a8' => 'Engineering',
    'survey05-q1a9' => 'Health and Medicine',
    'survey05-q1a10' => 'Humanities',
    'survey05-q1a11' => 'Law',
    'survey05-q1a12' => 'MBA',
    'survey05-q1a13' => 'Personal Care and Fitness',
    'survey05-q1a14' => 'Social Studies and Media',
    'survey05-q1a15' => 'Travel and Hospitality',
    'survey05-q2' => 'Where is your chosen study destination?',
    'survey05-q2d' => 'Please choose one of these options',
    'survey05-q3' => 'When are you planning to apply?',
    'survey05-q3d' => 'Please choose one of these options',
    'survey05-q3a1' => 'Unsure',
    
    'survey06-head' => 'Get excited! Our counselor will present a selection of the best universities for you.',
    'survey06-desc' => 'Have you decided which study destination you want to go to?',
    
    'survey07-head' => 'Alright! We’ll find you the course that perfectly suits your strength.',
    'survey07-q1' => 'Where is your chosen study destination?',
    'survey07-q1d' => 'Please choose one of these options',
    'survey07-q1a0' => 'Select your option',
    'survey07-q2' => 'When are you planning to apply?',
    'survey07-q2d' => 'Please choose one of these options',
    'survey07-q2a1' => 'Unsure',
    
    'survey08-head' => 'Alright! Discover courses that matters to you and seek inspiring study destinations.',
    'survey08-q1' => 'When are you planning to apply?',
    'survey08-q1d' => 'Please choose one of these options',
    'survey08-q1a1' => 'Unsure',
    
    'survey09-head' => 'Have you taken a TOEFL or Academic IELTS test before?',
    
    'survey13-head' => 'Thanks! We hear you loud and clear!',
    'survey13-desc' => 'Our counsellors will contact you as soon as possible to start your education abroad.',
    'survey13-btn' => 'Back to Fortrust website',
    
    'survey14-head' => 'Please input your IELTS or TOEFL score :',
    'survey14-desc' => 'Just leave it blank if you\'ve forgotten your score',
    'survey14-time' => 'When did you take your Academic IELTS / TOEFL?',
    'survey14-warn' => 'Please Choose One of Below',
    'survey14-before' => 'Before 2015',
        
];
